<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->integer('company_id')->unsigned()->nullable();
            $table->string('tax_id', 64)->nullable();;
            $table->integer('user_role_id')->unsigned();
            $table->string('username', 32)->unique();
            $table->string('password', 2048)->nullable()    ;
            $table->enum('status', ['valid', 'rejected','pending'])->default('valid');
            $table->string('note')->nullable();
            $table->boolean('is_active')->default(true);


            $table->timestamps();

            $table->index('username');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('user_role_id')->references('id')->on('user_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
