<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Emails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('to', 255);
            $table->string('cc', 255)->nullable();
            $table->string('from', 255)->nullable();
            $table->string('subject', 255);
            $table->text('body');
            $table->dateTime('time_sent')->nullable();
            $table->boolean('sent_ind')->default(false);
            $table->timestamps();

            $table->index('sent_ind');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
