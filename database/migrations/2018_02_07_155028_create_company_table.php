<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('company_code', 16);
            $table->string('company_name', 128);
            $table->string('quickbooks_list_id', 64)->nullable();
            $table->boolean('approve_system')->default(false);
            $table->string('shipper_group_code', 32)->nullable();

            $table->string('tax_id', 64)->nullable();;
            $table->string('first_name', 64)->nullable();;
            $table->string('last_name', 64)->nullable();;
            $table->string('address1', 128)->nullable();
            $table->string('address2', 128)->nullable();
            $table->string('address3', 128)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->string('zip', 64)->nullable();
            $table->string('country', 64)->nullable();
            $table->string('phone1', 64)->nullable();
            $table->string('phone2', 64)->nullable();
            $table->string('fax', 64)->nullable();
            $table->string('email', 64)->nullable();


            $table->boolean('is_admin')->default(false);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
