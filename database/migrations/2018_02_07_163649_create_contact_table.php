<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable();
            $table->string('first_name', 64)->nullable();;
            $table->string('last_name', 64)->nullable();;
            $table->string('address1', 128)->nullable();
            $table->string('address2', 128)->nullable();
            $table->string('address3', 128)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->string('zip', 64)->nullable();
            $table->string('country', 64)->nullable();
            $table->string('phone1', 64)->nullable();
            $table->string('phone2', 64)->nullable();
            $table->string('fax', 64)->nullable();
            $table->string('email', 64)->nullable();
            $table->string('url', 64)->nullable();
            $table->integer('image_id')->unsigned()->nullable();
            $table->string('language', 8)->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
