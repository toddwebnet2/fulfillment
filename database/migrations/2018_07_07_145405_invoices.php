<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('invoice_number');
            $table->integer('company_id')->unsigned();

            $table->string('first_name', 64)->nullable();
            $table->string('last_name', 64)->nullable();
            $table->string('address1', 128)->nullable();
            $table->string('address2', 128)->nullable();
            $table->string('address3', 128)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->string('zip', 64)->nullable();
            $table->string('country', 64)->nullable();
            $table->string('phone1', 64)->nullable();
            $table->string('phone2', 64)->nullable();
            $table->string('fax', 64)->nullable();
            $table->string('email', 64)->nullable();


            $table->decimal('invoice_amount', 18, 2)->default(0);

            $table->text('notes')->nullable();
            $table->string('status')->default(1);
            $table->boolean('is_active')->default(true);
            $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
        \Illuminate\Support\Facades\DB::statement("ALTER TABLE invoices AUTO_INCREMENT = 5000;");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('invoices');
    }
}
