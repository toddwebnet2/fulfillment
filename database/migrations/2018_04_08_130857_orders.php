<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('confirmation_number', 128)->nullable();
            $table->integer('company_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('shipper_id')->unsigned();
            $table->string('event', 64)->nullable();
            $table->string('from_first_name', 64)->nullable();
            $table->string('from_last_name', 64)->nullable();
            $table->string('from_company', 64)->nullable();

            $table->string('from_address1', 128)->nullable();
            $table->string('from_address2', 128)->nullable();
            $table->string('from_address3', 128)->nullable();
            $table->string('from_city', 64)->nullable();
            $table->string('from_state', 64)->nullable();
            $table->string('from_zip', 64)->nullable();
            $table->string('from_country', 64)->nullable();
            $table->string('from_phone1', 64)->nullable();
            $table->string('from_phone2', 64)->nullable();
            $table->string('from_fax', 64)->nullable();
            $table->string('from_email', 64)->nullable();

            $table->string('to_first_name', 64)->nullable();
            $table->string('to_last_name', 64)->nullable();
            $table->string('to_company', 64)->nullable();
            $table->string('to_address1', 128)->nullable();
            $table->string('to_address2', 128)->nullable();
            $table->string('to_address3', 128)->nullable();
            $table->string('to_city', 64)->nullable();
            $table->string('to_state', 64)->nullable();
            $table->string('to_zip', 64)->nullable();
            $table->string('to_country', 64)->nullable();
            $table->string('to_phone1', 64)->nullable();
            $table->string('to_phone2', 64)->nullable();
            $table->string('to_fax', 64)->nullable();
            $table->string('to_email', 64)->nullable();
            $table->text('notes')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('shipper_id')->references('id')->on('shippers');
        });

        \Illuminate\Support\Facades\DB::statement("ALTER TABLE orders AUTO_INCREMENT = 50000;");

        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->string('item_type', 128);
            $table->string('ref', 32);
            $table->string('title', 255);

            $table->integer('num_items')->unsigned();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('item_id')->references('id')->on('items');
        });

        Schema::create('order_trackings', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('shipper_id')->unsigned();
            $table->string('tracking_number', 64)->nullable();
            $table->decimal('cost', 18, 2)->nullable();
            $table->decimal('fees', 18, 2)->nullable();
            $table->decimal('invoice_amount', 18, 2)->nullable();
            $table->decimal('profit', 18, 2)->nullable();
            $table->string('descr', 255)->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('shipper_id')->references('id')->on('shippers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('order_trackings');
        Schema::drop('order_items');
        Schema::drop('orders');
    }

}
