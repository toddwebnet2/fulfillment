<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFedexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fedex', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_id', 255)->nullable();
            $table->string('reference', 255)->nullable();
            $table->date('date')->nullable();
            $table->string('recipient',255)->nullable();
            $table->string('company',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('address2',255)->nullable();
//            $table->string('city')->nullable();
//            $table->string('state')->nullable();
//            $table->string('zip_code')->nullable();
            $table->string('phone',255)->nullable();
            $table->decimal('rate_quote', 18,2)->nullable();
            $table->decimal('billed_amount', 18,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fedex');
    }
}
