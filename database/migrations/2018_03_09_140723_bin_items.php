<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BinItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('bin_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('num_items')->unsigned();
            $table->timestamps();
            $table->unique(['bin_id', 'item_id']);

            $table->foreign('bin_id')->references('id')->on('bins');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_items');
    }
}
