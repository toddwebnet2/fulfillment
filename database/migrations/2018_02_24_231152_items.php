<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Items extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * grms
         * lbs
         * kg
         * oz
         */
        // add shippers

        Schema::create('items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('item_type_id')->unsigned();
            $table->string('ref', 32);
            $table->string('title', 255);
            $table->decimal('item_w', 10, 2)->nullable();
            $table->decimal('item_d', 10, 2)->nullable();
            $table->decimal('item_h', 10, 2)->nullable();
            $table->decimal('weight', 10, 2)->nullable();
            $table->string('weight_unit', 16)->nullable();
            // $table->integer('shipper_id')->unsigned();
            $table->integer('image_id')->unsigned()->nullable();
            $table->integer('reorder')->unsigned();
            $table->boolean('is_active')->default(true);
            $table->timestamps();

            $table->foreign('item_type_id')->references('id')->on('item_types');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
