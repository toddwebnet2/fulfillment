<?php

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Contact;
use App\Models\User;
use App\Models\UserRole;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        print "############## SEEDING DATABASE ##############\n";
        $this->seedUserRoles();
        print "\n\ncompleted.\n";
    }

    private function seedUserRoles()
    {
        print "# userRoles #\n";
        $num = UserRole::all()->count();
        if ($num > 0) {
            print "{$num} Entries already found.\n";
        } else {
            $rows = [
                [
                    'code' => 'ADMIN',
                    'name' => 'Administrator'
                ],
                [
                    'code' => 'USER',
                    'name' => 'Customer'
                ],
            ];
            foreach ($rows as $fields) {

                $userRole = new UserRole();
                foreach ($fields as $field => $value) {
                    $userRole->$field = $value;
                }
                $userRole->save();
                $num++;
            }

            print "{$num} Entries inserted.\n\n";
        }

        print "\nBins";
        $binBins = [
            1 => ['A', 'B', 'C'],
            2 => ['D', 'E', 'F'],
            3 => ['G', 'H', 'I'],
            4 => ['M', 'N', 'O'],
            5 => ['P', 'Q', 'R'],
            6 => ['S', 'T', 'U'],
        ];
        \App\Models\Bin::where('warehouse', 'Houston')->delete();
        foreach ($binBins as $rack => $bins) {
            foreach ($bins as $bin) {
                for ($x = 1; $x < 10; $x++) {
                    $binLabel = "R{$rack}-L{$x}-B{$bin}";
                    \App\Models\Bin::create([
                        'name' => $binLabel,
                        'warehouse' => 'Houston',
                        'code' => $binLabel
                    ]);
                }
            }
        }
    }
}
