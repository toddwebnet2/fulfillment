<?php

namespace App\Services;

use App\Models\Company;
use App\Models\MailGunner\Email;
use App\Models\Order;
use App\Models\User;

class MailGunnerService
{

    public static function addEmail($to, $from, $subject, $body)
    {
        if (env('ORDER_EMAIL', null) == "toddwebnet@gmail.com") {
            $to = env('ORDER_EMAIL', null);
        }
        Email::create([
            'message_id' => null,
            'account_code' => 'fulfillment',
            'to' => $to,
            'from' => $from,
            'subject' => $subject,
            'body' => $body
        ]);
    }
//
//    public static function sendOrderEmail($orderId, $companyCode = 'FEIADMIN')
//    {
//
//        $tos = [SecurityService::getCompanyEmail($companyCode)];
//        $order = Order::find($orderId);
//        $tos[] = $order->from_email;
//        $user = User::find($order->user_id);
//        $tos[] = $user->contact->email;
//
//        $emailsDone = [];
//        foreach ($tos as $to) {
//            if ($to !== null && filter_var($to, FILTER_VALIDATE_EMAIL) && !in_array($to, $emailsDone)) {
//                $emailsDone[] = $to;
//                $subject = "An Order Has Been Received";
//                $from = "fulfillment@feifinancial.com";
//                $link = trim(env('APP_URL'), '/') . "/order/view/" . hashEncrypt($orderId);
//                $orderView = "<h1>An Order Has Been Received</h1><br><a href='{$link}'>{$link}</a><br>" .
//                    OrderService::getOrderHtml($orderId, false);
//                self::addEmail($to, $from, $subject, $orderView);
//            }
//        }
//    }
//
//    public static function sendShippedEmail($orderId, $companyCode = 'FEIADMIN')
//    {
//        $to = SecurityService::getCompanyEmail($companyCode);
//
//        $subject = "Fulfillment Order Shipped";
//        $from = "fulfillment@feifinancial.com";
//        $link = trim(env('APP_URL'), '/') . "/public/order/" . hashEncrypt($orderId);
//
//        $orderView = "<h1> Your Fulfillment Order Has Been Shipped</h1><br><a href='{$link}'>{$link}</a><br>" .
//            OrderService::getOrderHtml($orderId, false);
//
//        $order = Order::find($orderId);
//        $company = Company::find($order->company_id);
//        $user = User::find($order->user_id);
//        $emailTos = [
//            $user->contact->email,
//            $company->email,
//            $order->from_email,
//            $to,
//        ];
//        $emailsDone = [];
//        foreach ($emailTos as $emailTo) {
//            if ($emailTo !== null && filter_var($emailTo, FILTER_VALIDATE_EMAIL) && !in_array($emailTo, $emailsDone)) {
//                $emailsDone[] = $emailTo;
//                self::addEmail($emailTo, $from, $subject, $orderView);
//            }
//        }
//    }

}
