<?php

namespace App\Services;

use App\Models\Company;
use App\Models\User;

class AuthService
{

    public static function isLoggedIn()
    {
        return (is_numeric(session('userId')));
    }

    public static function getCompanyAdmins()
    {
        return ['FEIADMIN', 'CABOS'];
    }

    public static function getCurrentCompanyIds()
    {
        return self::getCurrentCompanyCodes('id');
    }

    public static function getCurrentCompanyCodes($id = 'company_code')
    {
        return self::getCompanyCodeMap(self::getCompanyCode(), $id);
    }

    public static function getCompanyCodeMap($ownerCode, $id = 'company_code')
    {

        switch ($ownerCode) {
            case "FEIADMIN":
                return Company::whereNotIn('id', self::getCompanyCodeMap('CABOS', 'id'))->pluck($id)->toArray();
                break;
            case "CABOS":
                return Company::whereRaw("company_code like '%CABOS%'")->pluck($id)->toArray();
                break;
        }
        return Company::where('company_code', $ownerCode)->pluck($id)->toArray();
    }

    public static function isCompanyAdminUser()
    {

        return self::isAdminUser() && in_array(self::getCompanyCode(), self::getCompanyAdmins());
    }

    public static function isFeiAdminUser()
    {
        return self::isAdminUser() && self::getCompanyCode() == "FEIADMIN";
    }

    public static function isAdminUser()
    {
        return (session('userRole') == 'ADMIN');
    }

    public static function getCompanyId()
    {
        return session('company_id');
    }

    public static function getCompanyCode()
    {
        return session('companyCode');
    }

    public static function storeUserInSession($username)
    {
        $request = request();
        $session = $request->session();
        $userDetails = User::getUserDetails(User::getUserIdFromUserName($username));
        $session->put('userId', $userDetails->user_id);
        $session->put('companyCode', $userDetails->company_code);
        $session->put('company_id', $userDetails->company_id);
        $session->put('userRole', $userDetails->user_role);
    }

    public static function getUserInformation()
    {
        $userId = session('userId');

        if (!is_numeric($userId)) {
            return null;
        }
        return User::getUserDetails($userId);
    }

    public static function getUserId()
    {
        return session('userId');
    }
}
