<?php

namespace App\Services;

use App\Http\Middleware\AuthIsAdmin;
use App\Models\ShoppingCart;
use Illuminate\Support\Facades\DB;

class ShoppingCartService
{

    public static function getShoppingCartItemCount($itemIds)
    {
        if (count($itemIds) == 0) {
            return [];
        }
        $qs = implode(',', array_fill(0, count($itemIds), '?'));

        $sql = "
        select i.id item_id, coalesce (sum(sc.num_items), 0) as num_items
        from items i
        left outer join shopping_carts sc on sc.item_id = i.id
          and sc.user_id = ?
        where i.id in ({$qs})   
        group by i.id
        ";
        $params = $itemIds;
        array_unshift($params, AuthService::getUserId());
        $items = [];
        foreach ($itemIds as $itemId) {
            $items[$itemId] = 0;
        }
        foreach (DB::select($sql, $params) as $entity) {
            $items[$entity->item_id] = $entity->num_items;
        }

        return $items;
    }

    public static function saveToCart($itemId, $qty)
    {
        $userId = AuthService::getUserId();
        ShoppingCart::where([
            ['user_id', $userId],
            ['item_id', $itemId]
        ])->delete();
        if ($qty > 0) {
            ShoppingCart::create([
                'user_id' => $userId,
                'item_id' => $itemId,
                'num_items' => $qty,
            ]);
        }
    }

    public static function numCartItems()
    {
        $sql = "
            select 
              coalesce (count(item_id), 0) num_products, 
              coalesce (sum(num_items), 0) num_items
            from (
              select item_id, sum(num_items) num_items 
              from shopping_carts
              where user_id = ?
              group by item_id
            )x            
        ";
        $params = [AuthService::getUserId()];
        foreach (DB::select($sql, $params) as $obj) {
            return (array)$obj;
        }
    }

    public static function getCartCompanyId()
    {
        $companyId = AuthService::getCompanyId();
        $sql = "
        select 
          distinct i.company_id
        from shopping_carts sc
        inner join items i on i.id = sc.item_id
        where sc.user_id = ?
        ";
        $params = [AuthService::getUserId()];

        foreach (DB::select($sql, $params) as $row) {
            $companyId = $row->company_id;
            break;
        }
        return $companyId;
    }

    public static function getCart()
    {
        $sql = "
        select 
          sc.item_id, sc.num_items, it.name item_type, i.ref, i.title, ig.path
        from shopping_carts sc
        inner join items i on i.id = sc.item_id
        inner join item_types it on it.id = i.item_type_id
        left outer join images ig on ig.id = i.image_id
        where sc.user_id = ?
      ";
        $params = [AuthService::getUserId()];
        return DB::select($sql, $params);
    }

    public static function resetCart()
    {
        $userId = AuthService::getUserId();
        ShoppingCart::where([
            ['user_id', $userId]
        ])->delete();
    }
}
