<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class InvoicingService
{

    public function getCompanyListWithOrderCount()
    {
        $ids = implode(',', AuthService::getCurrentCompanyIds());
        $sql = "
        select c.id company_id, c.company_code, c.company_name, count(o.id) num_orders 
        from companies c
        left outer join orders o on c.id = o.company_id and o.status_id = 2
        where c.is_active = 1
        and c.id in ({$ids }) 
        group by c.id, c.company_code, c.company_name
        order by count(o.id) desc
        ";
        return DB::select($sql);
    }
}
