<?php

namespace App\Services;

use App\Models\Bin;
use App\Models\BinItem;
use App\Models\Company;
use App\Models\Emails;
use App\Models\ItemType;
use App\Models\Order;
use App\Models\OrderFulfillment;
use App\Models\OrderItem;
use App\Models\OrderItemBin;
use App\Models\OrderTracking;
use App\Models\Shipper;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderService
{

    public static function sendOrderEmails($orderId)
    {
        $emails = self::collectEmails($orderId);
        if ($emails == []) {
            return;
        }
        $order = Order::find($orderId);
        $status = CollectionsService::getStatusList()[$order->status_id];
        $doneEmails = [];
        foreach ($emails as $email) {
            if (in_array($email, $doneEmails)) {
                continue;
            }
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $doneEmails[] = $email;
                MailGunnerService::addEmail(
                    $email,
                    'fulfillment@feifinancial.com',
                    __('fei.feiOrder') . ' - ' . $status,
                    self::getOrderHtml($orderId)
                );
            }
        }
    }

    public static function collectEmails($orderId)
    {
        $order = Order::find($orderId);
        $company = Company::find($order->company_id);
        $emails = [];
        $emails[] = $order->to_email;
        $emails[] = $order->from_email;
        $user = User::find($order->user_id);
        if ($user !== null) {
            $emails[] = $user->contact->email;
        }
        $emails = array_merge($emails, SecurityService::getCompanyEmail($company->company_code));
        $emails = array_unique($emails);
        $emails2 = [];
        foreach ($emails as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emails2[] = $email;
            }
        }
        return $emails2;
    }

    public static function formatAddresses($order, $type, $isHtml = true)
    {
        $order = $order->toArray();
        $type .= '_';

        $op = [
            trim($order[$type . 'first_name'] . ' ' . $order[$type . 'last_name']),
            trim($order[$type . 'company']),
            trim($order[$type . 'address1']),
            trim($order[$type . 'address2']),
            trim($order[$type . 'address3']),
            trim($order[$type . 'city'] . ' ' . $order[$type . 'state'] . ' ' . $order[$type . 'zip']),
            trim($order[$type . 'country']),
            trim((trim($order[$type . 'phone1']) == '' ? '' : 'Phone: ' . $order[$type . 'phone1'])),
            trim((trim($order[$type . 'phone2']) == '' ? '' : 'Phone: ' . $order[$type . 'phone2'])),
            trim((trim($order[$type . 'fax']) == '' ? '' : 'Fax: ' . $order[$type . 'fax'])),
            trim((trim($order[$type . 'email']) == '' ? '' : 'Email: ' . $order[$type . 'email'])),
        ];
        $op2 = [];
        foreach ($op as $o) {
            if (trim($o) != '') {
                $op2[] = $o;
            }
        }
        $delim = ($isHtml) ? '<BR/>' : "\n";
        return implode($delim, $op2);
    }

    public static function getOrderHtml($orderId, $showCost = true)
    {
        $showCost = false;
        $order = Order::find($orderId);
        if ($order === null) {
            throw new ModelNotFoundException("Order {$orderId} not found");
        }
        $data = [
            'order' => $order,
            'items' => $order->items,
            'status' => CollectionsService::getStatusList()[$order->status_id],
            'orderTracking' => $order->tracking,
            'company' => Company::find($order->company_id),
            'shipper' => Shipper::find($order->shipper_id),
            'shippers' => Shipper::pluck('shipper_name', 'id'),
            'showCost' => $showCost
        ];
        return view('order.order', $data);
    }

    public static function getFulfillmentCount($orderId)
    {
        $sql = " 
              select sum(num_items) num_items, sum(items_remaining) as items_remaining from (
                  select o.item_id, o.num_items, o.num_items - COALESCE(f.num_items, 0) items_remaining
                  from (
                      select 
                        item_id, sum(num_items) num_items 
                      from order_items
                      where order_id = ? 
                      group by item_id              
                  ) o
                  left outer join (
                    select 
                      item_id, sum(num_items) num_items
                    from order_fulfillments
                    where order_id = ? 
                    group by item_id
                  ) f
                  on f.item_id = o.item_id
              ) x              
              ";
        $params = [$orderId, $orderId];
        return DB::select($sql, $params)[0];
    }

    public static function getStatusBarCounts()
    {
        /**
         *    0 => __('message.pending'),
         * 1 => __('message.incomplete'),
         * 2 => __('message.readyForInvoice'),
         * 3 => __('message.canceled'),
         * 4 => __('message.complete'),
         */
        $ids = implode(',', AuthService::getCurrentCompanyIds());
        $counts = DB::select("
        select status_id, count(id) num_orders
        from orders 
        where status_id < 1
        and company_id in ({$ids})
        group by status_id
        order by status_id
        ");
        $statusList = CollectionsService::getStatusList();
        unset($statusList[1]);
        // unset($statusList[2]);
        unset($statusList[3]);
        unset($statusList[4]);

        $list = [];
        foreach ($statusList as $statusId => $status) {
            $list[$status] = 0;
        }

        foreach ($counts as $count) {
            $list[$statusList[$count->status_id]] += $count->num_orders;
        }

        return $list;
    }

    public static function getPicklistBins(Order $order)
    {
        $items = $order->items;
        $bins = CollectionsService::getBins();
        $retItems = [];
        foreach ($items as $item) {
            $i = $item->toArray();
            self::makeSurePicklistIsThere($item);
            $item = OrderItem::find($item->id);
            $i['bins'] = self::reconcilePickListBinItem($item['num_items'],
                self::getBinLocsForItemFromDB($item, $bins));
            $retItems[] = $i;
        }

        return $retItems;
    }

    public static function replenishOrder(Order $order)
    {

        foreach ($order->items as $item) {

            foreach (OrderItemBin::where('order_item_id', $item->id)->get() as $orderItemBin) {

                $binItems = BinItem::where([
                    'item_id' => $item->id,
                    'bin_id' => $orderItemBin->bin_id
                ])->get();
                if (count($binItems) > 0) {
                    $binItem = $binItems[0];
                    $binItem->num_items += $orderItemBin->num_items;
                    $binItem->save();
                } else {
                    BinItem::create([
                        'item_id' => $item->id,
                        'bin_id' => $orderItemBin->bin_id,
                        'num_items' => $orderItemBin->num_items
                    ]);
                }
            }
        }
    }

    public static function refreshPicklistBins(Order $order)
    {
        self::flushPicklistBins($order);
        self::getPicklistBins($order);
    }

    public static function flushPicklistBins(Order $order)
    {
        foreach ($order->items as $item) {
            OrderItemBin::where('order_item_id', $item->id)->delete();
        }
    }

    private static function makeSurePicklistIsThere(OrderItem $item)
    {
        $count = $item->num_items;
        Log::info($item->id);
        if (self::doINeedToRefreshBins($item)) {
            OrderItemBin::where('order_item_id', $item->id)->delete();
            foreach (self::getBinLocsForItem($item->item_id) as $bin) {
                $numItems = $bin->num_items;
                if ($numItems > $count) {
                    $numItems = $count;
                }

                $oib = new OrderItemBin;
                $oib->order_item_id = $item->id;
                $oib->bin_id = $bin->id;
                $oib->num_items = $numItems;
                $oib->save();

                $count = $count - $numItems;
                if ($count == 0) {
                    break;
                }
            }
            if ($count > 0) {
                $oib = new OrderItemBin;
                $oib->order_item_id = $item->id;
                $oib->bin_id = null;
                $oib->num_items = $numItems;
                $oib->save();
            }
        }
    }

    private static function reconcilePickListBinItem($count, $bins)
    {
        $r = [];
        foreach ($bins as $bin) {
            $bin = (object)$bin;
            if ($bin->num_items > $count) {
                $bin->num_items = $count;
            }
            $r[] = $bin;
            $count = $count - $bin->num_items;
        }
        if ($count > 0) {
            $r[] = (object)[
                'id' => null,
                'warehouse' => 'none',
                'bin' => 'unknown',
                'num_items' => $count,
            ];
        }

        return $r;
    }

    private static function doINeedToRefreshBins(OrderItem $item)
    {
        $numItems = 0;
        foreach ($item->itemBins as $bin) {
            $numItems += $bin->num_items;
        }
        return ($numItems != $item->num_items);
    }

    private static function getBinLocsForItemFromDB(OrderItem $item, $bins = null)
    {
        if ($bins == null) {
            $bins = CollectionsService::getBins();
        }
        $r = [];
        foreach ($item->itemBins as $itemBin) {
            $binId = $itemBin->bin_id;
            if ($binId == null) {
                $binId = 0;
            }
            $bin = $bins[$binId];

            $r[] = (object)[
                'id' => $itemBin->bin_id,
                'warehouse' => $bin->warehouse,
                'bin' => $bin->name,
                'num_items' => $itemBin->num_items,
            ];
        }

        return $r;
    }

    public static function getBinLocsForItem($itemId)
    {

        $binItems = BinItem::where([
            ['item_id', $itemId]
        ])->orderBy('num_items')->get();
        $r = [];

        foreach ($binItems as $binItem) {

            $r[] = (object)[
                'id' => $binItem->bin->id,
                'warehouse' => $binItem->bin->warehouse,
                'bin' => $binItem->bin->name,
                'num_items' => $binItem->num_items,
            ];
        }

        return $r;
    }

    public static function updateOrderFulfillment($orderId, $items)
    {
        $sql = "delete from order_item_bins where order_item_id in (select id from order_items where order_id = ?)";
        $params = [$orderId];
        DB::update($sql, $params);
        OrderFulfillment::where('order_id', $orderId)->delete();

        $orderItems = OrderItem::where('order_id', $orderId)->pluck('id', 'item_id')->toArray();

        foreach ($items as $itemId => $bins) {
            foreach ($bins as $binId => $numItems) {
                $orderItemBin = OrderItemBin::create([
                    'order_item_id' => $orderItems[$itemId],
                    'bin_id' => $binId,
                    'num_items' => $numItems
                ]);
                $orderItemBin->save();
                $orderFulfillment = OrderFulfillment::create([
                    'order_id' => $orderId,
                    'user_id' => AuthService::getUserId(),
                    'item_id' => $itemId,
                    'bin_id' => $binId,
                    'num_items' => $numItems
                ]);
                $orderFulfillment->save();
            }
        }
    }

    public static function updateOrderTracking($orderId, $trackings)
    {
        OrderTracking::where('order_id', $orderId)->delete();
        foreach ($trackings as $tracking) {
            $tracking['order_id'] = $orderId;
            $tracking['descr'] = '';

            $orderTracking = OrderTracking::create($tracking);
            $orderTracking->save();
        }
    }

    public static function fulfillOrderFulfillments($orderId)
    {

        foreach (OrderFulfillment::where('order_id', $orderId)->where('fulfilled', 0)->get() as $orderFulfillment) {
            $bin = BinItem::where([
                'bin_id' => $orderFulfillment->bin_id,
                'item_id' => $orderFulfillment->item_id
            ])->first();
            if ($bin !== null) {
                $bin->num_items = $bin->num_items - $orderFulfillment->num_items;
                $bin->save();
            } else {
                BinItem::create([
                    'bin_id' => $orderFulfillment->bin_id,
                    'item_id' => $orderFulfillment->item_id,
                    'num_items' => 0 - $orderFulfillment->num_items
                ]);
            }
            $orderFulfillment->fulfilled = 1;
            $orderFulfillment->save();
        }
    }

    public static function updateInventoryFromCompletedOrder($orderId)
    {
        $order = Order::find($orderId);
        if ($order->finalized = 0) {
            $orderBins = OrderFulfillment::where('order_id', $orderId)->get();
            foreach ($orderBins as $orderBin) {
                $binObj = BinItem::where([
                    ['bin_id', $orderBin->bin_id],
                    ['item_id', $orderBin->item_id]
                ]);
                if ($binObj->count() > 0) {
                    $bin = $binObj->first();
                    $bin->num_items = $bin->num_items - $orderBin->num_items;
                    $bin->save();
                } else {
                    $bin = BinItem::create([
                        'item_id' => $orderBin->item_id,
                        'bin_id' => $orderBin->bin_id,
                        'num_items' => 0 - $orderBin->num_items
                    ]);
                    $bin->save();
                }
            }
        }
        $order->status_id = 5;
        $order->finalized = true;
        $order->save();
    }

    public static function getOrderAddresses($direction, $companyId, $userId)
    {
        $direction = $direction . "_";
        $sql = "
        select
        {$direction}first_name,
        {$direction}last_name,
        {$direction}company,
        {$direction}address1,
        {$direction}address2,
        {$direction}address3,
        {$direction}city,
        {$direction}state,
        {$direction}zip,
        {$direction}country,
        {$direction}phone1,
        {$direction}phone2,
        {$direction}fax,
        {$direction}email,
        case when user_id = ? then 0 else 1 end userSort
        
        ,count(id) numOrders
        ,max(created_at) as lastOrderDate
        
        from orders
        where created_at > ?
         and company_id = ?
        group by 
        {$direction}first_name,
        {$direction}last_name,
        {$direction}company,
        {$direction}address1,
        {$direction}address2,
        {$direction}address3,
        {$direction}city,
        {$direction}state,
        {$direction}zip,
        {$direction}country,
        {$direction}phone1,
        {$direction}phone2,
        {$direction}fax,
        {$direction}email,
       userSort
        
        order by userSort asc, 
        count(id) desc

        ";
        $lastYear = date("Y-m-d", strtotime(date("Y-m-d") . " - 1 year"));
        $params = [$userId, $lastYear, $companyId];
        // $params = [$userId, $lastYear];
        return DB::select($sql, $params);
    }

}
