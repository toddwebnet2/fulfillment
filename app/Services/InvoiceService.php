<?php

namespace App\Services;

use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class InvoiceService
{
    public function checkUpdateTrackingReadyForInvocing()
    {

        $sql = "
            select o.id order_id, o.status_id,
            sum(case when ot.profit is null then 0 else 1 end) num_completed, sum(1) num
            from orders o
            inner join order_trackings ot on ot.order_id = o.id
            where o.status_id not in (0,1,3,4)
            group by o.id, o.status_id
        ";
        foreach (DB::select($sql) as $row) {
            if ($row->num_completed == $row->num) {
                if ($row->status_id != 2) {
                    $order = Order::find($row->order_id);
                    $order->status_id = 2;
                    $order->save();
                }
            } else {
                if ($row->status_id != 5) {
                    $order = Order::find($row->order_id);
                    $order->status_id = 5;
                    $order->save();
                }
            }
        }
    }

    public function saveInvoice($invoiceParams, $orderIds)
    {
        $invoiceId = $invoiceParams['id'];
        $invoiceParams['status'] = 1;
        unset($invoiceParams['id']);
        $invoice = Invoice::find($invoiceId);
        if ($invoice === null) {
            $invoice = Invoice::create($invoiceParams);
        } else {
            $invoice->update($invoiceParams);
        }
        $invoiceId = $invoice->id;
        $invoiceOrderIds = InvoiceOrder::where('invoice_id', $invoiceId)->pluck('order_id')->toArray();
        foreach ($invoiceOrderIds as $invoiceOrderId) {
            if (!in_array($invoiceOrderIds, $orderIds)) {
                InvoiceOrder::where('invoice_id', $invoiceId)->where('order_id', $invoiceOrderId)->delete();
            }
        }
        foreach ($orderIds as $orderId) {
            if (!in_array($orderId, $invoiceOrderIds)) {
                InvoiceOrder::create([
                    'invoice_id' => $invoiceId,
                    'order_id' => $orderId
                ]);
            }
        }

        $this->setInvoiceTotals($invoiceId);

        Order::whereIn('id', $orderIds)->update([
            'status_id' => 4
        ]);
    }

    public function setInvoiceTotals($invoiceId)
    {
        $sql = "
            select i.id as invoice_id, io.order_id, sum(ot.invoice_amount) invoice_amount
            from invoices i
            inner join invoice_orders io on i.id = io.invoice_id
            inner join order_trackings ot on ot.order_id = io.order_id
            where i.id = ?
            group by i.id, io.order_id
        ";
        $invoiceTotal = 0;
        foreach (DB::select($sql, [$invoiceId]) as $row) {
            InvoiceOrder::where([
                'order_id' => $row->order_id,
                'invoice_id' => $row->invoice_id
            ])->update(['invoice_amount' => $row->invoice_amount]);
            $invoiceTotal += $row->invoice_amount;
        }
        Invoice::find($invoiceId)->update([
            'invoice_number' => $invoiceId,
            'invoice_amount' => $invoiceTotal
        ]);
    }

    public static function compileDetails($invoiceId)
    {

        $orders = [];

        $params = [$invoiceId];
        $sql = "
        select
            o.id as order_id,
            o.created_at as order_date,
            o.event, 
            o.confirmation_number, 
            o.from_first_name, 
            o.from_last_name            
        from invoice_orders io
        inner join orders o on o.id = io.order_id        
        where io.invoice_id = ?;
        ";
        foreach (DB::select($sql, $params) as $item) {
            $orders[$item->order_id] = $item;
        }

        $sql = "
        select
            o.id as order_id,
            s.shipper_name,
            ot.id tracking_id,
            ot.tracking_number, 
            ot.invoice_amount            
        from invoice_orders io
        inner join orders o on o.id = io.order_id
        inner join order_trackings ot on o.id = ot.order_id
        inner join shippers s on s.id = ot.shipper_id       
        where io.invoice_id = ?;
        ";
        $trackings = [];
        foreach (DB::select($sql, $params) as $item) {
            if (!array_key_exists($item->order_id, $trackings)) {
                $trackings[$item->order_id] = [];
            }
            $trackings[$item->order_id][] = $item;
        }

        $sql = "
        select
            o.id as order_id,            
            oi.id order_item_id,
            oi.item_type, 
            oi.ref, 
            oi.title, 
            oi.num_items
        from invoice_orders io
        inner join orders o on o.id = io.order_id        
        inner join order_items oi on o.id = oi.order_id
        where io.invoice_id = ?;
        ";
        $items = [];
        foreach (DB::select($sql, $params) as $item) {
            if (!array_key_exists($item->order_id, $items)) {
                $items[$item->order_id] = [];
            }
            $items[$item->order_id][] = $item;
        }
        $rows = [];
        foreach ($orders as $orderId => $order) {
            $maxRows = (count($items[$orderId]) > count($trackings[$orderId])) ?
                count($items[$orderId]) :
                count($trackings[$orderId]);
            for ($x = 0; $x < $maxRows; $x++) {
                if (array_key_exists($x, $trackings[$orderId])) {
                    $trackingShipper = $trackings[$orderId][$x]->shipper_name;
                    $trackingNumber = $trackings[$orderId][$x]->tracking_number;
                    $shippingCost = formatNumber($trackings[$orderId][$x]->invoice_amount, 2);
                } else {
                    $trackingShipper = "";
                    $trackingNumber = "";
                    $shippingCost = formatNumber(0, 2);
                }

                if (array_key_exists($x, $items[$orderId])) {
                    $itemRef = $items[$orderId][$x]->ref;
                    $itemTitle = $items[$orderId][$x]->title;
                    $numItems = formatNumber($items[$orderId][$x]->num_items, 0);
                } else {
                    $itemRef = "";
                    $itemTitle = "";
                    $numItems = "";
                }
                $rows[] = [
                    'orderId' => $orderId,
                    'refNumber' => $order->confirmation_number,
                    'itemDate' => formatDate($order->order_date),
                    'event' => $order->event,
                    'shipFrom' => $order->from_first_name . ' ' . $order->from_last_name,
                    'trackingShipper' => $trackingShipper,
                    'trackingNumber' => $trackingNumber,
                    'shippingCost' => $shippingCost,
                    'itemRef' => $itemRef,
                    'itemTitle' => $itemTitle,
                    'numItems' => $numItems,

                ];
            }
        }
        return $rows;
    }
}
