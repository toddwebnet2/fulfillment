<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Shipper;

class ShippersService
{

    public function getCompanyShippers($companyId)
    {
        $company = Company::find($companyId);
        return Shipper::where('shipper_group_code',
            ($company->shipper_group_code === null) ? 'DEFAULT' : $company->shipper_group_code
        )->orderBy('shipper_name')->pluck('shipper_name', 'id');
    }
}
