<?php

namespace App\Services;

use App\Models\Emails;
use Illuminate\Support\Facades\Mail;

class EmailService
{

    public static function deliverAllEmails()
    {

        $count = 0;
        foreach (Emails::where('sent_ind', 0)->get() as $email) {
            $count++;
            self::sendEmail(json_decode($email->to), $email->subject, $email->body);
            $email->completed();
        }
        return $count;

    }

    public static function sendEmail($to, $subject, $body)
    {

        Mail::send('email', ['content' => $body], function ($message) use ($to, $subject) {

            $message->subject($subject);
            foreach ($to as $t) {
                $message->to($t);
            }

        });
    }
}