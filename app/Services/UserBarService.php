<?php

namespace App\Services;

use App\Models\Order;

class UserBarService
{
    public static function getUserBarInfo()
    {
        $userInfo = AuthService::getUserInformation();
        return $userInfo->first_name . ' ' . $userInfo->last_name . ' at ' . $userInfo->company_name;
    }

    public static function getOrdersBar()
    {

        $data = [
            'orderList' => OrderService::getStatusBarCounts()
        ];
        return view('order.orderbar', $data);
    }
}