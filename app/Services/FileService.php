<?php

namespace App\Services;

use App\Models\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mimey\MimeTypes;

class FileService
{
    public static function saveFeiFile($fileId)
    {
        if ($fileId == 0) {
            return null;
        }
        $existingFile = self::getFeiFileIfExists($fileId);

        $ds = DIRECTORY_SEPARATOR;
        $uuid = uniqid() . uniqid() . uniqid() . uniqid();
        $tempPath = app_path("..{$ds}storage{$ds}app{$ds}tmp{$ds}") . $uuid;
        if ($existingFile === null) {
            $url = str_replace('{id}', $fileId, env('FEI_IMG_PATH', 'not_here'));
            if ($url == 'not_here') {
                abort('403', '.env var FEI_IMG_PATH does not exist');
            }
            file_put_contents($tempPath, fopen($url, 'r'));
            $newPath = self::storeFeiFile($fileId, $tempPath, $uuid);
            if (file_exists($tempPath)) {
                unlink($tempPath);
            }
            $tempPath = $newPath;
        } else {
            $tempPath = $existingFile;
            //copy($existingFile, $tempPath);
        }
        $ext = (new MimeTypes())->getExtension(mime_content_type($tempPath));
        $savePath = "{$ds}products{$ds}" . $uuid . '.' . $ext;
        $newPath = appPath('..' . $ds . 'public' . $savePath);
        copy($tempPath, $newPath);
        $image = Image::create(['path' => $savePath]);
        return $image->id;
    }

    private static function feiPath()
    {
        $folder = storage_path() . '/fei';

        if (!file_exists($folder)) {
            File::makeDirectory($folder);
        }

        $file = $folder . '/data.txt';
        if (!file_exists($file)) {
            file_put_contents($file, '');
        }
        return $folder;
    }

    public static function getFeiFileIfExists($fileId)
    {
        $feiPath = self::feiPath();
        $file = $feiPath . '/data.txt';
        foreach (explode("\n", File::get($file)) as $line) {
            if (strpos($line, '|')) {
                list($id, $path) = explode('|', $line);
                if ($fileId == $id) {
                    if (file_exists($path)) {
                        return $path;
                    } else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

    private static function storeFeiFile($fileId, $path, $filename)
    {
        $folder = self::feiPath();
        $newPath = $folder . '/' . $filename;
        copy($path, $newPath);
        $file = $folder . '/data.txt';
        File::append($file, "\n" . $fileId . "|" . $newPath);
        return $newPath;
    }

    public static function saveIt($tempPath, $url)
    {
        try {
            file_put_contents($tempPath, fopen($url, 'r'));
        } catch (\Exception $e) {
            print ("\nfail...\n");
            sleep(1);
            self::saveIt($tempPath, $url);
        }
    }

    public static function saveHttpFile($imageId, $key)
    {
        $uuid = uniqid() . uniqid() . uniqid() . uniqid();
        $tempPath = '/tmp/' . $uuid;
        $tempPath = Storage::putFile($tempPath, request()->file($key));
        $tempPath = app_path('../storage/app/' . $tempPath);

        $ext = (new MimeTypes())->getExtension(mime_content_type($tempPath));
        $savePath = '/products/' . $uuid . '.' . $ext;
        $newPath = appPath('../public' . $savePath);
        copy($tempPath, $newPath);
        if (file_exists($tempPath)) {
            unlink($tempPath);
        }
        if ($imageId == 0) {
            $image = Image::create(['path' => $savePath]);
            return $image->id;
        } else {
            $image = Image::find($imageId);
            $gooberPath = appPath('../public' . $image->path);
            if (file_exists($gooberPath)) {
                unlink($gooberPath);
            }

            $image->path = $savePath;
            $image->save();
            return $image->id;
        }
    }
}
