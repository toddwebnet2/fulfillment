<?php
namespace App\Services;

use App\Models\Order;
use App\Models\Shipper;
use Illuminate\Support\Facades\DB;

class TrackingService
{
    // http://www.erchov.com/ShipmentTracking.asp

    public static function getTrackingLink($trackingNumber, $shipperId)
    {
        switch (Shipper::find($shipperId)->tracking_code) {
            case "fedex":
                return self::buildFedexLink($trackingNumber);
                break;
            case "usps":
                return self::buildUSPSLink($trackingNumber);
                break;
            case "ups":
                return self::buildUPSLink($trackingNumber);
                break;
            default:
                return $trackingNumber;
        }
    }

    private static function buildFedexLink($num)
    {
        $link = "http://www.fedex.com/Tracking?action=track&tracknumbers={$num}";
        return "<a href='{$link}' target='_blank'>{$num}</a>";
    }

    private static function buildUSPSLink($num)
    {
        // $link = "http://www.stamps.com/shipstatus/submit/?confirmation={$num}";
        $link = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1={$num}";
        return "<a href='{$link}' target='_blank'>{$num}</a>";
    }

    private static function buildUPSLink($num)
    {
        $link = "https://wwwapps.ups.com/tracking/tracking.cgi?tracknum={$num}";
        return "<a href='{$link}' target='_blank'>{$num}</a>";
    }

    public static function getTrackingList($filter)
    {
        $params = [];
        $companyIds = implode(',', AuthService::getCurrentCompanyIds());
        if (strlen($filter) > 0) {
            $filter = "%" . $filter . "%";
            $where = " ot.tracking_number like ? ";
            $params[] = $filter;
        } else {
            $where = " ot.profit is null ";
        }
        $where .= " and o.company_id in ({$companyIds}) ";
        $sql = "
            select o.confirmation_number, o.created_at as order_date, s.shipper_name,
            ot.* 
            from 
            order_trackings ot
            inner join orders o on o.id = ot.order_id
            inner join shippers s on s.id = ot.shipper_id 
            where {$where}
            order by ot.tracking_number
        ";
        return DB::select($sql, $params);
    }

    public static function updateOrderStatus($trackingIds)
    {
        $qs = arrayQs($trackingIds);
        $sql = "
        select order_id, o.status_id, sum( case when profit is null then 1 else 0 end ) num_profited
        from order_trackings ot
        inner join orders o on o.id = ot.order_id
        where 
        ot.order_id in (
         select order_id from order_trackings where id in ({$qs})
        )
        group by order_id, status_id
        ";

        $orders = DB::select($sql, $trackingIds);
        foreach ($orders as $order) {
            if (!in_array($order->status_id, [3, 4])) {
                $statusId = ($order->num_profited == 0) ? 2 : 5;
                $orderObject = Order::find($order->order_id);
                $orderObject->status_id = $statusId;
                $orderObject->save();
            }
        }
    }
}
/*
\App\Services\TrackingService::updateOrderStatus([1]);
\App\Models\Order::find(31)->toArray()
*/
