<?php

namespace App\Services;

class SecurityService
{
    public static function getCompanyEmail($companyCode)
    {
        if (in_array($companyCode, AuthService::getCompanyCodeMap('CABOS'))) {
            return explode(';', env('LOSCABOS_EMAIL'));
        } else {
            return explode(';',
                (in_array($companyCode, getMexicos())) ? env('MEXICO_EMAILS') : env('ORDER_EMAIL')
            );
        }
    }
}
