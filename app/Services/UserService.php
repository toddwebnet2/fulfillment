<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Contact;
use App\Models\User;
use App\Models\UserRole;

class UserService
{

    public static function getUserList($companyId)
    {
        $users = [];
        foreach (User::where('company_id', $companyId)->where('status', 'valid')->orderBy('username')->get() as $user) {
            $tUser = swapIdArrayKey($user->toArray(), 'user_id');
            $contact = swapIdArrayKey($user->contact()->get()->toArray()[0], 'contact_id');
            $users[] = array_merge($tUser, $contact);
        }
        return $users;
    }

    public static function getUser($userId)
    {
        $user = User::find($userId);
        $tUser = swapIdArrayKey($user->toArray(), 'user_id');
        $contact = swapIdArrayKey($user->contact()->get()->toArray()[0], 'contact_id');
        return array_merge($tUser, $contact);
    }

    public static function getEmptyUser($companyId = null)
    {
        return [
            'user_id' => 0,
            'contact_id' => 0,
            'company_id' => $companyId,
            'user_role_id' =>  UserRole::where('code', 'USER')->pluck('id')[0],
            'username' => '',
            'password' => '',
            'first_name' => '',
            'last_name' => '',
            'tax_id' => '',
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'country' => '',
            'phone1' => '',
            'phone2' => '',
            'fax' => '',
            'email' => '',
            'url' => '',
            'image_id' => 0,
            'language' => 'en',
        ];
    }
}
