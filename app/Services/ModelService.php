<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

class ModelService
{
    public static function saveModel($model, $data)
    {
        if ($data['id'] == 0) {
            $model = new $model();
        } else {
            $model = $model::find($data['id']);
        }
        unset($data['id']);
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        $model->save();
        return $model;
    }
}