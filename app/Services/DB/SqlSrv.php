<?php

namespace App\Services\DB;


use Illuminate\Support\Facades\DB;

class SqlSrv extends DB
{
    public static function select()
    {
        return self::functionCall('select', func_get_args());
    }

    public static function table()
    {
        return self::functionCall('table', func_get_args());
    }

    private static function functionCall($function, $args)
    {

        switch (count($args)) {
            case 0:
                return self::connection('sqlsrv')->$function();
                break;
            case 1:
                return self::connection('sqlsrv')->$function($args[0]);
                break;
            case 2:
                return self::connection('sqlsrv')->$function($args[0], $args[1]);
                break;
            case 3:
                return self::connection('sqlsrv')->$function($args[0], $args[1], $args[2]);
                break;
            case 4:
                return self::connection('sqlsrv')->$function($args[0], $args[1], $args[2], $args[3]);
                break;

        }
    }
}