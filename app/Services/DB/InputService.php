<?php

namespace App\Services;

use Illuminate\Http\Request;

class InputService
{
    public static function collectInputFields(Request $request, $idKey = null, $explicitKeys = [])
    {
        $data = [];
        $excludedKeys = ['_token'];
        foreach ($request->input() as $key => $value) {
            if (
                (!in_array($key, $excludedKeys)) &&
                ($explicitKeys == [] || in_array($key, $explicitKeys))
            ) {

                if ($idKey == $key) {
                    $key = 'id';
                }
                $data[$key] = trim($value);
            }
        }

        return $data;
    }
}