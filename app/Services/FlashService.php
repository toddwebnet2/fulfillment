<?php

namespace App\Services;

use function Couchbase\defaultDecoder;

class FlashService
{
    public static function setFlashMessage($type, $content)
    {
        $request = request();
        $request->session()->put('flash.message', json_encode([
            'type' => $type,
            'content' => $content
        ]));
    }

    public static function getFlashMessage()
    {
        return session('flash.message');
    }

    public static function handleFlashMessage()
    {
        $flashMessage = self::getFlashMessage();
        if (!is_null($flashMessage) && trim($flashMessage) != '') {
            $flashObj = json_decode($flashMessage);
            $message = $flashObj->content;
            switch ($flashObj->type) {
                case "error":
                    $key = "danger";
                    $heading = __('message.error');
                    break;
                case "warning":
                    $key = "warning";
                    $heading = __('message.warning');
                    break;
                case "success":
                    $key = "success";
                    $heading = __('message.success');
                    break;
                case "info":
                    $key = "info";
                    $heading = __('message.info');
                default:
                    break;

            }
            self::killFlashMessage();
            return self::buildFlashMessageHtml($key, $heading, $message);
        }
        return '';
    }

    public static function killFlashMessage()
    {
        $request = request();
        $request->session()->forget('flash.message');
    }

    private static function buildFlashMessageHtml($key, $heading, $message)
    {
        return "<div class=\"alert alert-{$key}\">
        <strong>{$heading}:</strong> {$message}
        </div>";
    }
}