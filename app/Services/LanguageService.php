<?php

namespace App\Services;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;


class LanguageService extends Lang
{

    public static function getDefaultLanguage()
    {
        return Config::get('app.locale');
    }

    public static function getLanguage()
    {
        $lang = Cookie::get('fei-language');
        if (is_null($lang)) {
            $lang = self::getDefaultLanguage();
        }
        return $lang;
    }

    public static function setLanguage($lang)
    {
        Cookie::queue('fei-language', $lang, 45000);
    }

}