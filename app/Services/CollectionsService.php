<?php

namespace App\Services;

use App\Models\Bin;
use App\Models\Company;
use App\Models\Shipper;

class CollectionsService
{
    public static function getShippingCodes()
    {
        return [
            'DEFAULT',
            'COLUMBIA',
            'SPECIAL1'
        ];
    }

    public static function getStatusList()
    {
        return [
            0 => __('message.pending'),
            1 => __('message.incomplete'),
            2 => __('message.readyForInvoice'),
            3 => __('message.canceled'),
            4 => __('message.complete'),
            5 => __('message.pendingTracking'),

        ];
    }

    public static function getShippers()
    {
        return Shipper::all()->pluck('shipper_name', 'id')->toArray();
    }

    public static function getCompanies()
    {
        return Company::all()->pluck('company_name', 'id')->toArray();
    }

    public static function getIdList($obj)
    {
        $ret = [];
        foreach ($obj as $item) {
            $ret[] = $item->id;
        }
        return $ret;
    }

    public static function getCompanyList($companyId = null)
    {
        $companies = [];
        if ($companyId === null) {
            $companyCollection = Company::whereIn('id', AuthService::getCurrentCompanyIds())->get();
        } else {
            $companyCollection = Company::where('id', $companyId)->get();
        }
        foreach ($companyCollection as $key => $company) {
            $companies[$company->id] = $company->company_code . ' - ' . $company->company_name;
        }
        return $companies;
    }

    public static function getBins()
    {
        $r = [
            0 => (object)[
                'warehouse' => 'none',
                'name' => 'unknown',
            ],
        ];
        foreach (Bin::all() as $bin) {
            $r[$bin->id] = (object)[
                'warehouse' => $bin->warehouse,
                'name' => $bin->name,
            ];
        }
        return $r;
    }
}
