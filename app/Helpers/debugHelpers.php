<?php


function print_object($data)
{
    print "<div style=\"text-align:left\">";
    if (!empty($data)) :
        echo 'Code: <br />' . "\n";
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        echo '<br />--------------------';
    else :
        echo 'CODE: The object is empty';
    endif;
    print "</div>";
}

function print_rr()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        print_object($arg);
    }
}

function print_rd()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        print_object($arg);
    }
    die();
}

function print_rv()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        dump_object($arg);
    }
}

/*
    CREATE AN ARRAY OF ARRAYS TO LOOP THROUGH AND PRINT TO SCREEN
    SO DATA[ARRAY] => OBJECT , DATA[ARRAY] => OBJECT
*/

function print_multiple_objects($data)
{

    echo 'Code: <br />' . "\n";
    //used for counting
    $i = 1;

    foreach ($data as $array) {
        if (!empty($array)) :
            echo 'Array[' . $i . ']:<br />';
            echo '<pre>';
            print_r($array);
            echo '</pre>';
            echo '===================<br />';
            //increment
            $i = $i + 1;
        else :
            echo 'Array[' . $i . ']: The Object is empty. Moving On';
        endif;
    }
    //ending
    echo '<br />--------------------';

}

/*
    DUMP SINGLE OBJECT TO SCREEN
*/

function dump_object($data)
{
    print "<div style=\"text-align: left\">";
    echo 'Dump Code: <br />' . "\n";
    echo '<pre>';
    if (!empty($data)) :
        var_dump($data);
    else :
        echo 'The Object is empty';
    endif;
    echo '<pre>';
    echo '<br />----------------------';
    print "</div>";
}


/*
    CREATE AN ARRAY OF OBJECTS TO LOOP THROUGH AND DUMP TO SCREEN
    SO DATA[ARRAY] => OBJECT, DATA[ARRAY] => OBJECT
*/
function dump_multiple_objects($data)
{

    echo 'Code: <br />' . "\n";
    //used for counting
    $i = 1;

    foreach ($data as $array) {
        if (!empty($array)) :
            echo 'Array[' . $i . ']:<br />';
            echo '<pre>';
            var_dump($array);
            echo '<pre>';
            echo '===================<br />';
            //increment
            $i = $i + 1;
        else :
            echo 'Array[' . $i . ']: The Object is empty.';
        endif;
    }
    //ending
    echo '<br />--------------------';

}
