<?php


function hashEncrypt($data)
{
    return (new \App\Services\IdTransformer())->encodeId($data);
}

function hashDecrypt($data)
{
    return (new \App\Services\IdTransformer())->decodeId($data);
}

function swapIdArrayKey($array, $destinationKey)
{
    $array[$destinationKey] = $array['id'];
    unset($array['id']);
    return $array;
}

function arrayQs($array){
    return  implode(',', array_fill(0, count($array), '?'));
}
