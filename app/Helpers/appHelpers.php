<?php

function appPath($path)
{
    return app_path(str_replace('/', DIRECTORY_SEPARATOR, ltrim($path, '/')));
}

if (!function_exists('mime_content_type')) {
    function mime_content_type($filename)
    {
        $result = new finfo();

        if (is_resource($result) === true) {
            return $result->file($filename, FILEINFO_MIME_TYPE);
        }

        return false;
    }
}

function formatNumber($num, $decimals = 0)
{
    return number_format($num, $decimals, '.', ",");
}

function formatDate($date)
{
    return date('m/d/Y', strtotime($date));
}

function dbDate($date)
{
    return date('Y-m-d H:s:i', strtotime($date));
}

function buildCompiledAddress($address)
{
    $validKeys = [
        "from_first_name",
        "from_last_name",
        "from_company",
        "from_address1",
        "from_address2",
        "from_address3",
        "from_city",
        "from_state",
        "from_zip",
        "from_country",
        "from_phone1",
        "from_phone2",
        "from_fax",
        "from_email",
        "to_first_name",
        "to_last_name",
        "to_company",
        "to_address1",
        "to_address2",
        "to_address3",
        "to_city",
        "to_state",
        "to_zip",
        "to_country",
        "to_phone1",
        "to_phone2",
        "to_fax",
        "to_email",

    ];
    $fields = [];
    foreach (array_keys((array)$address) as $key) {
        if (in_array($key, $validKeys)) {
            $fields[] = $address->{$key};
        }
    }
    return implode(" ", $fields);
}

function app_url()
{
    return config('app.url');
}

function isLosCabos()
{
    return (bool)strpos(app_url(), 'cabos');
}

function getMexicos()
{
    $mexicos = [
        'TVGM-Mexico',
        'EXXCM',
        'HXMM',
        'LCCVBMEX',
        'GPXCM'
    ];
    return $mexicos;
}
