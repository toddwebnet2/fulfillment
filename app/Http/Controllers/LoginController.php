<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AuthService;
use App\Services\FlashService;
use App\Services\ShoppingCartService;
use Illuminate\Http\Request;

/**
 * Class LoginController
 * @package App\Http\Controllers
 *
 */
class LoginController extends Controller
{
    public function index()
    {
        return view("login");
    }

    public function login(Request $request)
    {

        $username = $request->input('username');
        $password = $request->input('password');
        if (!User::isValidLogin($username, $password)) {
            FlashService::setFlashMessage('error', __('message.invalidCredentials'));
        } else {
            AuthService::storeUserInSession($username);
        }
        if (AuthService::isAdminUser()) {
            ShoppingCartService::resetCart();
        }
        return redirect(app_url() .'/');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        FlashService::setFlashMessage('info', __('message.loggedOut'));
        return redirect(app_url() .'/');
    }
}
