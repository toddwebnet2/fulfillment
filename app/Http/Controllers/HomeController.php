<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Contact;
use App\Models\User;
use App\Models\UserRole;
use App\Services\EmailService;
use App\Services\FlashService;
use App\Services\InputService;
use App\Services\LanguageService;
use App\Services\AuthService;
use App\Services\MailGunnerService;
use App\Services\ModelService;
use App\Services\SecurityService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        if (AuthService::isLoggedIn()) {
            return redirect(app_url() . '/home');
        } else {
            return redirect(app_url() . '/login');
        }
    }

    public function template()
    {
        return view('oldtemp');
    }

    public function setLang($lang)
    {
        if (in_array($lang, ['en', 'es'])) {
            LanguageService::setLanguage($lang);
            App::setLocale($lang);
        }

        return redirect(request()->input('r'));
    }

    public function home()
    {
        $companyItemCount = Company::getCompanyProductCounts();
        $companies = [];
        $companyCodes = AuthService::getCurrentCompanyCodes();
        foreach (Company::wherein('company_code', $companyCodes)->orderBy('is_admin', 'desc')->orderBy('company_name')->get() as $company) {
            if ($company->id == 1 || $companyItemCount[$company->id] > 0) {
                $companies[hashEncrypt($company->id)] = $company->company_code . ' - ' . $company->company_name . ' - '
                    . $companyItemCount[$company->id] . ' ' . __('menu.product') . '(s)';
            }
        }

        if (isLosCabos()) {
            $pendingUserCount = User::wherein('note', ['Los Cabos'])->where('status', 'pending')->count();
        } else {
            $pendingUserCount = User::where('note', '!=', 'Los Cabos')->where('status', 'pending')->count();
        }

        $data = [
            'companyId' => hashEncrypt(AuthService::getCompanyId()),
            'company' => Company::find(AuthService::getCompanyId()),
            'companies' => $companies,
            'pendingUserCount' => $pendingUserCount,
        ];

        return view('home', $data);
    }

    public function translate(Request $request)
    {
        return __($request->input('c'));
    }

    public function create()
    {

        $user = UserService::getEmptyUser();
        $data = [
            'user' => $user,
            'companies' => Company::all()
        ];
        return view('create', $data);
    }

    public function createSave(Request $request)
    {
        $validator = $this->createValidate($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }

//        $companyId = $request->input('company_id');
        $company = Company::where('tax_id', $request->input('tax_id'))->first();
        if ($company === null) {
            $companyCode = 'FEIADMIN';
        } else {
            $companyCode = $company->company_code;
        }

        $userData = InputService::collectInputFields($request, 'user_id', [
            'user_id',
            'company_id',
            'username',
            'tax_id',
        ]);
        $userData['note'] = $request->input('warehouse');

        $userData['user_role_id'] = UserRole::where('code', 'USER')->pluck('id')[0];
        $contactData = InputService::collectInputFields($request, 'contact_id', [
            'company_id',
            'contact_id',
            'first_name',
            'last_name',
            'address1',
            'address2',
            'address3',
            'city',
            'state',
            'zip',
            'country',
            'email',
            'url',
            'phone1',
            'phone2',
            'fax',
        ]);
        if (!array_key_exists('company_id', $contactData)) {
            $contactData['company_id'] = null;
        }
        $contact = ModelService::saveModel(Contact::class, $contactData);
        $userData['contact_id'] = $contact->id;
        $userData['status'] = 'pending';
        $user = ModelService::saveModel(User::class, $userData);

        // saveModel doesn't work right with password encryption
        $user->password = $request->input('password');
        $user->save();

        $tos = SecurityService::getCompanyEmail($companyCode);
        if ($tos !== null) {
            $link = trim(env('APP_URL'), '/') . "/admin/users";
            foreach ($tos as $to) {
                MailGunnerService::addEmail(
                    $to,
                    'fulfillment@feifinancial.com',
                    "New Fulfillment User Request",
                    "A new user has requested to join fulfillment<BR><a href=\"{$link}\">{$link}</a>"
                );
            }
        }
        return redirect(app_url() . '/create/done');
    }

    public function createDone()
    {
        $data = [
            'title' => __('fei.createAccount'),
            'message' => __('message.userCreatedMessage')
        ];
        return view('message', $data);
    }

    public function createValidate(Request $request)
    {

        $errs = [];

        $companyId = $request->input('company_id');

        $userId = $request->input('user_id');
        $email = trim($request->input('email'));
        $userName = trim($request->input('username'));
        $taxId = trim($request->input('tax_id'));

        $contactId = $request->input('contact_id');
        $firstName = trim($request->input('first_name'));
        $lastName = trim($request->input('last_name'));
        $warehouse = trim($request->input('warehouse'));

        if (!is_numeric($companyId)) {
            $err[] = 'Invalid Company Id';
        }
        if (!is_numeric($userId)) {
            $err[] = 'Invalid User Id';
        }
        if (!is_numeric($contactId)) {
            $err[] = 'Invalid Contact Id';
        }
        if ($userName == "") {
            $errs[] = __('message.userNameBlank');
        } elseif (User::where('username', $userName)->where('id', '!=', $userId)->count() > 0) {
            $errs[] = __('message.userNameInUse');
        }

        if ($email == "") {
            $errs[] = __('message.emailNameBlank');
//        } elseif (Contact::where('id', '!=', $contactId)->where('email', $email)->where('id', '!=', $userId)->count() > 0) {
//            $errs[] = __('message.emailNameInUse');
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errs[] = __('message.emailIsInvalid');
        }

        if (trim($taxId) == '' || Company::whereRaw('lcase(tax_id) = ?', [trim(strtolower($taxId))])->count() == 0) {
            $errs[] = __('message.taxIdNotFound');
        }

        if ($firstName == "") {
            $errs[] = __('message.firstNameBlank');
        }
        if ($lastName == "") {
            $errs[] = __('message.lastNameBlank');
        }
        if ($warehouse == "") {
            $errs[] = __('message.warehouseBlank');
        }
        $fields = ['password', 'address1', 'city', 'state', 'zip', 'country', 'email', 'phone1'];
        foreach ($fields as $field) {
            if (trim($request->input($field)) == '') {
                $errs[] = __('company.' . $field) . ' ' . __('message.cannotBeBlank');
            }
        }

        return implode("\n", $errs);
    }

    public function forgot()
    {
        return view('forgot');
    }

    public function forgotPost(Request $request)
    {
        $username = $request->input('username');

        $sql = "
            select u.id as user_id
            from users u
            inner join contacts c on c.id = u.contact_id
            and u.is_active = 1
            and ? in (u.username, c.email)
        ";
        $params = [$username];
        $userIds = [];
        foreach (DB::select($sql, $params) as $item) {
            $userIds[] = $item->user_id;
        }

        if (count($userIds) > 0) {

            $users = User::whereIn('id', $userIds)->get();
            foreach ($users as $user) {
                $subject = __('message.yourPasswordIs');
                $body =
                    __('message.yourUsernameIs') . "<BR>" . $user->username . "<BR><BR>" .
                    __('message.yourPasswordIs') . "<BR>" . $user->password . "<BR><BR>" .
                    "<a href='\"" . env("APP_URL") . "\">" . env("APP_URL") . "</a>";
                MailGunnerService::addEmail($user->contact->email, "fulfillment@feifinancial.com", $subject, $body);
            }
        }
        FlashService::setFlashMessage('info', __('message.emailHasBeenSentWithPassword'));
        return redirect(app_url() . '/');
    }
}
