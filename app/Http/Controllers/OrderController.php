<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Order;
use App\Models\Shipper;
use App\Services\AuthService;
use App\Services\CollectionsService;
use App\Services\FlashService;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function list(Request $request)
    {

        $isAdmin = AuthService::isAdminUser();

        $request = request();
        $companyId = ($isAdmin) ? $request->input('companyId') : AuthService::getCompanyId();
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        if ($startDate == '') {
            $startDate = formatDate('now');
        }
        if ($endDate == '') {
            $endDate = formatDate('now - 30 days');
        }

        $start = "now";
        $end = "30 days ago";
        $basefilter = [];
        if (!AuthService::isAdminUser()) {
            $basefilter[] = ['company_id', AuthService::getCompanyId()];
        } elseif (is_numeric($companyId)) {
            $basefilter[] = ['company_id', $companyId];
        }
        //$basefilter[] = ['company_id', AuthService::getCurrentCompanyIds()];
        $companyIds = AuthService::getCurrentCompanyIds();

        $filterPending = $basefilter;
        $filterPending[] = ['status_id', 0];

        $filterIncomplete = $basefilter;
        $filterIncomplete[] = ['status_id', 1];

        $filterTracking = $basefilter;
        $filterTracking[] = ['status_id', 5];

        $filterPast = $basefilter;
        $filterPast[] = ['status_id', 2];
//        $filterPast[] = ['updated_at', '<', dbDate($startDate . ' + 1 day')];
//        $filterPast[] = ['updated_at', '>', dbDate($endDate)];

        $pendingOrders = Order::whereIn('company_id', $companyIds)->where($filterPending)->orderBy('created_at')->get();

        //$incompletOrders = Order::where($filterIncomplete)->orderBy('created_at')->get();
        $trackingOrders = Order::whereIn('company_id', $companyIds)->where($filterTracking)->orderBy('created_at')->get();

        $invoicableOrders = Order::whereIn('company_id', $companyIds)->where($filterPast)->orderBy('updated_at')->get();
        if ($isAdmin) {
            $companies = CollectionsService::getCompanyList();
        } else {
            $companies = CollectionsService::getCompanyList(AuthService::getCompanyId());
        }
        $data = [
            'pendingOrders' => $pendingOrders,
//            'incompleteOrders' => $incompletOrders,
            'trackingOrders' => $trackingOrders,
            'invoicableOrders' => $invoicableOrders,
            'orderCounts' => $this->deriveOrderCounts($pendingOrders, $trackingOrders, $invoicableOrders),
            'start' => formatDate($start),
            'end' => formatDate($end),
            'shippers' => CollectionsService::getShippers(),
            'companies' => $companies,
            'companyId' => $companyId,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'isAdmin' => $isAdmin,
        ];
        return view('order.list', $data);
    }

    public function canceledList()
    {
        $canceledOrders = Order::whereIn('company_id', AuthService::getCurrentCompanyIds())->where('status_id', 3)->orderBy('created_at')->get();
        $data = [
            'orders' => $canceledOrders
        ];

        return view('order.canceled_list', $data);
    }

    private function deriveOrderCounts($o1, $o2, $o3)
    {
        $ids = array_merge(
            CollectionsService::getIdList($o1),
            CollectionsService::getIdList($o2),
            CollectionsService::getIdList($o3)
        );
    }

    public function viewPrintable($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $data = [
            'orderView' => OrderService::getOrderHtml($orderId),
        ];
        return view('order.printable', $data);
    }

    public function viewPublic($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $data = [
            'orderView' => OrderService::getOrderHtml($orderId),
        ];
        return view('order.printable', $data);
    }

    public function view($orderId)
    {

        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);

        $data = [
            'order' => $order,
            'items' => $order->items,
            'status' => CollectionsService::getStatusList()[$order->status_id],
            'orderView' => OrderService::getOrderHtml($orderId),
        ];
        return view('order.view', $data);
    }

    public function sendBackToTracking($orderId){
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);
        $order->status_id = 5;
        $order->save();
        $sql = "update order_trackings set profit = null where order_id = {$order->id}";
        DB::update($sql);
        FlashService::setFlashMessage('info',"Order sent back to Tracking Status");
        return redirect('/order/view/' . hashEncrypt($orderId));
    }

    public function picklist($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);

        $data = [
            'order' => $order,
            'statusId' => $order->status_id,
            'picklist' => OrderService::getPicklistBins($order),
            'tracking' => $order->tracking,
            'orderView' => OrderService::getOrderHtml($orderId),

        ];
        return view('order.picklist', $data);
    }

    public function tracking($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);
        $companyCode = Company::find($order->company_id)->shipper_group_code;

        $data = [
            'order' => $order,
            'statusId' => $order->status_id,
            'picklist' => OrderService::getPicklistBins($order),
            'shippers' => Shipper::where('shipper_group_code', $companyCode)->orderBy('shipper_name')->pluck('shipper_name', 'id'),
            'tracking' => $order->tracking
        ];

        return view('order.tracking', $data);
    }

    public function finalizeTracking($orderId, Request $request)
    {
        $r = $request->input();
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);

        $trackings = [];
        $i = 0;
        while (true) {
            $i++;
            if (!array_key_exists('tracking_' . $i, $r)) {
                break;
            }
            if (trim($r['tracking_' . $i]) != '' || trim($r['shipper_' . $i]) != '') {

                $shipperId = trim($r['shipper_' . $i]);

                if ($shipperId == '') {
                    $shipperId = null;
                }
                $tracking = [
                    'tracking_number' => trim($r['tracking_' . $i]),
                    'shipper_id' => $shipperId,
                ];
                if (strlen($tracking['tracking_number']) > 0) {
                    $trackings[] = $tracking;
                }
            }
        }
        OrderService::updateOrderTracking($orderId, $trackings);
        FlashService::setFlashMessage('info', __('message.trackingSaved'));

        if ($order->status_id < 2) {
            $items = [];
            foreach (explode(',', $r['itemList']) as $item) {
                $items[$item] = [];
                $i = 0;
                while (true) {
                    $i++;
                    $key = 'item_' . $item . '_' . $i;
                    $binKey = 'item_bin_' . $item . '_' . $i;
                    if (!array_key_exists($key, $r)) {
                        break;
                    }
                    $items[$item][$r[$binKey]] = $r[$key];
                }
            }

            OrderService::updateOrderFulfillment($orderId, $items);
            OrderService::updateInventoryFromCompletedOrder($orderId);
            FlashService::setFlashMessage('info', __('message.orderMarkedAsCompletedAndInventoryUpdated'));
        }
        OrderService::fulfillOrderFulfillments($orderId);

        OrderService::sendOrderEmails($orderId);
        //return redirect('order/view/' . hashEncrypt($orderId));
        return redirect(app_url() . '/orders');
    }

    public function cancel($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);
        $order->status_id = 3;
        $order->save();
        OrderService::sendOrderEmails($orderId);
        FlashService::setFlashMessage('info', __('message.orderCanceled'));
        return redirect(app_url() . '/order/view/' . hashEncrypt($orderId));
    }

    public function invoice($orderId)
    {
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);

        $data = [
            'orders' => $order,
            'company' => $order->company,
            'user' => $order->user,
            'contact' => $order->user->contact
        ];
//        dump($order->toArray());
//        dump($order->user->contact->toArray());
//        dump($order->company->toArray());

        return view('order.invoice', $data);
    }

    public function addresses($direction, $companyId, $userId)
    {
        $data = [
            'direction' => $direction,
            'addresses' => OrderService::getOrderAddresses($direction, $companyId, $userId)
        ];
        return view('order.addresses', $data);
    }

    public function reset($orderId, $type){
        $orderId = hashDecrypt($orderId);
        $order = Order::find($orderId);
        $order->status_id = 0;
        $order->save();

        if($type == 'replenish'){
            OrderService::replenishOrder($order);
        }

        OrderService::refreshPicklistBins($order);
        FlashService::setFlashMessage('info', 'Order Resurrected');
        return redirect(app_url() . '/orders');
    }
}
