<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Item;
use App\Models\ItemType;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipper;
use App\Services\AuthService;
use App\Services\FlashService;
use App\Services\MailGunnerService;
use App\Services\OrderService;
use App\Services\ShippersService;
use App\Services\ShoppingCartService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function list($companyId)
    {
        $companyId = hashDecrypt($companyId);
        $request = request();
        $typeId = $request->input('typeId');

        $where = [
            ['company_id', $companyId]
        ];
        $itemTypes = ItemType::whereIn('id',[61,62,63])->orderBy('name')->pluck('name', 'id');
        if (is_numeric($typeId)) {
            $where[] = [
                'item_type_id', $typeId
            ];
        }
        $items = Item::where($where)->get();
        $itemIds = $items->pluck('id')->toArray();

        $itemcounts = Item::getItemInventorySums($itemIds);

        $data = [
            'companyId' => $companyId,
            'items' => $items,
            'itemCounts' => $itemcounts['available'],
            'itemInvCounts' => $itemcounts['inventory'],
            'itemTypes' => $itemTypes,
            'typeId' => $typeId,
            'shoppingCartItems' => ShoppingCartService::getShoppingCartItemCount($itemIds),
        ];
        return view('product.list', $data);
    }

    public function updateCart(Request $request)
    {
        $itemId = $request->input('id');
        $qty = $request->input('qty');
        if (!is_numeric($itemId) || !is_numeric($qty)) {
            FlashService::setFlashMessage('error', __('message.invalidData'));
            return '';
        }
        ShoppingCartService::saveToCart($itemId, $qty);
        FlashService::setFlashMessage('info', __('message.shoppingCartUpdated'));
        return '';
    }

    public function cartHeader()
    {
        $counts = ShoppingCartService::numCartItems();
        if ($counts['num_products'] == 0) {
            return '';
        }
        return view('product.cart_header', $counts);
    }

    public function viewCart()
    {

        $data = [
            'noViewCartLink' => 1,
            'cart' => $this->cartView(true)
        ];
        return view('product.cart', $data);
    }

    private function cartView($formView)
    {
        $cart = ShoppingCartService::getCart();
        $itemIds = [];
        foreach ($cart as $item) {
            $itemIds[] = $item->item_id;
        }
        $itemcounts = Item::getItemInventorySums($itemIds);

        $data = [
            'formView' => $formView,
            'width' => ($formView) ? '250' : '75',
            'cart' => $cart,
            'itemCounts' => $itemcounts['available'],
            'itemInvCounts' => $itemcounts['inventory'],
        ];
        return view('product.cart_contents', $data);
    }

    public function checkout()
    {
        $companyId = ShoppingCartService::getCartCompanyId();
        $companyCode = Company::find($companyId)->shipping_group_code;
        if ($companyCode == null) {
            $companyCode = 'DEFAULT';
        }
        $shipperService = new ShippersService();
        $data = [
            'cart' => $this->cartView(false),
            'shippers' => $shipperService->getCompanyShippers($companyId),
            'toHistory' => OrderService::getOrderAddresses('to', $companyId, AuthService::getUserId()),
            'fromHistory' => OrderService::getOrderAddresses('from', $companyId, AuthService::getUserId()),
        ];

        return view('product.checkout', $data);
    }

    public function validateCheckout(Request $request)
    {
        $errs = [];

        if ($request->input('shipper_id') == '') {
            $errs[] = __('message.needShipping');
        }
        $fields = [
            'first_name', 'last_name', 'company',
            'address1', 'city', 'state', 'zip',
            'country', 'email'
        ];
        foreach (['from', 'to'] as $prefix) {
            foreach ($fields as $field) {
                if (trim($request->input($prefix . '_' . $field)) == '') {
                    $errs[] = __('order.' . $prefix) . ' ' . __('order.' . $field) . ' ' . __('message.isRequired');
                } elseif ($field == 'email' && !filter_var($request->input($prefix . '_' . $field), FILTER_VALIDATE_EMAIL)) {
                    $errs[] = __('order.' . $prefix) . ' ' . __('order.' . $field) . ' ' . __('message.isInvalid');
                }
            }
        }
        return implode("\n", $errs);
    }

    public function checkoutFinalize(Request $request)
    {
        $formFields = $request->input();
        if (array_key_exists('_token', $formFields)) {
            unset($formFields['_token']);
        }
        $cart = [];
        foreach (ShoppingCartService::getCart() as $item) {
            $cart[] =
                [
                    'item_id' => hashEncrypt($item->item_id),
                    'num_items' => $item->num_items,
                    'item_type' => $item->item_type,
                    'ref' => $item->ref,
                    'title' => $item->title,
                ];
        }
        $formFields['cart'] = json_encode($cart);

        $data = [
            'cart' => $this->cartView(false),
            'shipper' => Shipper::find($formFields['shipper_id'])->shipper_name,
            'formFields' => $formFields,
        ];
        return view('product.finalize', $data);
    }

    public function checkoutFinal(Request $request)
    {
        $company = Company::find(AuthService::getCompanyId());
        $fields = ['shipper_id', 'event',
            'from_first_name', 'from_last_name', 'from_company',
            'from_address1', 'from_address2', 'from_address3',
            'from_city', 'from_state', 'from_zip',
            'from_country',
            'from_phone1', 'from_phone2', 'from_fax',
            'from_email',
            'to_first_name', 'to_last_name', 'to_company',
            'to_address1', 'to_address2', 'to_address3',
            'to_city', 'to_state', 'to_zip',
            'to_country',
            'to_phone1', 'to_phone2', 'to_fax',
            'to_email',
            'notes'];
        $order = [
            'user_id' => AuthService::getUserId(),
            'company_id' => $company->id
        ];
        foreach ($fields as $field) {
            $order[$field] = $request->input($field);
        }
        $cart = json_decode($request->input('cart'));
        if (count($cart) > 0) {
            $order['company_id'] = Item::find(hashDecrypt($cart[0]->item_id))->company_id;
        }
        $order = Order::create($order);
        $order->setConfirmationNumber();
        $orderId = $order->id;
        foreach ($cart as $item) {
            OrderItem::create([
                'order_id' => $orderId,
                'item_id' => hashDecrypt($item->item_id),
                'num_items' => $item->num_items,
                'item_type' => $item->item_type,
                'ref' => $item->ref,
                'title' => $item->title,
            ]);
        }
        $order->updateStatus();
        OrderService::sendOrderEmails($orderId);
        ShoppingCartService::resetCart();
        FlashService::setFlashMessage('info', __('message.orderProcessed'));
//        MailGunnerService::sendOrderEmail($orderId);
        return redirect(app_url() .'/');
    }

}
