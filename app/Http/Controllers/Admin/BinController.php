<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bin;
use App\Models\BinItem;
use App\Services\AuthService;
use App\Services\FlashService;
use Illuminate\Http\Request;

class BinController extends Controller
{
    public function index()
    {


        if (!isLosCabos()) {
            $bins = Bin::where('warehouse', '!=', 'LosCabos')->orderBy('warehouse', 'bin')->get()->toArray();
            $binCounts = Bin::getBinItemCounts();
        } else {

            $bins = Bin::wherein('warehouse', ['Unassigned', 'LosCabos'])->orderBy('warehouse', 'bin')->get()->toArray();
            $binCounts = Bin::getBinItemCounts();
        }
        $data = [
            'bins' => $bins,
            'binCounts' => $binCounts,
        ];
        return view('admin.bins.list', $data);
    }

    public function form($binId)
    {
        $binId = hashDecrypt($binId);

        if ($binId == 0) {
            $bin = (array)Bin::getEmptyBin();
        } else {
            $bin = Bin::find($binId)->toArray();
        }

        $data = [
            'bin' => swapIdArrayKey($bin, 'bin_id')
        ];
        return view('admin.bins.form', $data);
    }

    public function view($binId)
    {
        $binId = hashDecrypt($binId);

        $data = [
            'bin' => swapIdArrayKey(Bin::find($binId)->toArray(), 'bin_id')
        ];

        return view('admin.bins.view', $data);
    }

    private function validateCode($binId, $code)
    {
        return Bin::where('code', $code)->where('id', '!=', $binId)->count() == 0;
    }

    public function ajaxValidateCode($binId, $code)
    {
        return $this->validateCode($binId, $code) ? $code : 0;
    }

    public function validation(Request $request)
    {
        $errs = [];

        $binId = $request->input('bin_id');
        $warehouse = $request->input('warehouse');
        $name = $request->input('name');
        $code = $request->input('code');

//        if (!in_array($warehouse, ['Houston', 'Mexico'])) {
//            $errs[] = __('message.invalidWarehouse');
//        }
        if (strlen(trim($code)) == 0) {
            $errs[] = __('message.blankCode');
        } elseif (!$this->validateCode($binId, $code)) {
            $errs[] = __('message.codeInUse');
        }
        if (strlen(trim($name)) == 0) {
            $errs[] = __('message.blankName');
        } elseif (Bin::where('warehouse', $warehouse)->where('id', '!=', $binId)->where('name', $name)->count() > 0) {
            $errs[] = __('message.duplicateName');
        }

        return implode("\n", $errs);
    }

    public function save(Request $request)
    {

        $validator = $this->validation($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }
        $id = $request->input('bin_id');
        $data = [
            'warehouse' => $request->input('warehouse'),
            'name' => $request->input('name'),
            'code' => $request->input('code'),
        ];
        if ($id == 0) {
            $bin = Bin::create($data);
        } else {
            $bin = Bin::find($id);
            foreach ($data as $key => $value) {
                $bin->$key = $value;
            }
            $bin->save();
        }

        FlashService::setFlashMessage('info', __('message.binSaved'));
        return redirect(app_url() . '/admin/bin/view/' . hashEncrypt($bin->id));
    }

    public function productBinForm($companyId, $itemId)
    {
        $companyId = hashDecrypt($companyId);
        $itemId = hashDecrypt($itemId);
        $data = [
            'itemId' => $itemId,
            'companyId' => $companyId,
            'bins' => Bin::getAllBinsWithItems($itemId, AuthService::isFeiAdminUser()),
        ];
        return view('admin.bins.productForm', $data);
    }

    public function productBinValidate(Request $request)
    {
        $errs = [];
        $companyId = $request->input('company_id');
        $itemId = $request->input('item_id');
        $bins = $request->input('bins');
        if (!is_numeric($companyId)) {
            $err[] = 'Invalid Company Id';
        }
        if (!is_numeric($itemId)) {
            $err[] = 'Invalid Item Id';
        }
        foreach ($bins as $binId) {
            if (!is_numeric($request->input('bin_' . $binId))) {
                $errs[] = 'Bin: "' . $request->input('bin_label_' . $binId) . '" must contain a number';
            }
        }
        return implode("\n", $errs);
    }

    public function productBinFormSave(Request $request)
    {
        $validator = $this->productBinValidate($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }
        $companyId = $request->input('company_id');
        $itemId = $request->input('item_id');
        $bins = $request->input('bins');
        foreach ($bins as $binId) {
            $count = BinItem::where([
                ['bin_id', $binId],
                ['item_id', $itemId]
            ])->count();
            if ($count > 0) {
                $binItem = BinItem::where([
                    ['bin_id', $binId],
                    ['item_id', $itemId]
                ])->firstOrFail();

                $binItem->num_items = $request->input('bin_' . $binId);
                $binItem->save();
            } else {
                $binItem = new BinItem([
                    'bin_id' => $binId,
                    'item_id' => $itemId,
                    'num_items' => $request->input('bin_' . $binId)
                ]);
                $binItem->save();
            }
        }
        BinItem::where('num_items', 0)->delete();
        FlashService::setFlashMessage('info', __('message.binSaved'));
        return redirect(app_url() . '/admin/company/view/' . hashEncrypt($companyId));
    }

}
