<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\InvoiceService;
use App\Services\InvoicingService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoicingController extends Controller
{

    public function index()
    {
        $invoiceService = new InvoiceService();
        $invoiceService->checkUpdateTrackingReadyForInvocing();
        $data = [
            'companies' => (new InvoicingService())->getCompanyListWithOrderCount()
        ];
        return view('admin.invoicing.index', $data);
    }


    private function generateFields()
    {
        $map = [
            1 => 'A',
            2 => 'B',
            3 => 'C',
            4 => 'D',
            5 => 'E',
            6 => 'F',
            7 => 'G',
            8 => 'H',
            9 => 'I',
            10 => 'J',
        ];
        $arr = [
            'DateTimeFrom' => Carbon::now()->format('m/d/Y'),
            'DateTimeTo' => Carbon::now()->format('m/d/Y'),
        ];
        foreach ($map as $value) {
            $arr = array_merge($arr, [
                'Date' . $value => Carbon::now()->format('m/d/Y'),
                'Evento' . $value => "",
                'Attn' . $value => "",
                'TrackingID' . $value => "",
                'Company' . $value => "",
                'Address1' . $value => "",
                'Address2' . $value => "",
                'RateQuote' . $value => "",
                'AmountBilled' . $value => "",
                'Phone' . $value => ""
            ]);

        }
        return $arr;
    }

    public function generate()
    {
        return view('admin.invoicing.generate_form', ['form' => $this->generateFields()]);
    }

    public function generated(Request $request)
    {

        $form = $request->all();
        foreach (array_keys($this->generateFields()) as $key) {
            if (!isset($form[$key])) {
                $form[$key] = null;
            }
        }
dd($form);

    }
}
