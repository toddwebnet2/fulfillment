<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Contact;
use App\Models\User;
use App\Models\UserRole;
use App\Services\EmailService;
use App\Services\FlashService;
use App\Services\InputService;
use App\Services\MailGunnerService;
use App\Services\ModelService;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function add($companyId)
    {
        $companyId = hashDecrypt($companyId);
        $user = UserService::getEmptyUser($companyId);
        $data = [
            'company' => Company::find($companyId)->toArray(),
            'user' => $user,
            'companies' => Company::all(),
            'userRoles' => UserRole::all(),
        ];

        return view('admin.user.form', $data);
    }

    public function edit($companyId, $userId)
    {
        $companyId = hashDecrypt($companyId);
        $userId = hashDecrypt($userId);

        $user = UserService::getUser($userId);
        $data = [
            'company' => Company::find($companyId)->toArray(),
            'user' => $user,
            'companies' => Company::all(),
            'userRoles' => UserRole::all(),

        ];

        return view('admin.user.form', $data);
    }

    public function validation(Request $request)
    {

        $errs = [];

        $companyId = $request->input('company_id');

        $userId = $request->input('user_id');
        $userName = trim($request->input('username'));

        $contactId = $request->input('contact_id');
        $firstName = trim($request->input('first_name'));
        $lastName = trim($request->input('last_name'));

        if (!is_numeric($companyId)) {
            $err[] = 'Invalid Company Id';
        }
        if (!is_numeric($userId)) {
            $err[] = 'Invalid User Id';
        }
        if (!is_numeric($contactId)) {
            $err[] = 'Invalid Contact Id';
        }
        if ($userName == "") {
            $errs[] = __('message.userNameBlank');
        } elseif (User::where('username', $userName)->where('id', '!=', $userId)->count() > 0) {
            $errs[] = __('message.userNameInUse');
        }

        if ($firstName == "") {
            $errs[] = __('message.firstNameBlank');
        }
        if ($lastName == "") {
            $errs[] = __('message.lastNameBlank');
        }

        return implode("\n", $errs);
    }

    public function save(Request $request)
    {
        $validator = $this->validation($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }

        $companyId = $request->input('company_id');

        $userData = InputService::collectInputFields($request, 'user_id', [
            'user_id',
            'company_id',
            'username',
            'tax_id',
        ]);

        $userData ['user_role_id'] = (in_array('user_role_id', $request->keys())) ?
            $request->input('user_role_id') :
            UserRole::where('code', 'USER')->pluck('id')[0];

        $contactData = InputService::collectInputFields($request, 'contact_id', [
            'company_id',
            'contact_id',
            'first_name',
            'last_name',
            'address1',
            'address2',
            'address3',
            'city',
            'state',
            'zip',
            'country',
            'email',
            'url',
            'phone1',
            'phone2',
            'fax',
        ]);

        $contact = ModelService::saveModel(Contact::class, $contactData);
        $userData['contact_id'] = $contact->id;
        $user = ModelService::saveModel(User::class, $userData);

        // saveModel doesn't work right with password encryption
        $user->password = $request->input('password');
        $user->save();

        FlashService::setFlashMessage('info', __('message.userSaved'));

        return redirect(app_url() . '/admin/company/view/' . hashEncrypt($companyId));
    }

    public function pendingApproval()
    {
        $data = [
            'users' => User::where('status', 'pending')->get(),
            'companies' => Company::orderBy('is_admin', 'desc')->orderBy('company_name')->get(),

        ];

        return view('admin.user.pending', $data);
    }

    public function denyUser($userId)
    {
        $userId = hashDecrypt($userId);
        $user = User::find($userId);
        $user->status = 'rejected';
        $user->save();
        $from = "fulfillment@feifinancial.com";

        MailGunnerService::addEmail($user->contact->email, $from, __('message.userRejectMessage'), __('message.userRejectMessage'));
        FlashService::setFlashMessage('info', __('message.userSaved'));
        return redirect(app_url() . '/admin/users');
    }

    public function approveUser(Request $request)
    {
        $userId = hashDecrypt($request->input('user_id'));
        $companyId = hashDecrypt($request->input('company_id'));

        $user = User::find($userId);
        $user->company_id = $companyId;
        $user->status = 'valid';
        $user->save();
        $user->contact->company_id = $companyId;
        $user->contact->save();
        $message = __('message.userValidatedMessage');
        $a = explode('<BR>', $message);
        $subject = $a[0];
        $body = $message . "<BR>" . env('APP_URL');
        $from = "fulfillment@feifinancial.com";
        MailGunnerService::addEmail($user->contact->email, $from, $subject, $body);
        FlashService::setFlashMessage('info', __('message.userSaved'));
        return redirect(app_url() . '/admin/users');
    }

    public function delete($companyId, $userId)
    {

        $userId = hashDecrypt($userId);
        FlashService::setFlashMessage('warning', 'User Deleted');
        $user = User::find($userId);
        $user->is_active = FALSE;
        $user->save();
        return redirect(app_url() . 'admin/company/view/' . $companyId);
    }
}
