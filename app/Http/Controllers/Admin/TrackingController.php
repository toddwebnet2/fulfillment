<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderTracking;
use App\Services\TrackingService;

class TrackingController extends Controller
{
    public function index()
    {
        return view('admin.tracking.index');
    }

    public function list()
    {
        $request = request();
        $trackingFilter = trim($request->input('trackingFilter'));
        $data = [
            'trackings' => TrackingService::getTrackingList($trackingFilter)
        ];
        return view('admin.tracking.list', $data);
    }

    public function save()
    {
        $request = request();
        $fees = $request->input('fees');
        $invoiceAmounts = $request->input('invoice_amount');
        $costs = $request->input('cost');
        $profits = $request->input('profit');
        foreach ($profits as $id => $profit) {

            OrderTracking::find($id)->update([
                'cost' => $costs[$id],
                'fees' => $fees[$id],
                'invoice_amount' => $invoiceAmounts[$id],
                'profit' => $profit
            ]);
        }
        TrackingService::updateOrderStatus(array_keys($profits));
    }
}

