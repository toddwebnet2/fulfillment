<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FedEx;
use Illuminate\Http\Request;

class FedexController extends Controller
{
    public function index()
    {
        return view('admin.fedex.index');
    }

    public function save(Request $request)
    {
        $form = [];
        foreach (FedEx::getFillables() as $element) {
            if ($request->has($element)) {
                $form[$element] = $request->input($element);
            }
        }
        $fedex = FedEx::create($form);
        return redirect('/admin/fedex/print/' . $fedex->id);
    }

    public function print($fedexId)
    {
        $fedex = FedEx::find($fedexId);
        return view('admin.fedex.print', compact('fedex'));
    }

    public function list()
    {
        return view('admin.fedex.partials.list-content', [
            'fedexes' => FedEx::orderBy('created_at', 'desc')->take(50)->get()
        ]);

    }
}