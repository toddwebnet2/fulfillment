<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bin;
use App\Models\Company;
use App\Models\Invoice;
use App\Models\InvoiceOrder;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderTracking;
use App\Models\Shipper;
use App\Services\AuthService;
use App\Services\FlashService;
use App\Services\InputService;
use App\Services\InvoiceService;
use App\Services\ModelService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{

    public function index()
    {
        $includeZeros = request()->input('flag');
        if (!strlen(trim(request()->input('flag'))) > 0) {
            $includeZeros = 1;
        }

        if ($includeZeros != 1) {
            $includeZeros = 0;
        }

        if (AuthService::isFeiAdminUser()) {
            $companies = Company::orderBy('is_admin', 'desc')
                ->orderBy('company_name')
                ->get()
                ->toArray();
        } else {
            $companies = Company::wherein('id', AuthService::getCurrentCompanyIds())
                ->orderBy('is_admin', 'desc')
                ->orderBy('company_name')
                ->get()
                ->toArray();
        }
        $data = [
            'includeZeros' => (bool)$includeZeros,
            'companies' => $companies,
            'companyItemCount' => Company::getCompanyProductCounts(),
        ];

        return view('admin.company.list', $data);
    }

    public function view($id)
    {
        $id = hashDecrypt($id);
        $companyBins = [];
        foreach (Bin::getCompanyItemBins($id) as $item) {
            $companyBins[$item->item_id][] = $item;
        }
        $data = [
            'company' => swapIdArrayKey(Company::find($id)->toArray(), 'company_id'),
            'users' => UserService::getUserList($id),
            'products' => Item::where('company_id', $id)->get(),
            'companyBins' => $companyBins,
            'ordersReadyForInvoicing' => Order::where('company_id', $id)->where('status_id', 2)->count()
        ];
        return view('admin.company.view', $data);
    }

    public function add()
    {
        $data = [
            'company' => swapIdArrayKey((array)Company::getEmptyCompany(), 'company_id')
        ];

        return view('admin.company.form', $data);
    }

    public function edit($id)
    {
        $id = hashDecrypt($id);

        $data = [
            'company' => swapIdArrayKey(Company::find($id)->toArray(), 'company_id'),
        ];

        return view('admin.company.form', $data);
    }

    public function validation(Request $request)
    {

        $errs = [];
        $companyId = $request->input('company_id');
        $companyCode = trim($request->input('company_code'));
        $companyName = trim($request->input('company_name'));

        if (!is_numeric($companyId)) {
            $err[] = 'Invalid Company Id';
        }
        if ($companyCode == "") {
            $errs[] = __('message.companyCodeBlank');
        }
//        elseif (Company::where('company_code', $companyCode)->where('id', '!=', $companyId)->count() > 0) {
//            $errs[] = __('message.companyCodeInUse');
//        }

        if ($companyName == "") {
            $errs[] = __('message.companyNameBlank');
        } elseif (Company::where('company_name', $companyName)->where('id', '!=', $companyId)->count() > 0) {
            $errs[] = __('message.companyNameInUse');
        }

        return implode("\n", $errs);
    }

    public function save(Request $request)
    {
        $validator = $this->validation($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }

        $data = InputService::collectInputFields($request, 'company_id');
        $company = ModelService::saveModel(Company::class, $data);
        FlashService::setFlashMessage('info', __('message.companySaved'));
        return redirect('/admin/company/view/' . hashEncrypt($company->id));
    }

    public function invoices($id)
    {
        $id = hashDecrypt($id);

        $data = [
            'companyId' => $id,
            'company' => swapIdArrayKey(Company::find($id)->toArray(), 'company_id'),
            'pendingOrders' => Order::where('company_id', $id)->whereIn('status_id', [0, 1])->get(),
            'ordersReadyToInvoice' => Order::where('company_id', $id)->where('status_id', 2)->get(),
            'invoices' => Invoice::where('company_id', $id)->get(),
        ];

        return view('admin.company.invoices', $data);
    }

    public function startInvoice(Request $request, $id)
    {
        $orderIds = $request->input('order_ids');

        if (!is_array($orderIds) || count($orderIds) == 0) {
            FlashService::setFlashMessage('error', __('message.invalidData'));
            return redirect('/admin/company/' . $id . '/invoices');
        }

        $id = hashDecrypt($id);
        $company = Company::find($id);

        foreach ($orderIds as $key => $orderId) {
            $orderIds[$key] = hashDecrypt($orderId);
        }
        $orders = Order::whereIn('id', $orderIds)->get();

        $orderTracking = [];
        foreach (OrderTracking::wherein('order_id', $orderIds)->get() as $tracking) {
            if (!array_key_exists($tracking->order_id, $orderTracking)) {
                $orderTracking[$tracking->order_id] = [];
            }
            $orderTracking[$tracking->order_id][] = $tracking;
        }

        $firstOrder = $orders[0];

        $shippingCost = $handlingCost = $otherCost = $totalCost = 0;
        $invoiceItems = [];
        foreach ($orders as $order) {
            $invoiceItems[$order->id] = (object)[
                'shipping' => 0, 'handling' => 0, 'other' => 0, 'total' => 0,
            ];
            foreach ($order->tracking as $tracking) {
                $shippingCost += $tracking->cost;
                $invoiceItems[$order->id]->shipping += $tracking->cost;
                $totalCost += $tracking->cost;
                $invoiceItems[$order->id]->shipping += $tracking->cost;
            }
        }

        $fakeInvoiceObject = (object)[
            'id' => 0,
            'invoice_number' => '-- will generate on save --',
            'company_id' => $id,
            'first_name' => $company->first_name,
            'last_name' => $company->last_name,
            'address1' => $company->address1,
            'address2' => $company->address2,
            'address3' => $company->address2,
            'city' => $company->city,
            'state' => $company->state,
            'zip' => $company->zip,
            'country' => $company->country,
            'phone1' => $company->phone1,
            'phone2' => $company->phone2,
            'fax' => $company->fax,
            'email' => $company->email,
            'invoice_amount' => 0,
            'notes' => '',
            'status' => 'creating'
        ];

        $data = [
            'companyId' => $id,
            'company' => swapIdArrayKey(Company::find($id)->toArray(), 'company_id'),
            'orders' => $orders,
            'invoice' => $fakeInvoiceObject,
            'invoiceItems' => $invoiceItems,
            'orderTracking' => $orderTracking,
            'shippers' => Shipper::pluck('shipper_name', 'id'),
            'orderIds' => $orderIds
        ];

        return view('admin.company.invoiceForm', $data);
    }

    public function deleteInvoice($companyId, $invoiceId)
    {
        $invoiceId = hashDecrypt($invoiceId);
        $invoice = Invoice::find($invoiceId);

        Order::whereIn('id',
            InvoiceOrder::where('invoice_id', $invoiceId)->pluck('order_id')
        )->update(['status_id'=> 2]);

        $invoice->is_active = 0;
        $invoice->save();

        FlashService::setFlashMessage('info', __('message.invoiceSaved'));
        return redirect('admin/company/' . $companyId . '/invoices');
    }

    public function invoice($companyId, $invoiceId)
    {
        $companyId = hashDecrypt($companyId);
        $invoiceId = hashDecrypt($invoiceId);

        $orderIds = InvoiceOrder::where('invoice_id', $invoiceId)->pluck('order_id')->toArray();
        $orders = Order::whereIn('id', $orderIds)->get();
        $invoiceItems = [];
        foreach ($orders as $order) {
            $invoiceItems[$order->id] = (object)[
                'shipping' => 0, 'handling' => 0, 'other' => 0, 'total' => 0,
            ];
            foreach ($order->tracking as $tracking) {

                $invoiceItems[$order->id]->shipping += $tracking->cost;

                $invoiceItems[$order->id]->shipping += $tracking->cost;
            }
        }
        $orderTracking = [];
        foreach (OrderTracking::wherein('order_id', $orderIds)->get() as $tracking) {
            if (!array_key_exists($tracking->order_id, $orderTracking)) {
                $orderTracking[$tracking->order_id] = [];
            }
            $orderTracking[$tracking->order_id][] = $tracking;
        }

        $data = [
            'companyId' => $companyId,
            'invoiceId' => $invoiceId,
            'company' => swapIdArrayKey(Company::find($companyId)->toArray(), 'company_id'),
            'orders' => $orders,
            'invoice' => Invoice::find($invoiceId),
            'invoiceItems' => $invoiceItems,
            'orderTracking' => $orderTracking,
            'shippers' => Shipper::pluck('shipper_name', 'id'),
            'orderIds' => $orderIds
        ];

        return view('admin.company.invoiceView', $data);
    }

    public function printControl($companyId, $invoiceId)
    {
        $companyId = hashDecrypt($companyId);
        $invoiceId = hashDecrypt($invoiceId);

        $orderIds = InvoiceOrder::where('invoice_id', $invoiceId)->pluck('order_id')->toArray();
        $orders = Order::whereIn('id', $orderIds)->pluck('confirmation_number', 'id')->toArray();

        $invoiceItems = InvoiceOrder::where('invoice_id', $invoiceId)->get();

        $orderTracking = [];
        foreach (OrderTracking::wherein('order_id', $orderIds)->get() as $tracking) {
            if (!array_key_exists($tracking->order_id, $orderTracking)) {
                $orderTracking[$tracking->order_id] = [];
            }
            $orderTracking[$tracking->order_id][] = $tracking;
        }

        $data = [
            'companyId' => $companyId,
            'invoiceId' => $invoiceId,
            'company' => swapIdArrayKey(Company::find($companyId)->toArray(), 'company_id'),
            'orders' => $orders,
            'invoice' => Invoice::find($invoiceId),
            'invoiceItems' => $invoiceItems,

        ];

        return view('admin.invoicing.controlInfo', $data);
    }

    public function printDetail($companyId, $invoiceId)
    {
        $companyId = hashDecrypt($companyId);
        $invoiceId = hashDecrypt($invoiceId);

        $orderIds = InvoiceOrder::where('invoice_id', $invoiceId)->pluck('order_id')->toArray();
        $orders = Order::whereIn('id', $orderIds)->get();

        $invoiceItems = InvoiceOrder::where('invoice_id', $invoiceId)->get();

        $orderTracking = [];
        foreach (OrderTracking::wherein('order_id', $orderIds)->get() as $tracking) {
            if (!array_key_exists($tracking->order_id, $orderTracking)) {
                $orderTracking[$tracking->order_id] = [];
            }
            $orderTracking[$tracking->order_id][] = $tracking;
        }

        $data = [
            'companyId' => $companyId,
            'invoiceId' => $invoiceId,
            'company' => swapIdArrayKey(Company::find($companyId)->toArray(), 'company_id'),
            'orders' => $orders,
            'invoice' => Invoice::find($invoiceId),
            'invoiceItems' => $invoiceItems,
            'invoiceDetails' => InvoiceService::compileDetails($invoiceId),

        ];

        return view('admin.invoicing.controlDetails', $data);
    }

    public function saveInvoice(Request $request)
    {
        $companyId = $request->input('companyId');
        $invoice = [
            'id' => $request->input('invoiceId'),
            'invoice_number' => $request->input('invoiceId'),
            'company_id' => $companyId,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'address1' => $request->input('address1'),
            'address2' => $request->input('address2'),
            'address3' => $request->input('address3'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'zip' => $request->input('zip'),
            'country' => $request->input('country'),
            'phone1' => $request->input('phone1'),
            'phone2' => $request->input('phone2'),
            'fax' => $request->input('fax'),
            'email' => $request->input('email'),
            'notes' => $request->input('notes'),
        ];
        foreach ($invoice as $key => $value) {
            if ($value === null) {
                $invoice[$key] = '';
            }
        }
        $orderIds = $request->input('orderIds');
        $invoiceService = new InvoiceService();
        $invoiceService->saveInvoice($invoice, $orderIds);

        FlashService::setFlashMessage('info', __('message.invoiceSaved'));
        return redirect('admin/company/' . hashEncrypt($companyId) . '/invoices');
    }

    public function delete($companyId)
    {
        $companyId = hashDecrypt($companyId);
        FlashService::setFlashMessage('warning', 'Company Deleted');
        $company = Company::find($companyId);
        $company->is_active = FALSE;
        $company->save();
        return redirect('admin/company');
    }
}
