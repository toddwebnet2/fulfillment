<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Item;
use App\Models\ItemType;
use App\Services\FileService;
use App\Services\FlashService;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function form($companyId, $itemId)
    {
        $companyId = hashDecrypt($companyId);
        $itemId = hashDecrypt($itemId);
        if ($itemId == 0) {
            $item = Item::emptyItem($companyId);
            $image = Image::emptyImage();
        } else {
            $item = Item::find($itemId)->toArray();
            $imageObj = Image::find($item['image_id']);
            if ($imageObj) {
                $image = $imageObj->toArray();
            } else {
                $image = ['path' => ''];
            }
        }
        $data = [
            'item' => $item,
            'image' => $image,
            'itemTypes' => ItemType::orderby('name')->get()->toArray()
        ];
        return view('admin.product.form', $data);
    }

    public function validation(Request $request)
    {
        $errs = [];

        $itemId = $request->input('id');
        $companyId = $request->input('company_id');
        $ref = $request->input('ref');
        $itemTypeId = $request->input('item_type_id');
        $weight = $request->input('weight');
        $title = $request->input('title');
        $reorder = $request->input('reorder');
        $itemW = $request->input('item_w');
        $itemD = $request->input('item_d');
        $itemH = $request->input('item_h');

        $validImageExts = [
            'jpg', 'jpeg', 'gif', 'bmp', 'png',
        ];

        if (strlen(trim($request->input('image_v'))) > 0) {
            $ext = pathinfo($request->input('image_v'))['extension'];
            if (!in_array($ext, $validImageExts)) {
                $errs[] = __('message.invalidImageFormat');
            }
        }
        if (!is_numeric($itemId)) {
            $errs[] = 'Invalid Item Id';
        }
        if (!is_numeric($companyId)) {
            $errs[] = 'Invalid Company Id';
        }
        if (!is_numeric($itemTypeId)) {
            $errs[] = __('message.invalidItemTypeId');
        } elseif (ItemType::where('id', $itemTypeId)->count() === 0) {
            $errs[] = __('message.selectItemType');
        }
        if (strlen(trim($ref)) == 0) {
            $errs[] = __('message.blankRef');
        } elseif (Item::where('company_id', $companyId)->where('id', '!=', $itemId)->where('ref', $ref)->count() > 0) {
            $errs[] = __('message.duplicateRef');
        }
        if (strlen(trim($title)) == 0) {
            $errs[] = __('message.blankTitle');
        }
        if (strlen(trim($itemW)) > 0 && !is_numeric($itemW)) {
            $errs[] = __('message.nanWidth');
        }
        if (strlen(trim($itemD)) > 0 && !is_numeric($itemD)) {
            $errs[] = __('message.nanDepth');
        }
        if (strlen(trim($itemH)) > 0 && !is_numeric($itemH)) {
            $errs[] = __('message.nanHeight');
        }
        if (strlen(trim($weight)) > 0 && !is_numeric($weight)) {
            $errs[] = __('message.nanWeight');
        }
        if (strlen(trim($reorder)) > 0 && !is_numeric($reorder)) {
            $errs[] = __('message.nanReorder');
        }

        return implode("\n", $errs);
    }

    public function save(Request $request)
    {

        $validator = $this->validation($request);
        if ($validator != '') {
            abort('403', $validator);
            return;
        }
        $imageId = $request->input('image_id');
        if ($request->file('image')) {
            $imageId = FileService::saveHttpFile($request->input('image_id'), 'image');
        }
        $id = $request->input('id');
        $data = [
            'company_id' => $request->input('company_id'),
            'item_type_id' => $request->input('item_type_id'),
            'ref' => $request->input('ref'),
            'title' => $request->input('title'),
            'item_w' => $request->input('item_w'),
            'item_d' => $request->input('item_d'),
            'item_h' => $request->input('item_h'),
            'weight' => $request->input('weight'),
            'weight_unit' => $request->input('weight_unit'),
            'image_id' => ($imageId == 0) ? null : $imageId,
            'reorder' => $request->input('reorder'),
        ];
        if ($id == 0) {
            $item = Item::create($data);
        } else {
            $item = Item::find($id);
            foreach ($data as $key => $value) {
                $item->$key = $value;
            }
            $item->save();
        }

        FlashService::setFlashMessage('info', __('message.productSaved'));
        return redirect('/admin/company/view/' . hashEncrypt($data['company_id']));
    }

    public function destroy($companyId, $itemId)
    {
        $item = Item::find(hashDecrypt($itemId));
        $item->is_active = false;
        $item->save();
        FlashService::setFlashMessage('info', __('message.productSaved'));
        return redirect('/admin/company/view/' . $companyId);
    }
}
