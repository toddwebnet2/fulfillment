<?php

namespace App\Http\Middleware;

use App;
use App\Services\LanguageService;
use Closure;

class LangaugeSetter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        App::setLocale(LanguageService::getLanguage());
        return $next($request);
    }
}
