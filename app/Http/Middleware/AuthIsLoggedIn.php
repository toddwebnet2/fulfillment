<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use App\Services\FlashService;
use Closure;


class AuthIsLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!AuthService::isLoggedIn()) {
            FlashService::setFlashMessage('error', __("message.notLoggedIn"));
            return redirect(app_url() .'/');
        }
        return $next($request);
    }
}
