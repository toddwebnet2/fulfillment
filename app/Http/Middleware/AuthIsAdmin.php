<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use App\Services\FlashService;
use Closure;


class AuthIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!AuthService::isAdminUser()) {
            FlashService::setFlashMessage('error', 'Access is denied.');
            return redirect(app_url() .'/');
        }
        return $next($request);
    }
}
