<?php

namespace App\Models;

class ItemType extends ActiveModel
{
    protected $fillable = [
        'id',
        'name',
    ];

    public function getName()
    {
        return __($this->name);
    }
}
