<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFulfillment extends Model
{
    protected $fillable = [
        'order_id',
        'user_id',
        'item_id',
        'bin_id',
        'num_items',
        'fulfilled'
    ];
}
