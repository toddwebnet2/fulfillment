<?php

namespace App\Models;

class Language extends ActiveModel
{
    protected $fillable = ['code', 'name'];
}
