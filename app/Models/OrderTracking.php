<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTracking extends Model
{
    protected $fillable = [
        'order_id', 'tracking_number', 'descr', 'cost', 'fees', 'invoice_amount', 'profit', 'shipper_id'
    ];
}
