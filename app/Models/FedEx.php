<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FedEx extends Model
{
    protected $table = 'fedex';
    protected $fillable = [
        'tracking_id',
        'reference',
        'date',
        'recipient',
        'company',
        'address',
        'address2',
        'phone',
        'rate_quote',
        'billed_amount'
    ];

    public static function getFillables()
    {
        return (new self)->fillable;

    }

}