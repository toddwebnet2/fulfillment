<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Invoice extends ActiveModel
{

    protected $fillable = [
        'invoice_number',
        'company_id',
        'first_name',
        'last_name',
        'company',
        'address1',
        'address2',
        'address3',
        'city',
        'state',
        'zip',
        'country',
        'phone1',
        'phone2',
        'fax',
        'email',
        'invoice_amount',
        'notes',
        'status'
    ];

    public static function getNextInvoiceNumber()
    {
        $startingInvoiceNumber = 5000;
        $lastInvoice = self::max('invoice_number');
        if ($lastInvoice === null) {
            $lastInvoice = $startingInvoiceNumber;
        }

        $file = storage_path() . '/invoiceNumber.txt';
        if (!file_exists($file)) {
            File::put($file, $startingInvoiceNumber);
        }
        $fileInvoice = File::get($file);
        if (!is_numeric($fileInvoice)) {
            $fileInvoice = $startingInvoiceNumber;
        }
        $invoiceNum = max([$lastInvoice, $fileInvoice]) + 1;
        File::put($file, $invoiceNum);

        return $invoiceNum;
    }

    public function invoiceOrders()
    {
        return $this->hasMany(InvoiceOrder::class);
    }

    public function orders()
    {
        return $this->invoiceOrders->orders;
    }


}
