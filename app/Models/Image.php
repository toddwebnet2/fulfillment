<?php

namespace App\Models;


class Image extends ActiveModel
{
    protected $fillable = ['path'];

    public static function emptyImage()
    {
        return [
            'id' => 0,
            'path' => null,
        ];
    }
}
