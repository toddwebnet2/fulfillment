<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemBin extends Model
{
    protected $fillable = [
        'order_item_id', 'bin_id', 'num_items'
    ];
}