<?php

namespace App\Models;

use App\Services\AuthService;
use Illuminate\Support\Facades\DB;

class Bin extends ActiveModel
{

    protected $fillable = ['name', 'warehouse', 'code'];

    public static function getEmptyBin()
    {
        return (object)[
            'id' => 0,
            'name' => '',
            'warehouse' => '',
            'code' => '',
            'is_active' => true,
        ];
    }

    public static function getBinItemCounts()
    {

        $sql = "
        select bin_id, count(item_id) num_products, sum(num_items) num_items from (
            select b.id bin_id, bi.item_id, sum(bi.num_items) num_items from bins b 
            inner join bin_items bi on bi.bin_id = b.id
            inner join items i on i.id = bi.item_id
            where b.is_active = 1 and i.is_active = 1
            and i.company_id in (" .
            implode(',',AuthService::getCurrentCompanyCodes('id'))
            . ")
            group by b.id, bi.item_id
        ) as x
        group by bin_id
        ";
        $items = DB::select($sql);
        $binCounts = [];
        foreach ($items as $item) {
            $binCounts[$item->bin_id] = (object)[
                'num_products' => $item->num_products,
                'num_items' => $item->num_items,
            ];
        }
        return $binCounts;
    }

    public static function getCompanyItemBins($companyId)
    {
        $sql = "
            select i.id as item_id, b.id as bin_id, b.warehouse, b.name bin_name, sum(bi.num_items) as num_items
            from items i
            inner join bin_items bi on bi.item_id = i.id
            inner join bins b on b.id = bi.bin_id and b.is_active = 1
            where i.company_id = ? and i.is_active = 1 
            group by i.id, b.id, b.warehouse, b.name
            order by b.warehouse, b.name        
        ";
        $params = [$companyId];
        return DB::select($sql, $params);
    }

    public static function getAllBinsWithItems($itemId, $isFeiAdmin)
    {

        $moreSql = '';
        if (!$isFeiAdmin) {
            $moreSql = " where ( b.warehouse in ('Unassigned', 'LosCabos') or COALESCE(bi.num_items, 0) > 0)";
        }
        $sql = "
            select b.id bin_id, b.warehouse, b.name, COALESCE(bi.num_items, 0) num_items,
              case b.warehouse when 'Unassigned' then 0 else 1 end as orderId
            from bins b
            left outer join 
            (
                select item_id, bin_id, sum(num_items) num_items 
                from bin_items bi
                inner join items i on i.id = bi.item_id
                where i.id = ?
                group by item_id, bin_id
            )bi on bi.bin_id = b.id
            {$moreSql}
            order by orderId, b.warehouse, b.name        
        ";
        $params = [$itemId];
        $items = [];
        foreach (DB::select($sql, $params) as $item) {
            if (!array_key_exists($item->warehouse, $items)) {
                $items[$item->warehouse] = [];
            }
            $items[$item->warehouse][] = (object)[
                'bin_id' => $item->bin_id,
                'bin' => $item->name,
                'num_items' => $item->num_items
            ];
        }
        return $items;
    }
}
