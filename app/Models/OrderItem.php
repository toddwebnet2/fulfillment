<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $fillable = [
        'order_id', 'item_id', 'num_items',
        'item_type', 'ref', 'title'
    ];

    public function itemBins()
    {
        return $this->hasMany(OrderItemBin::class);
    }

    public function product(){
        return $this->belongsTo(Item::class);
    }

}