<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Item extends ActiveModel
{
    protected $fillable = [
        'company_id',
        'item_type_id',
        'ref',
        'title',
        'item_w',
        'item_d',
        'item_h',
        'weight',
        'weight_unit',
        'image_id',
        'reorder',
    ];

    public function image()
    {
        if ($this->image_id == 0) {
            return (object)['path' => '/images/spacer.gif'];
        } else {
            return Image::find($this->image_id);
        }
    }

    public function productImagePath()
    {
        return $this->image()->path;
    }

    public static function emptyItem($companyId)
    {
        return [
            'id' => 0,
            'company_id' => $companyId,
            'item_type_id' => 0,
            'ref' => '',
            'title' => '',
            'item_w' => '',
            'item_d' => '',
            'item_h' => '',
            'weight' => '',
            'weight_unit' => '',
            'image_id' => 0,
            'reorder' => 0,
        ];
    }

    public static function getItemInventorySums($itemIds)
    {
        if (count($itemIds) == 0) {
            return [
                'available' => [],
                'inventory' => [],
            ];
        }

        $strInValues = implode(',', array_fill(0, count($itemIds), '?'));
        $params = array_merge($itemIds,$itemIds);
        $sql = "
          select i.item_id, i.num_items inventory_items, i.num_items - coalesce(o.num_items, 0) num_items 
            from 
            (
                select item_id, sum(num_items) num_items 
                from bin_items
                where item_id in ({$strInValues})
                group by item_id
            ) i
            left outer join (
                select oi.item_id, sum(oi.num_items) as num_items from orders o 
                inner join order_items oi on oi.order_id = o.id
                where o.status_id = 0 and o.finalized = 0
                and item_id in ({$strInValues})
            group by oi.item_id
            ) o on o.item_id = i.item_id

        ";
//        print "<pre>";print_r($sql);print "</pre>";

        $itemsAvailable = [];
        $itemsInventory= [];

        foreach ($itemIds as $itemId) {
            $itemsAvailable[$itemId] = 0;
            $itemsInventory[$itemId] = 0;
        }
        foreach (DB::select($sql, $params) as $obj) {
            $itemsAvailable[$obj->item_id] = $obj->num_items;
            $itemsInventory[$obj->item_id] = $obj->inventory_items;
        }

        return [
            'available' => $itemsAvailable,
            'inventory' => $itemsInventory,
        ];
    }

    public function itemType()
    {
        return ItemType::find($this->item_type_id)->name;
    }

}
