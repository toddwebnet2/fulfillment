<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BinItem extends Model
{
    protected $fillable = ['bin_id', 'item_id', 'num_items'];

    public function bin()
    {
        return $this->belongsTo(Bin::class);
    }

}