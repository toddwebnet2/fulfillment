<?php

namespace App\Models;

class Shipper extends ActiveModel
{
    protected $fillable = [
        'shipper_group_code',
        'shipper_name',
        'tracking_code'
    ];
}
