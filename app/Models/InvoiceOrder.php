<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceOrder extends Model
{
    protected $fillable = ['invoice_id', 'order_id'];
    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
