<?php

namespace App\Models;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class User extends ActiveModel
{
    protected $fillable = [
        'contact_id',
        'company_id',
        'user_role_id',
        'username',
        'password',
        'is_active',
        'note'
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Crypt::encrypt($value);
    }

    public function getPasswordAttribute($value)
    {
        return Crypt::decrypt($value);
    }

    public static function isValidLogin($username, $password)
    {
        $users = User::where('username', $username)->where('status', 'valid')->select('password', 'company_id')->get();
        if (count($users) > 0) {
            if (Company::where('id', $users[0]->company_id)->count() > 0) {
                if ($users[0]->password == $password) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getUserIdFromUserName($userName)
    {
        return self::where('username', $userName)->pluck('id')->toArray()[0];
    }

    public static function getUserDetails($userId)
    {
        $sql = "
            select 
            u.id as user_id,
            ur.code as user_role,
            c.first_name, c.last_name,
            c.company_id,
            c.language, cc.company_code, 
            cc.company_name
            from users u
            inner join user_roles ur on ur.id = u.user_role_id
            inner join contacts c on c.id = u.contact_id
            inner join companies cc on cc.id = c.company_id
            where u.id = ? 
        ";
        $params = [$userId];
        $user = DB::select($sql, $params);
        return $user[0];
    }
}
