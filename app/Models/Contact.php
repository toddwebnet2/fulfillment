<?php

namespace App\Models;


class Contact extends ActiveModel
{
    protected $fillable = [
        'company_id',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'address3',
        'city',
        'state',
        'zip',
        'country',
        'phone1',
        'phone2',
        'fax',
        'email',
        'url',
        'image_id',
        'language',
        'is_active'
    ];
}
