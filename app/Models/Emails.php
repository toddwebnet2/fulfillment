<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $fillable = ['to', 'cc', 'from', 'subject', 'body'];

    public function completed()
    {
        $this->sent_ind = 1;
        $this->time_sent = now();
        $this->save();
    }
}