<?php

namespace App\Models;

use App\Services\OrderService;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'company_id',
        'shipper_id',
        'event',
        'from_first_name',
        'from_last_name',
        'from_company',
        'from_address1',
        'from_address2',
        'from_address3',
        'from_city',
        'from_state',
        'from_zip',
        'from_country',
        'from_phone1',
        'from_phone2',
        'from_fax',
        'from_email',
        'to_first_name',
        'to_last_name',
        'to_company',
        'to_address1',
        'to_address2',
        'to_address3',
        'to_city',
        'to_state',
        'to_zip',
        'to_country',
        'to_phone1',
        'to_phone2',
        'to_fax',
        'to_email',
        'notes'
    ];

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function tracking()
    {
        return $this->hasMany(OrderTracking::class);
    }

    public function setConfirmationNumber()
    {
        $confirmationNumber = trim(Company::find($this->company_id)->company_code) . '-' . hashEncrypt($this->id);
        $this->confirmation_number = $confirmationNumber;
        $this->save();
    }

    public function updateStatus()
    {
        $counts = OrderService::getFulfillmentCount($this->id);

        if ($counts->items_remaining <= 0) {
            $statusId = 2;
        } elseif ($counts->num_items == $counts->items_remaining) {
            $statusId = 0;
        } else {
            $statusId = 1;
        }

        if ($this->status_id == null || $this->status_id != $statusId) {

            $this->status_id = $statusId;
            $this->save();
        }
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
