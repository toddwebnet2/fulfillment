<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Company extends ActiveModel
{
    protected $fillable = [
        'company_code',
        'company_name',
        'quickbooks_list_id',
        'approve_system',
        'is_admin'
    ];

    public static function getEmptyCompany()
    {
        return (object)[
            'id' => 0,
            'company_code' => '',
            'company_name' => '',
            'first_name' => '',
            'last_name' => '',
            'tax_id' => '',
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'country' => '',
            'phone1' => '',
            'phone2' => '',
            'fax' => '',
            'email' => '',
            'quickbooks_list_id' => '',
            'approve_system' => false,
            'shipper_group_code' => null,
            'is_admin' => false,
            'is_active' => true,
        ];
    }

    public static function getCompanyProductCounts()
    {
        $dataObj = DB::select("
        SELECT c.id, count(i.id) as product_count 
        FROM companies c 
        LEFT OUTER JOIN items i on i.company_id = c.id and i.is_active = 1
        where c.is_active =1 
        group by c.id 
        ");
        $companyItemCount = [];
        foreach ($dataObj as $data) {
            $companyItemCount[$data->id] = $data->product_count;
        }
        return $companyItemCount;
    }

    public function getAttribute($key)
    {
        $attribute = parent::getAttribute($key);

        if ($key == "shipper_group_code") {
            if ($attribute == null) {
                $attribute = "DEFAULT";
            }
        }

        return $attribute;
    }

}
