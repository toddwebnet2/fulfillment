<?php
namespace App\Models\MailGunner;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $connection = 'mailgunner';
    protected $fillable = [
        'message_id', 'account_code', 'to', 'from', 'subject', 'body'
    ];

}
