<?php

namespace App\Console\Commands;

use App\Models\Bin;
use App\Models\BinItem;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Item;
use App\Models\ItemType;
use App\Models\Shipper;
use App\Models\User;
use App\Models\UserRole;
use App\Services\DB\SqlSrv;
use App\Services\FileService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Import extends Command
{
    protected $signature = 'util:import';

    protected $description = 'import data from sql server';

    public function handle()
    {

        Artisan::call('migrate:refresh');
        Artisan::call('db:seed');
        Artisan::call('util:imageCleanup');
        $this->importCompaniesAndUsers();
        $this->importItems();
        $this->cleanUpItemTypes();
        $this->importInventoryCounts();
        // Artisan::call('util:imageCleanup');
        // Artisan::call('util:fakeOrders');

        $this->updateTrackingCodes();
        print "\nCompleted.\n";
    }

    private function importItems()
    {

        print "Adding Item Types: \n";
        $itemTypes = SqlSrv::select('select * from FulfillmentItemType order by Id');
        foreach ($itemTypes as $itemType) {
            ItemType::create([
                'id' => $itemType->ID,
                'name' => $itemType->Description
            ]);
        }

        $newShippers = [];
        print "Adding Shippers: \n";
        $shippers = SqlSrv::select("
                        select
                            ContactID as ShipperID,
                            case ContactTypeID when 3 then 'DEFAULT'
                            when 8 THEN 'SHIPTO'
                            WHEN 9 THEN 'SHIPFROM'
                            WHEN 11 THEN 'COLUMBIA'
                            WHEN 12 THEN 'SPECIAL1'
                            END as ShipperGroup, CompanyName
                        from [FulfillmentContacts] c
                        where ContactTypeID in (3, 11, 12)
                        ");
        foreach ($shippers as $shipper) {
            $shipperObj = Shipper::create([
                'shipper_group_code' => $shipper->ShipperGroup,
                'shipper_name' => $shipper->CompanyName,
            ]);
            $newShippers[$shipper->ShipperID] = $shipperObj->id;
        }

        $companies = Company::all()->pluck('id', 'company_code')->toArray();
        print "Adding Items: \n";
        $products = SqlSrv::select('
            with cteGoober as
            (select i.itemID, cc.CompanyID, i.ItemRef from 
                FulfillmentContacts c
                INNER JOIN FulfillmentItems i ON c.ContactID = i.CustomerID 
                inner join FEiCustomers cc ON c.FEiID = cc.ID
            )
            
            select c.CompanyID, i.*
            from
             FulfillmentItems i
            inner join cteGoober g on g.ItemID = i.ItemID
            inner join FEICustomers c on c.CompanyID = g.CompanyID
            
            where 
            c.CompanyID is not null and i.active = 1                    
        ');
        $i = 0;
        $r = [];
        foreach ($products as $product) {
            $i++;
            print ".";
            if ($i % 80 == 0) {
                print "\n";
            }

            if (array_key_exists($product->CompanyID, $companies)) {
                $companyId = $companies[$product->CompanyID];
                $item = Item::create([
                    'company_id' => $companyId,
                    'item_type_id' => $product->ItemTypeID,
                    'ref' => $product->ItemRef,
                    'title' => $product->ItemDesc,
                    'item_w' => $product->ItemW,
                    'item_d' => $product->ItemD,
                    'item_h' => $product->ItemH,
                    'weight' => $product->Weight,
                    'weight_unit' => $product->WeightUnit,
                    // 'shipper_id' => $product->ItemTypeID,
                    'image_id' => FileService::saveFeiFile($product->ImageID),
                    'reorder' => $product->Reorder,
                ]);
                $r[$product->ItemRef] = $item->id;
            }
        }
        print "\n";
        return $r;
    }

    private function importCompaniesAndUsers()
    {
        $r = [];
        $adminRoleId = UserRole::where('code', 'ADMIN')->pluck('id')[0];
        $customerRoleId = UserRole::where('code', 'USER')->pluck('id')[0];

        $companies = self::collectCompanyData();
        foreach ($companies as $company) {
            print "Adding Company: " . $company->CompanyName . "\n";
            $comp = Company::create([
                'company_code' => $company->CompanyID,
                'company_name' => $company->CompanyName,
                'quickbooks_list_id' => ($company->QBListID === null) ? '' : $company->QBListID,
                'approve_system' => ($company->ApprovSystem == null) ? 0 : $company->ApprovSystem,
                'is_admin' => ($company->CompanyID == 'FEIADMIN')
            ]);

            $companyId = $comp->id;
            $r[$company->CompanyID] = $companyId;

            foreach ($company->users as $user) {
                if (User::where('username', $user->UserID)->count() == 0) {

                    print ".";
                    $userRoleId = ($user->CompanyID == 'FEIADMIN') ? $adminRoleId : $adminRoleId;
                    $contact = $user->contact;

                    $cont = Contact::create([
                        'company_id' => $companyId,
                        'first_name' => $contact->ContName,
                        'last_name' => $contact->ContLastName,
                        'address1' => $contact->ContAddress1,
                        'address2' => $contact->ContAddress2,
                        'address3' => $contact->ContAddress3,
                        'city' => $contact->ContCity,
                        'state' => $contact->ContState,
                        'zip' => $contact->ContZipCode,
                        'country' => $contact->ContCountry,
                        'phone1' => $contact->ContPhone1,
                        'phone2' => $contact->ContPhone2,
                        'fax' => $contact->ContFax,
                        'email' => $contact->ContEMail,
                        'url' => $contact->ContURL,
                        'image_id' => null,
                        'language' => 'en',
                        'is_active' => true,
                    ]);
                    $contactId = $cont->id;

                    $usr = User::create([
                        'contact_id' => $contactId,
                        'company_id' => $companyId,
                        'user_role_id' => $userRoleId,
                        'username' => $user->UserID,
                        'password' => $user->Password,
                        'is_active' => true
                    ]);
                }
            }
            print "\n";
        }
        return $r;
    }

    private static function getFeiCustomersSQl()
    {
        return " ( 
        select * from FEICustomers where CompanyID is not null 
        UNION 
        select 0, 'FEIADMIN', 'FEI Financial', '', '', 0
        ) ";
    }

    private static function collectCompanyData()
    {
        $feiCustomers = self::getFeiCustomersSQl();

        $customers = SqlSrv::select("
            select distinct c.*, case when c.CompanyID = 'FEIAdmin' then 0 else 1 end as orderID
            from {$feiCustomers} c             
            left outer join InternetSecurity s on s.CompanyID = c.CompanyID
            order by case when c.CompanyID = 'FEIAdmin' then 0 else 1 end, c.ID           
        ");

        $users = SqlSrv::select("
            select distinct s.*
            from {$feiCustomers} c             
            inner join InternetSecurity s on s.CompanyID = c.CompanyID              
        ");

        $contacts = SqlSrv::select("
            select distinct fc.*
            from {$feiCustomers} c             
            inner join InternetSecurity s on s.CompanyID = c.CompanyID
            inner join FulfillmentContacts fc on fc.ContactID = s.ContactID
              
        ");

        foreach ($customers as &$customer) {
            $customer->users = [];
            foreach ($users as $user) {
                if ($user->CompanyID == $customer->CompanyID) {
                    $newContact = null;
                    foreach ($contacts as $contact) {
                        if ($contact->ContactID == $user->ContactID) {
                            $newContact = $contact;
                            break;
                        }
                    }

                    $user->contact = ($newContact == null) ? self::emptyContact() : $newContact;
                    $customer->users[] = $user;
                }
            }
        }
        return $customers;
    }

    private static function emptyContact()
    {
        return (object)[
            'ContactID' => null,
            'FEiID' => null,
            'LinkID' => null,
            'ContactTypeID' => null,
            'LanguageID' => null,
            'CompanyName' => null,
            'EventName' => null,
            'ContName' => 'Unknown',
            'ContLastName' => 'Entity',
            'ContAddress1' => null,
            'ContAddress2' => null,
            'ContAddress3' => null,
            'ContCity' => null,
            'ContState' => null,
            'ContZipCode' => null,
            'ContCountry' => null,
            'ContPhone1' => null,
            'ContPhone2' => null,
            'ContFax' => null,
            'ContEMail' => null,
            'ContURL' => null,
            'ImageID' => null,
            'PhotoImageID' => null,
            'Active' => null,
            'TaxID' => null,
            'RCIID' => null,
            'RCIINT' => null,
            'RCIType' => null,
        ];
    }

    private function importInventoryCounts()
    {
        print "Adding Inventory Counts: \n";
        $companies = Company::all()->pluck('id', 'company_code')->toArray();

        $items = SqlSrv::select("
            with cteGoober as
            (select i.itemID, cc.CompanyID, i.ItemRef from 
                FulfillmentContacts c
                INNER JOIN FulfillmentItems i ON c.ContactID = i.CustomerID 
                inner join FEiCustomers cc ON c.FEiID = cc.ID
            )
            
            select distinct c.CompanyID, i.ItemId,i.ItemRef, QTY,
			PendingQty, AvailableQty
            from
             FulfillmentItems i
            inner join cteGoober g on g.ItemID = i.ItemID
            inner join FEICustomers c on c.CompanyID = g.CompanyID
			inner join FulfillmentItemList l on l.ItemID = i.ItemID
            
            where 
            c.CompanyID is not null and i.active = 1     
               
        ");

        $bin = Bin::create([
            'name' => 'Unassigned',
            'warehouse' => 'Unassigned',
            'code' => 0
        ]);
        $binId = $bin->id;
        $i = 0;
        foreach ($items as $item) {
            $i++;
            print ".";
            if ($i % 80 == 0) {
                print "\n";
            }
            if ($item->QTY > 0) {
                $companyId = $companies[$item->CompanyID];
                $itemId = Item::where('company_id', $companyId)
                    ->where('ref', $item->ItemRef)
                    ->firstOrFail()->id;
                BinItem::create([
                    'bin_id' => $binId,
                    'item_id' => $itemId,
                    'num_items' => $item->QTY
                ]);
            }
        }
        print "\n";
    }

    private function updateTrackingCodes()
    {
        Shipper::where('shipper_name', 'like', '%ups%')->update(['tracking_code' => 'ups']);
        Shipper::where('shipper_name', 'like', '%fedex%')->update(['tracking_code' => 'fedex']);
        Shipper::where('shipper_name', 'like', '%us postal%')->update(['tracking_code' => 'usps']);
    }

    private function cleanUpItemTypes()
    {
        $pm = ItemType::create([
            'name' => 'fei.printedMaterials'
        ]);
        $d = ItemType::create([
            'name' => 'fei.displays'
        ]);
        $pi = ItemType::create([
            'name' => 'fei.promotionalItems'
        ]);

        $newIds = [
            $pm->id,
            $d->id,
            $pi->id
        ];

        $pmItems = [1, 3, 4, 5, 6, 8, 12, 15, 16, 17, 18, 20, 31, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 51, 60];
        $piItems = [10];
        $dItems = [11, 13, 14, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 43, 44, 45, 46, 47, 48, 49, 50, 52, 53, 54, 55, 56, 57, 58, 59];

        Item::whereIn('item_type_id', $pmItems)->update(['item_type_id' => $pm->id]);
        Item::whereIn('item_type_id', $dItems)->update(['item_type_id' => $d->id]);
        Item::whereIn('item_type_id', $piItems)->update(['item_type_id' => $pi->id]);

        ItemType::whereNotIn('id', $newIds)->delete();
    }
}
