<?php

namespace App\Console\Commands;

use App\Models\Image;
use App\Models\Scopes\ActiveScope;
use Illuminate\Console\Command;

class ImageCleanup extends Command
{
    protected $signature = 'util:imageCleanup';

    protected $description = 'delete orphaned images';


    public function handle()
    {

        $dbFiles = Image::withoutGlobalScope(ActiveScope::class)->pluck('path')->toArray();
        $path = appPath('/../public/products');
        $files = scandir($path);
        $i = 0;
        foreach ($files as $file) {

            $filePath = '/products/' . $file;
            if (substr($file, 0, 1) != '.' && !in_array($filePath, $dbFiles)) {
                $i++;
                if (file_exists(appPath('/../public' . $filePath))) {
                    unlink(appPath('/../public' . $filePath));
                }
            }
        }
        print "\n\n {$i} files deleted.\n";

    }
}
