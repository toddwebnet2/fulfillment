<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Item;
use App\Models\ItemType;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderTracking;
use App\Models\Shipper;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FakeOrders extends Command
{
    protected $signature = 'util:fakeOrders';

    protected $description = 'dump and populate fake orders';

    public function handle()
    {

        print "Creating Fake Orders: \n";

        DB::statement("SET foreign_key_checks=0");

        OrderTracking::truncate();
        OrderItem::truncate();
        Order::truncate();
        DB::statement("SET foreign_key_checks=1");

        $itemSums = $this->getItems();
        $companyIds = Item::whereIn('id', array_keys($itemSums))
            ->whereIn('company_id',
                array_keys(User::all()->groupBy('company_id')->pluck('company_id')->toArray()
                )
            )
            ->groupBy('company_id')->pluck('company_id')->toArray();
        print "\n";
        for ($x = 0; $x < 50; $x++) {
            if ($x % 80 == 0 && $x > 0) {
                print "\n";
            }
            print ".";
            $this->populateOrder($companyIds);
        }
        print "\n";
        print "\ncompleted\n";
    }

    private function populateOrder($companyIds)
    {
        $itemSums = $this->getItems();
        $faker = Faker::create();
        $address1 = $this->getAddresss();
        $address2 = $this->getAddresss();
        $companyId = $companyIds[array_random(array_keys($companyIds))];
        $company = Company::find($companyId);
        $items = Item::where('company_id', $companyId)->inRandomOrder()->take(rand(10, 100))->get()->toArray();
        try {
            $userId = User::where('company_id', $companyId)->inRandomOrder()->take(1)->firstOrFail()->id;
        } catch (\Exception $e) {
            return;
        }
        $shipperCode = ($company->shipper_group_code == null) ? 'DEFAULT' : $company->shipper_group_code;
        $shipperId = Shipper::where('shipper_group_code', $shipperCode)->inRandomOrder()->take(1)->first()->id;

        $orderData = [
            'user_id' => $userId,
            'company_id' => $companyId,
            'shipper_id' => $shipperId,
            'from_first_name' => $faker->firstName(),
            'from_last_name' => $faker->lastName(),
            'from_company' => $faker->company(),
            'from_address1' => $address1->street,
            'from_address2' => null,
            'from_address3' => null,
            'from_city' => $address1->city,
            'from_state' => $address1->state,
            'from_zip' => $address1->zip,
            'from_country' => 'USA',
            'from_phone1' => $faker->phoneNumber,
            'from_phone2' => $faker->phoneNumber,
            'from_fax' => $faker->phoneNumber,
            'from_email' => $faker->email(),
            'to_first_name' => $faker->firstName(),
            'to_last_name' => $faker->lastName(),
            'to_company' => $faker->company(),
            'to_address1' => $address2->street,
            'to_address2' => null,
            'to_address3' => null,
            'to_city' => $address2->city,
            'to_state' => $address2->state,
            'to_zip' => $address2->zip,
            'to_country' => 'USA',
            'to_phone1' => $faker->phoneNumber(),
            'to_phone2' => $faker->phoneNumber(),
            'to_fax' => $faker->phoneNumber(),
            'to_email' => $faker->email(),
            'notes' => implode("\n", $faker->sentences())
        ];

        $order = Order::create($orderData);
        $order->save();
        $order->setConfirmationNumber();

        $orderId = $order->id;

        $i = 0;
        foreach ($items as $item) {
            $item = (object)$item;
            if (array_key_exists($item->id, $itemSums) && $itemSums[$item->id] > 0) {
                $i++;
                $itemType = ItemType::find($item->item_type_id);
                $numItems = rand(1, $itemSums[$item->id]);
                if($numItems>  $itemSums[$item->id])
                dd([$numItems, $itemSums[$item->id]]);
                $itemData = [
                    'order_id' => $orderId,
                    'item_id' => $item->id,
                    'num_items' => $numItems,
                    'item_type' => $itemType->name,
                    'ref' => $item->ref,
                    'title' => $item->title,
                ];
                OrderItem::create($itemData);
            }
        }
        if ($i > 0) {
            Order::find($orderId)->updateStatus();
        } else {
            Order::find($orderId)->delete();
        }
    }

    private function getItems()
    {
        $itemIds = Item::all()->pluck('id')->toArray();
        $sums = Item::getItemInventorySums($itemIds)['available'];
        foreach ($sums as $id => $sum) {
            if ($sum == 0) {
                unset($sums[$id]);
            }
        }
        return $sums;
    }

    private function getAddresss()
    {

        $faker = Faker::create();

        $address = explode("\n", $faker->address());
        $street = $address[0];
        $obj = explode(',', $address[1]);
        $city = $obj[0];
        $obj = explode(' ', trim($obj[1]));

        $state = $obj[0];
        $zip = $obj[1];
        return ((object)[
            'street' => $street,
            'city' => $city,
            'state' => $state,
            'zip' => $zip
        ]);

    }
}
