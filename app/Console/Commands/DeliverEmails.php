<?php

namespace App\Console\Commands;

use App\Services\EmailService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeliverEmails extends Command
{
    protected $signature = 'util:deliverEmails';

    protected $description = 'delivers unsent emails';

    public function handle()
    {
        $numEmails = EmailService::deliverAllEmails();
        if($numEmails > 0) {
            Log::info("{$numEmails} delivered.");
            }
    }

}