<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Services\FileService;
use App\Services\OrderService;
use App\Services\TrackingService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Test extends Command
{
    protected $signature = 'test';

    public function handle()
    {
        dd(User::find(1)->toArray());
    }
}
