<?php
if (array_key_exists('HTTPS', $_SERVER)) {
    $https = ( $_SERVER['HTTPS'] == "on") ? "https" : "http";
    $url = $https . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    if (strpos($url, app_url()) === false) {
        header("location: " . app_url() . "\n\n");
        exit();
    }
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('bootstrap');
});
*/

Route::get('/', 'HomeController@index');
// Route::get('/template', 'HomeController@template');
Route::get('/setLang/{lang}', 'HomeController@setLang');
Route::get('/translate', 'HomeController@translate');
Route::get('/forgot', 'HomeController@forgot');
Route::post('/forgot', 'HomeController@forgotPost');
Route::get('/create', 'HomeController@create');
Route::post('/create', 'HomeController@createSave');
Route::post('/create/v', 'HomeController@createValidate');
Route::get('/create/done', 'HomeController@createDone');

// login process
Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');
Route::get('/public/order/{orderId}', 'OrderController@viewPublic');
// locked to user
Route::group(['middleware' => [\App\Http\Middleware\AuthIsLoggedIn::class]], function () {
    Route::get('/home', 'HomeController@home');
    Route::get('/product/list/{companyId}', 'ProductController@list');
    Route::post('/product/list/{companyId}', 'ProductController@list');
    Route::post('/product/cart/save', 'ProductController@updateCart');
    Route::get('/product/cart/header', 'ProductController@cartHeader');
    Route::get('/product/cart/view', 'ProductController@viewCart');
    Route::get('/checkout', 'ProductController@checkout');
    Route::post('/checkout/v', 'ProductController@validateCheckout');
    Route::post('/checkout/finalize', 'ProductController@checkoutFinalize');
    Route::post('/checkout/checkout', 'ProductController@checkoutFinal');
    Route::get('/order/view/{orderId}', 'OrderController@view');
    Route::get('/order/back-to-tracking/{orderId}', 'OrderController@sendBackToTracking');
    Route::get('/order/view/{orderId}/printable', 'OrderController@viewPrintable');
    Route::get('/orders', 'OrderController@list');
    Route::post('/orders', 'OrderController@list');
    Route::get('/orders/canceled', 'OrderController@canceledList');
    Route::get('/order/address/{direction}/{companyId}/{userId}', 'OrderController@addresses');

    Route::get('/order/tracking/{orderId}', 'OrderController@tracking');
    Route::get('/order/reset/{orderId}/{type}', 'OrderController@reset');

    Route::get('/order/picklist/{orderId}', 'OrderController@picklist');
    Route::post('/order/tracking/finalize/{orderId}', 'OrderController@finalizeTracking');
    Route::get('/order/cancel/{orderId}', 'OrderController@cancel');
    Route::get('/order/invoice/{orderId}', 'OrderController@invoice');

    // locked to admin
    Route::group([
        'prefix' => '/admin',
        'middleware' => [\App\Http\Middleware\AuthIsAdmin::class]], function () {
        Route::get('/', 'Admin\AdminController@index');
        Route::get('/company', 'Admin\CompanyController@index');
        Route::get('/company/view/{id}', 'Admin\CompanyController@view');
        Route::get('/company/add', 'Admin\CompanyController@add');
        Route::get('/company/delete/{id}', 'Admin\CompanyController@delete');

        Route::get('/company/{id}/edit', 'Admin\CompanyController@edit');
        Route::get('/company/{id}/invoices', 'Admin\CompanyController@invoices');
        Route::post('/company/{id}/invoice/start', 'Admin\CompanyController@startInvoice');
        Route::post('/company/{id}/invoice/save', 'Admin\CompanyController@saveInvoice');
        Route::get('/company/{companyId}/invoice/{invoiceId}', 'Admin\CompanyController@invoice');
        Route::get('/company/{companyId}/invoice/{invoiceId}/cancel', 'Admin\CompanyController@deleteInvoice');
        Route::get('/company/{companyId}/invoice/{invoiceId}/control/print', 'Admin\CompanyController@printControl');
        Route::get('/company/{companyId}/invoice/{invoiceId}/detail/print', 'Admin\CompanyController@printDetail');

        Route::post('/company/v', 'Admin\CompanyController@validation');
        Route::post('/company', 'Admin\CompanyController@save');

        Route::get('/users', 'Admin\UserController@pendingApproval');
        Route::post('/users', 'Admin\UserController@approveUser');
        Route::get('/users/deny/{user_id}', 'Admin\UserController@denyUser');

        Route::get('/user/{companyId}', 'Admin\UserController@add');
        Route::get('/user/{companyId}/{userId}', 'Admin\UserController@edit');
        Route::get('/user/{companyId}/{userId}/delete', 'Admin\UserController@delete');
        Route::post('/user/v', 'Admin\UserController@validation');
        Route::post('/user', 'Admin\UserController@save');

        Route::get('/product/{companyId}/{itemId}', 'Admin\ProductController@form');
        Route::get('/product/{companyId}/{itemId}/delete', 'Admin\ProductController@destroy');
        Route::post('/product/v', 'Admin\ProductController@validation');
        Route::post('/product', 'Admin\ProductController@save');

        Route::get('/fedex', 'Admin\FedexController@index');
        Route::post('/fedex', 'Admin\FedexController@save');
        Route::get('/fedex/list', 'Admin\FedexController@list');
        Route::get('/fedex/print/{id}', 'Admin\FedexController@print');

        Route::get('/bins', 'Admin\BinController@index');
        Route::get('/bin/form/{binId}', 'Admin\BinController@form');
        Route::get('/bin/view/{binId}', 'Admin\BinController@view');
        Route::get('/bin/vcode/{binId}/{code}', 'Admin\BinController@ajaxValidateCode');
        Route::post('bin/v', 'Admin\BinController@validation');
        Route::post('bin', 'Admin\BinController@save');
        Route::get('/bin/items/product/form/{companyId}/{itemId}', 'Admin\BinController@productBinForm');
        Route::post('/bin/items/product/save/', 'Admin\BinController@productBinSave');
        Route::post('/bin/product/v', 'Admin\BinController@productBinValidate');
        Route::post('bin/items/save', 'Admin\BinController@productBinFormSave');

        Route::get('/tracking', 'Admin\TrackingController@index');
        Route::post('/tracking/list', 'Admin\TrackingController@list');
        Route::post('/tracking/save', 'Admin\TrackingController@save');

        Route::get('/invoice', 'Admin\InvoicingController@index');
        Route::get('/invoice/generate', 'Admin\InvoicingController@generate');
        Route::post('/invoice/generate', 'Admin\InvoicingController@generated');
    });
});
Route::get('/session', function () {
    dump(request()->session());
});


