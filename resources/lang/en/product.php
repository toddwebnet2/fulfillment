<?php
return [
    'itemType' => 'Item Type',
    'ref' => 'Reference Number',
    'title' => 'Title',
    'item_w' => 'Width',
    'item_d' => 'Depth',
    'item_h' => 'Height',
    'weight' => 'Weight',
    'weight_unit' => 'Weight Unit',
    'reorder' => 'Reorder',
    'image' => 'Image',
    'item_type' => 'Item type',

    'itemsInStock' => 'Item(s) in stock.',
    'itemsAvailable' => 'Item(s) available.',
    'cannotOrderThatMany' => 'Cannot order that many.',
    'getMoreStuff' => 'Please coordinate with FEI to stock more products.',
];
