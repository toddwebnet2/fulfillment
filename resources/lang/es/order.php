<?php
return [
    'orders' => 'Ordenes',
    'shipping' => 'Envío',
    'shipper' => 'Metodo de Envio',
    'first_name' => 'Nombre',
    'last_name' => 'Apellido',
    'address1' => 'Dirección 1',
    'address2' => 'Dirección 2',
    'address3' => 'Dirección 3',
    'city' => 'Ciudad',
    'state' => 'Estado',
    'zip' => 'Código Postal',
    'country' => 'País',
    'phone1' => 'Teléfono 1',
    'phone2' => 'Teléfono 2',
    'fax' => 'Fax',
    'email' => 'Email',
    'billing' => 'Facturación',
    'from' => 'De',
    'to' => 'Entregar a',
    'copyfromShipping' => 'Copiar',
    'notes' => 'Notas',
    'company' => 'Compañia',
    'orderConfirmationDescr' => 'Confirmacion de Orden No. de Orden',
    'customerNumber' => 'Número personalizado',
    'description' => 'DESCRIPCIÓN',
    'itemNumber' => 'Item #',
    'qty' => 'QTY',
    'shippingMethodSelected' => 'MÉTODO DE ENVÍO SELECCIONADO',
    'totalQuantity' => 'Cantidad total',
    'pendingOrders' => 'Ordenes pendientes',
    'incompleteOrders' => 'Ordenes incompletos',
    'trackingOrders' => 'Ordenes de Seguimiento',
    'readyForInvoice' => 'Listo para la Factura',
    'completedOrders' => 'Ordenes completados',
    'canceledOrders' => 'Ordenes canceladas',

    'finalizeFulfillment' => 'Finalizar el cumplimiento',
    'totalsMustEqual' => 'Los totales deben ser iguales',

    'tracking' => 'Rastreo',
    'addTracking' => 'Añadir rastreo',
    'trackingNumber' => 'El número de rastreo',
    'picklist' => 'Lista de selección',
    'processTracking' => 'Proceso de seguimiento',
    'resetOrder' => 'Restablecer orden',
    'orderStatusIs' => 'Orden:',

    'confirmation_number_abbrv' => 'Conf #',
    'invoices' => 'Números de control',
    'createInvoice' => 'Crear número de control',
    'orderNumber' => 'Número de orden',
    'orderDate' => 'Fecha de orden',

    'invoice' => 'Factura',
    'fees' => 'Costo Adicional',
    'cost' => 'Costo',
    'profit' => 'Ganancia',
    'event' => 'Evento',
    'previous' => 'Dirección anterior',
    'previousExplanation' => 'Copie la dirección de un pedido anterior o déjelo en blanco para una nueva dirección',
];
