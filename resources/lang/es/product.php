<?php
return [
    'itemType' => 'Tipo de artículo',
    'ref' => 'Número de referencia',
    'title' => 'Título',
    'item_w' => 'Ancho',
    'item_d' => 'Largo',
    'item_h' => 'Altura',
    'weight' => 'Peso',
    'weight_unit' => 'Unidad de peso',
    'reorder' => 'Reordenar',
    'item_type' => 'Tipo de artículo',

    'itemsInStock' => 'Artículo(s) en stock.',
    'itemsAvailable' => 'Artículo(s) disponible.',
    'cannotOrderThatMany' => 'No puedes pedir tantos.',
    'getMoreStuff' => 'Por favor coordine con FEI para almacenar más productos.',
];
