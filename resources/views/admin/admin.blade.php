@extends('../layouts.template')

@section('title',  __('fei.adminHeader') )

@section('body')
    <h2>{{ __('fei.adminHeader') }}</h2>

    <ol class="breadcrumb">
        <li class="breadcrumb-item active">{{ __("menu.companies") }}</li>

    </ol>
    <a href="/admin/company" class="btn btn-success float-right">{{ __('menu.add') }}</a>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th style="width:30px">&nbsp;</th>
            <th>{{ __('fei.companies') }}
                <span class="float-right">
                    @if ($includeZeros)
                        <a href="?flag=0">{{ __('menu.hideEmptyCompanies') }}</a>
                    @else
                        <a href="?flag=1">{{ __('menu.showEmptyCompanies') }}</a>
                    @endif
                </span>
            </th>
        </tr>
        </thead>
        @foreach($companies as $company)
            @if ($company['company_code'] == 'FEIADMIN' || ($includeZeros ||  $companyItemCount[$company['id']] > 0))
                <tr>
                    <td><a href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($company['id']) }}"
                           class="btn btn-primary">{{ __('menu.view') }}</a></td>
                    <td>{{ $company['company_code'] }} - {{ $company['company_name'] }}
                        <span class="float-right">{{ $companyItemCount[$company['id']] }} {{ __('menu.product') }}(s)</span>
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
@endsection
