
<table class="table-sm table table-bordered">
<tr>
    <th>Tracking</th>
    <th>Ref</th>
    <th>Recipient</th>
    <th>Date</th>
    <th>Created</th>
</tr>
    @foreach($fedexes as $fedex)
        <tr>
            <td>{{ $fedex->tracking_id }}</td>
            <td>{{ $fedex->reference }}</td>
            <td>{{ $fedex->recipient }}</td>
            <td>{{ date("m/d/Y", strtotime($fedex->date)) }}</td>
            <td>{{ date("m/d/Y", strtotime($fedex->created_at)) }}</td>
        </tr>

    @endforeach
</table>
