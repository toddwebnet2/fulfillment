<div id="listLoading" style="display: block;">
    <img src="/images/ajax_loader.gif" style="height: 30px"/> Loading...
</div>
<div id="listContent" style="display:none">Your Content</div>


<script>
    $(document).ready(function () {
        loadList();
    });

    function loadList() {
        $('#listLoading').show();
        $('#listContent').hide();

        jQuery.ajax({
            url: app_url() + '/admin/fedex/list',
            type: "GET",
            cache: false,
        }).done(function (data) {
            $('#listContent').html(data);
            $('#listLoading').hide();
            $('#listContent').show();
        });
    }
</script>