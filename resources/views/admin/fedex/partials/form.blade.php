<script>
    function validateFedexForm() {

        return true;
    }
</script>
<form
        action="<?=app_url()?>/admin/fedex"
        method="post"
        target="_blank"
        onsubmit="return validateFedexForm()"
>
    <table
            class="table table-bordered"
            style="width: 95%"
    >
        <tr>
            <th colspan="2">FedEx Form</th>
        </tr>
        <tr>
            <td><b>Tracking Number</b></td>
            <td><input type="text" class="form-control" name="tracking_id" id="tracking_id" maxlength="255"/></td>

        </tr>
        <tr>
            <td><b>Reference Number</b></td>
            <td><input type="text" class="form-control" name="reference" id="reference" maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Date</b></td>
            <td><input type="date" class="form-control" name="date" id="date" maxlength="50"/></td>
        </tr>
        <tr>
            <td><b>Company</b></td>
            <td><input type="text" class="form-control" name="company" id="company" maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Recipient</b></td>
            <td><input type="text" class="form-control" name="recipient" id="recipient" maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Address</b></td>
            <td><input type="text" class="form-control" name="address" id="address" maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Address 2</b></td>
            <td><input type="text" class="form-control" name="address2" id="address2" maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Rate Quote</b></td>
            <td><input type="number" min="0" step="any" class="form-control" name="rate_quote" id="rate_quote"
                       maxlength="255"/></td>
        </tr>
        <tr>
            <td><b>Billed Amount</b></td>
            <td><input type="number" min="0" step="any" class="form-control" name="billed_amount" id="billed_amount"
                       maxlength="255"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <input type="submit" class="btn-sm btn btn-primary" value="Save and Print"/>
            </td>
        </tr>


    </table>
</form>