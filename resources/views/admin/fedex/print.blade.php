<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    <title>FedEx Report </title>

</head>

<body>
<style type="text/css">
    <!--
    .show {
        display: block;
    }

    .hide {
        display: none;
    }

    -->
</style>
<div style="line-height: 0; height: 0px"></div>
<div style="line-height: 0; height: 0px">
    <table width="850" border="0" align="center" cellpadding="8" cellspacing="0">
        <tr>
            <td align="center" bgcolor="#FFFFFF">
                <p><img src="/images/fedex.gif" width="850" height="282"></p>
                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0"
                       style="border-color: #808080">
                    <tbody>
                    <tr>
                        <td width="177" valign="top">
                            <font size="2"> <b>Tracking number:</b><br>
                                <font face="Arial Narrow" color="#6600CC"><u>
                                        {{ $fedex->tracking_id }}
                                    </u></font>
                                <br>
                            </font>
                            <font size="2">
                                <b>Date:</b></font><font size="1">
                                {{ date('m/d/Y', strtotime($fedex->date)) }}
                                <br>
                            </font>
                        </td>
                        <td width="1" valign="top" background="/images/gray.gif">&nbsp;</td>

                        <td width="186" valign="top">
                            <font size="2">
                                <b>Recipient information:</b>
                                <br>
                                @if($fedex->recipient)
                                    <b>Attn: </b> {{ $fedex->recipient }}<br/>
                                @endif
                                @if($fedex->company)
                                    <b>Company:</b> {{ $fedex->company }}><br/>
                                @endif
                                <br/>
                                @if($fedex->address)
                                    {{ $fedex->address  }} <br/>
                                @endif
                                @if($fedex->address2)
                                    {{ $fedex->address2 }}<br/>
                                @endif


                            </font>
                        </td>

                        <td width="1" valign="top" background="/images/gray.gif">&nbsp;</td>

                        <td width="174" valign="top">
                            <font size="2">
                                <b>Courtesy rate quote:</b>${{number_format($fedex->rate_quote, 2) }}<br>
                                <b>Discounted rate: </b> ${{number_format($fedex->billed_amount, 2) }}<br>
                            </font></td>
                        <td width="1" valign="top" background="/images/gray.gif">&nbsp;</td>
                        <td valign="top">
                            <font size="2"><b>Your Reference:</b>{{ $fedex->tracking_id }}><br>
                            </font><br>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</div>
<script>
    setTimeout( function () {
        window.print();
    }, 1000);

</script>
</body>

</html>
