@extends('../layouts.template')

@section('title',  __('fei.adminHeader') . ' - ' . __('menu.fedex') )

@section('body')


    <div class="row container">
        <div class="col-md-6">
            <h1>{{ __('menu.fedex') }}</h1>
            @include('admin.fedex.partials.form')
        </div>
        <div class="col-md-6">
            <h1>History</h1>
            @include('admin.fedex.partials.list')
        </div>
    </div>
@endsection