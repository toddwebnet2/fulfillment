<form onsubmit="return false" id="trackingForm">
    <table class="table-striped table table-bordered">
        <tr>
            <th>{{ __('order.orderNumber') }}</th>
            <th>{{ __('order.orderDate') }}</th>
            <th>{{ __('order.tracking') }}</th>
            <th>{{ __('order.cost') }}</th>
            <th>{{ __('order.fees') }}</th>
            <th>{{ __('order.invoice') }}</th>
            <th>{{ __('order.profit') }}</th>

        </tr>
        <?php
        $tabIndex = 0;
        ?>
        @foreach($trackings as $tracking)
            <tr id="row_{{ $tracking->id }}"
                @if ($tracking->profit > 0)
                class="success"
                    @endif
            >
                <td><a href="<?=app_url()?>/order/view/{{ hashEncrypt($tracking->order_id) }}" target="_blank">
                        {{ $tracking->confirmation_number }}  - {{ $tracking->order_id }}
                    </a></td>
                <td>{{ formatDate( $tracking->order_date) }}</td>
                <td>{{ $tracking->tracking_number }}</td>
                <td><input onkeyup="updateProfit('{{ $tracking->id }}')"
                           onclick="updateProfit('{{ $tracking->id }}')"
                           name="cost[{{ $tracking->id }}]"
                           id="cost_{{ $tracking->id }}" type="number" pattern="\d*"
                           tabindex="<?=$tabIndex+1?>" value="{{ $tracking->cost }}"/></td>
                <td><input onkeyup="updateProfit('{{ $tracking->id }}')"
                           onclick="updateProfit('{{ $tracking->id }}')"
                           name="fees[{{ $tracking->id }}]"
                           id="fees_{{ $tracking->id }}" type="number" pattern="\d*"
                           tabindex="<?=$tabIndex+2?>" value="{{ $tracking->fees }}"/></td>
                <td><input onkeyup="updateProfit('{{ $tracking->id }}')"
                           onclick="updateProfit('{{ $tracking->id }}')"
                           name="invoice_amount[{{ $tracking->id }}]"
                           id="invoice_amount_{{ $tracking->id }}"
                           tabindex="<?=$tabIndex+3?>" type="number" pattern="\d*" value="{{ $tracking->invoice_amount }}"/></td>
                <td><input onfocus="this.blur()" name="profit[{{ $tracking->id }}]" id="profit_{{ $tracking->id }}"
                           type="number"
                           value="{{ $tracking->profit }}"/></td>

            </tr>
            <?php $tabIndex+=3;?>
        @endforeach
    </table>
</form>

<div class="" style="padding-bottom: 50px">
    <input type="button" onclick="saveTracking()" class="btn btn-primary" value="Save Tracking"/>
</div>
<script>
  function updateProfit(id) {
    cost = document.getElementById('cost_' + id).value;
    fees = document.getElementById('fees_' + id).value;
    invoice = document.getElementById('invoice_amount_' + id).value;
    if (fees == '') {
      document.getElementById('fees_' + id).value = 0;
      fees = 0;
    }
    if (!isNaN(invoice) && !isNaN(cost) && cost.trim() != '' && invoice.trim() != '') {
      profit = invoice - cost - fees;
      document.getElementById('profit_' + id).value = profit;
      $('#row_' + id).addClass('success');
    } else {

      document.getElementById('profit_' + id).value = '';
      $('#row_' + id).removeClass('success');
    }


  }
</script>
