@extends('../layouts.template')

@section('title',  __('fei.adminHeader') )

@section('body')
    <h2>{{ __('menu.tracking') }}</h2>
    <div class="row">
        <form onsubmit="return trackingSubmit()">
            <table align="center">
                <tr>
                    <td><b>Tracking Number Fitler</b></td>
                    <td><input type="text" name="tracking_number_search" id="tracking_number_search"/></td>
                    <td>
                        <button class="btn btn-primary" type="submit">Filter</button>
                    </td>
                    <td style="padding-left: 100px">
                        <button class="btn btn-warning" type="button" onClick="resetTracking()">All Pending Tracking Numbers</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="row" id="trackingListContent">


    </div>
    <script>
      $(document).ready(function () {
        loadTracking($('#tracking_number_search').val());
      });


    </script>
@endsection
