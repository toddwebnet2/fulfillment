@extends('../../layouts.template')

@section('title', __('fei.companyForm') )

@section('body')
    <hr/>
    <h3>{{ __('fei.company') }}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/admin/company">{{ __("menu.companies") }}</a></li>
        @if ($company['company_id'] > 0)
            <li class="breadcrumb-item"><a
                        href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($company['company_id']) }}">{{ $company['company_name'] }}</a>
            </li>
        @endif
        <li class="breadcrumb-item active">{{ __('fei.companyForm') }}</li>
    </ol>
    <form action="<?=app_url()?>/admin/company" method="post" id="companyForm" onsubmit="return companyForm_validate()">
        {{ csrf_field() }}
        {{ Form::hidden('company_id', $company['company_id']) }}
        {{ Form::hidden('quickbooks_list_id', $company['quickbooks_list_id']) }}

        <div class="row">

            <div class="col-md-6">

                <div class="form-group">
                    <label for="company_code">{{ __('company.company_code') }}</label>
                    <input class="form-control required" id="company_code" name="company_code"
                           value="{{ $company['company_code'] }}"/>
                </div>
                <div class="form-group">
                    <label for="company_name">{{ __('company.company_name') }}</label>
                    <input class="form-control required" id="company_name" name="company_name"
                           value="{{ $company['company_name'] }}"/>
                </div>

                <div class="form-group">
                    <label for="shipper_group_code">{{ __('company.shipper_group_code') }}</label>
                    <input class="form-control" id="shipper_group_code" name="shipper_group_code"
                           value="{{ $company['shipper_group_code'] }}"/>
                </div>

                <div class="form-group">
                    <label for="tax_id">{{ __('company.tax_id') }}</label>
                    <input class="form-control" id="tax_id" name="tax_id"
                           value="{{ $company['tax_id'] }}"/>
                </div>
                <H2>CONTACT INFORMATION</H2>
                @foreach([ 'first_name', 'last_name',  'phone1',
                'phone2',
                'fax',
                'email',] as $key)
                    <div class="form-group">
                        <label for="tax_id">{{ __('company.' . $key) }}</label>
                        <input class="form-control" id="<?=$key?>" name="<?=$key?>"
                               value="{{ $company[$key] }}"/>
                    </div>
                @endforeach


            </div>

            <div class="col-md-6">
                @foreach([
                'address1',
                'address2',
                'address3',
                'city',
                'state',
                'zip',
                'country',

                ] as $key)
                    <div class="form-group">
                        <label for="tax_id">{{ __('company.' . $key) }}</label>
                        <input class="form-control" id="<?=$key?>" name="<?=$key?>"
                               value="{{ $company[$key] }}"/>
                    </div>
                @endforeach

            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="text-align:right">
                @if ($company['company_id'] > 0)
                    <a href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($company['company_id']) }}"
                       class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                @else
                    <a href="<?=app_url();?>/admin/company"
                       class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                @endif
                &nbsp;
                <input type='submit' id="companyFormSubmit" class="btn btn-primary"
                       value="{{ __('menu.save') }}"/>
            </div>
        </div>
    </form>
    <br/>
@endsection
