@extends('../../layouts.template')

@section('title', "{$company['company_name']} - {$company['company_name']}" )

@section('body')
    <h2>{{ $company['company_name'] }} - {{ $company['company_code'] }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/admin/company">{{ __("menu.companies") }}</a></li>
        <li class="breadcrumb-item active">{{ $company['company_name'] }}</li>
    </ol>
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <table class="table  table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('message.companyInfo') }}</th>
                        <th><a href='<?=app_url();?>/admin/company/{{ hashEncrypt($company['company_id']) }}/edit'
                               class="btn btn-success float-right">{{ __('menu.edit') }}</a></th>
                    </tr>
                    </thead>

                    @foreach ($company as $field => $value)

                        @if (__('company.' . $field) != 'company.' . $field && $field != 'password')
                            <tr>
                                <td><b>{{ __('company.' . $field) }}</b></td>
                                <td>{{ $value }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('fei.users') }}</th>
                        <th><a href='<?=app_url();?>/admin/user/{{ hashEncrypt($company['company_id']) }}'
                               class="btn btn-success float-right">{{ __('menu.add') }}</a></th>
                    </tr>
                    </thead>
                    @foreach ($users as $user)
                        <tr>
                            <td><b>{{ $user['username'] }}</b> -
                                {{ $user['first_name'] }} {{ $user['last_name'] }}
                            </td>
                            <td>
                                <a href="<?=app_url();?>/admin/user/{{ hashEncrypt($company['company_id']) }}/{{ hashEncrypt($user['user_id']) }}"
                                   class="float-right">{{ __('menu.edit') }}</a>
                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td style="text-align: right">
                            <a class=" btn btn-warning"
                               href="<?=app_url();?>/admin/company/{{ hashEncrypt($company['company_id']) }}/invoices">Invoicing</a>
                        </td>
                        <td>{{ $ordersReadyForInvoicing }} - {{__('order.readyForInvoice')}}</td>
                    </tr>
                    </thead>
                </table>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th colspan="2">{{ __('fei.companyProducts') }}</th>
                        <th>
                            <a href="JavaScript:productForm({{ hashEncrypt($company['company_id']) }}, {{ hashEncrypt(0) }}, 1)"
                               class="btn btn-success float-right">{{ __('menu.add') }}</a></th>
                    </tr>
                    </thead>
                    @foreach ($products as $product)
                        <tr>
                            <td style="vertical-align: top; text-align: center"><img
                                        src="<?=app_url()?>{{ $product->productImagePath() }}" style="
                                        max-height: 40px; max-width: 80px
                            "/></td>
                            <td style="vertical-align: top; "><b>{{ $product->ref }} - {{ $product->title }}</b>

                                @if(array_key_exists($product->id, $companyBins))
                                    <br/>
                                    <?php
                                    $warehouse = '';
                                    $c = 0;
                                    foreach ($companyBins[$product->id] as $bin) {
                                        if ($warehouse != $bin->warehouse) {
                                            $warehouse = $bin->warehouse;
                                            $c = 0;
                                            print "<b>{$warehouse}: </b>";
                                        }
                                        print "<br/>&nbsp;&nbsp;&nbsp;&nbsp;";

                                        print "{$bin->bin_name} - " . formatNumber($bin->num_items);
                                    }
                                    ?>
                                @endif
                            </td>
                            <td style="vertical-align: top; ">
                                <a href="JavaScript: productForm({{ hashEncrypt($company['company_id']) }}, {{ hashEncrypt($product->id) }}, 0)"
                                   class="float-right">{{ __('menu.edit') }}</a>

                                <br>
                                <a href="JavaScript: productInventoryForm({{ hashEncrypt($company['company_id']) }}, {{ hashEncrypt($product->id) }})"
                                   class="float-right">{{ __('menu.binLocations') }}</a>


                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>
            @if($company['company_id']!=1)
                <div class="col-md-6" style="text-align:center;margin-bottom: 20px">
                    <input type="hidden" id="companyCodeValidator" value="{{ $company['company_code'] }}"/>
                    <button class="btn btn-danger" onclick="removeCompany('{{ hashEncrypt($company['company_id']) }}')">
                        Remove Company
                    </button>
                </div>
            @endif
        </div>
    </div>





@endsection
