@extends('../../layouts.template')

@section('title', "{$company['company_code']} - {$company['company_name']}" )

@section('body')
    <h2>{{ $company['company_name'] }} - {{ $company['company_code'] }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/admin/company">{{ __("menu.companies") }}</a></li>
        <li class="breadcrumb-item active">{{ $company['company_name']}}</li>
    </ol>


<a class="float-right btn btn-danger" href="JavaScript:endInvoice('{{$invoiceId}}', '{{ hashEncrypt($invoiceId) }}')">{{__('fei.rescindInvoice')}}</a>
    <a class="btn btn-warning" target="_blank"
       href="<?=app_url();?>/admin/company/{{ hashEncrypt($companyId) }}/invoice/{{ hashEncrypt($invoiceId) }}/control/print"> Control
        Sheet</a>
    <a class="btn btn-warning" target="_blank"
       href="<?=app_url();?>/admin/company/{{ hashEncrypt($companyId) }}/invoice/{{ hashEncrypt($invoiceId) }}/detail/print"> Control
        Detail</a>
    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label for="invoice_number">{{ __('invoice.invoice_number') }}</label>
                <input class="form-control required readonly" id="" name=""
                       value="{{ $invoice->invoice_number}}" readonly/>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-6">
            <h2>{{ __('invoice.billing_info') }}</h2>
            <div style="font-size:10px">({{ __('invoice.billingInfoSource') }})</div>

            <div class="form-group">
                <label for="first_name">{{ __('company.first_name') }}</label>
                <input class="form-control required" readonly id="first_name" name="first_name"
                       value="{{ $invoice->first_name }}"/>
            </div>
            <div class="form-group">
                <label for="last_name">{{ __('company.last_name') }}</label>
                <input class="form-control required" readonly id="last_name" name="last_name"
                       value="{{ $invoice->last_name }}"/>
            </div>

            <div class="form-group">
                <label for="email">{{ __('company.email') }}</label>
                <input class="form-control " readonly id="email" name="email"
                       value="{{ $invoice->email }}"/>
            </div>

            <div class="form-group">
                <label for="phone1">{{ __('company.phone1') }}</label>
                <input class="form-control " readonly id="phone1" name="phone1"
                       value="{{ $invoice->phone1 }}"/>
            </div>
            <div class="form-group">
                <label for="phone2">{{ __('company.phone2') }}</label>
                <input class="form-control " readonly id="phone2" name="phone2"
                       value="{{ $invoice->phone2 }}"/>
            </div>

            <div class="form-group">
                <label for="fax">{{ __('company.fax') }}</label>
                <input class="form-control " readonly id="fax" name="fax"
                       value="{{ $invoice->fax }}"/>
            </div>
        </div>
        <div class="col-md-6">
            <h2>&nbsp;</h2>

            <div class="form-group">
                <label for="address1">{{ __('company.address1') }}</label>
                <input class="form-control " readonly id="address1" name="address1"
                       value="{{ $invoice->address1 }}"/>
            </div>
            <div class="form-group">
                <label for="address2">{{ __('company.address2') }}</label>
                <input class="form-control " readonly id="address2" name="address2"
                       value="{{ $invoice->address2 }}"/>
            </div>
            <div class="form-group">
                <label for="address3">{{ __('company.address3') }}</label>
                <input class="form-control " readonly id="address3" name="address3"
                       value="{{ $invoice->address3 }}"/>
            </div>
            <div class="form-group">
                <label for="city">{{ __('company.city') }}</label>
                <input class="form-control " readonly id="city" name="city"
                       value="{{ $invoice->city }}"/>
            </div>
            <div class="form-group">
                <label for="state">{{ __('company.state') }}</label>
                <input class="form-control " readonly id="state" name="state"
                       value="{{ $invoice->state }}"/>
            </div>
            <div class="form-group">
                <label for="zip">{{ __('company.zip') }}</label>
                <input class="form-control " readonly id="zip" name="zip"
                       value="{{ $invoice->zip }}"/>
            </div>
            <div class="form-group">
                <label for="country">{{ __('company.country') }}</label>
                <input class="form-control " readonly id="country" name="country"
                       value="{{ $invoice->country }}"/>
            </div>
        </div>

        <hr/>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>{{__('menu.orders')}}</h2>
            <table class="table-striped table" style="width: 100%" width="100%" align="center" border="1">
                <tr>
                    <th>{{__('order.confirmation_number_abbrv')}} / {{__('menu.tracking')}}</th>
                    <th>{{__('order.cost')}} </th>
                    <th>{{__('order.fees')}}</th>
                    <th>{{__('order.invoice')}}</th>
                    <th>{{__('invoice.total')}}</th>
                </tr>
                <?php $row = 0;
                $totalTotalPrice = 0;
                ?>
                @foreach ($orders as $order)
                    <?php
                    $totalPrice = 0;
                    foreach ($orderTracking[$order->id] as $tracking) {
                        $totalPrice += $tracking->invoice_amount;
                        $totalTotalPrice += $tracking->invoice_amount;
                    }?>

                    <tr>
                        <td style="text-align:left">{{ $order->confirmation_number }} - {{ $order->id }}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="text-align:right">
                            <b>${{ formatNumber($totalPrice, 2) }}</b>
                        </td>


                    </tr>
                    @foreach($orderTracking[$order->id] as $tracking)
                        <tr>
                            <td style="padding-left: 20px">
                                <?=$shippers[$tracking->shipper_id]?>
                                -
                                <?=$tracking->tracking_number?>
                            </td>
                            <td>${{ formatNumber($tracking->cost, 2) }}</td>
                            <td>${{ formatNumber($tracking->fees, 2) }}</td>
                            <td>${{ formatNumber($tracking->invoice_amount, 2) }}</td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    <?php $row++;?>

                @endforeach
                <tr style="max-height: 5px;padding: 0px;">
                    <th colspan="5" style="font-size: 6px;padding: 0px;">&nbsp;</th>
                </tr>
                <tr>
                    <td style="text-align:left">{{ __('invoice.total') }}
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="text-align:right">
                        <b>${{ formatNumber($totalTotalPrice, 2) }}</b>
                    </td>


                </tr>
            </table>

        </div>
        <h2>{{__('order.notes')}}</h2>
        <textarea readonly name="notes" style="width:100%; height: 100px">{{ $invoice->notes }}</textarea>
    </div>

<script type="application/javascript">
    function endInvoice(){
      if(prompt("{{__('fei.confirmControlNumber')}}") == "{{ $invoiceId }}"){
        document.location="<?=app_url()?>/admin/company/{{hashEncrypt($companyId)}}/invoice/{{hashEncrypt($invoiceId)}}/cancel";
      }
    }
</script>
@endsection
