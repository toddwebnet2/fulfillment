@extends('../../layouts.template')

@section('title', "{$company['company_code']} - {$company['company_name']}" )

@section('body')
    <h2>{{ $company['company_name'] }} - {{ $company['company_code'] }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/company">{{ __("menu.companies") }}</a></li>
        <li class="breadcrumb-item active"><a
                    href="/admin/company/view/{{ hashEncrypt($company['company_id']) }}">{{ $company['company_name'] }}</a>
        </li>
        <li class="breadcrumb-item">{{ __('menu.invoicing') }}</li>
    </ol>
    <div class="row">
        <div class="col-md-3">

            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>{{ __('order.pendingOrders') }}</th>

                    </thead>
                </tr>

                @if (count($pendingOrders) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($pendingOrders as $order)
                    <tr>
                        <td>
                            <a href="<?=app_url()?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }} - {{ $order->id }}</a>
                            - {{ formatDate($order->updated_at) }}
                        </td>
                    </tr>
                @endforeach
            </table>

        </div>
        <div class="col-md-6">
            <form action="<?=app_url()?>/admin/company/{{ hashEncrypt($companyId) }}/invoice/start" method="post">
                <table class="table table-striped">
                    <tr>
                        <thead>
                        <th>{{ __('message.readyForInvoice') }}

                            @if (count($ordersReadyToInvoice) > 0)
                                <span style="float:right"> <a href="JavaScript:selectAll('orderIds')"
                                                              style="color:#fff">{{ __('message.checkAll') }}</a></span>
                            @endif
                        </th>

                        </thead>
                    </tr>

                    @if (count($ordersReadyToInvoice) == 0)
                        <tr>
                            <td> 0 {{ __('fei.records') }}</td>
                        </tr>
                    @else
                        @foreach($ordersReadyToInvoice as $order)
                            <tr>
                                <td>
                                    <input class="orderIds" id="check_{{ $order->id }}" type="checkbox"
                                           name="order_ids[]" value="{{ hashEncrypt($order->id) }}">
                                    <label for="check_{{ $order->id }}">{{ $order->confirmation_number }} - {{ $order->id }}</label>
                                    - {{ formatDate($order->updated_at) }}
                                    - <a href="<?=app_url()?>/order/view/{{ hashEncrypt($order->id) }}">View</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td style="text-align:right">
                                <input type="submit" value="{{ __('order.createInvoice') }}" class="btn btn-primary"/>

                            </td>
                        </tr>
                    @endif
                </table>
            </form>

        </div>
        <div class="col-md-3">

            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>{{ __('order.invoices') }}</th>

                    </thead>
                </tr>

                @if (count($invoices) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($invoices as $invoice)
                    <tr>
                        <td>
                            <a href="<?=app_url();?>/admin/company/{{ hashEncrypt($companyId) }}/invoice/{{ hashEncrypt($invoice->id) }}">
                                {{ $invoice->invoice_number }} - {{ formatDate($invoice->created_at) }}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </div>
    </div>



@endsection
