<HTML>
<HEAD>
    <TITLE>FEi Invoice</TITLE>

    <meta name="Microsoft Theme" content="ice2 1011">

</HEAD>

<body onload="javascript:window.print()">

<center><A name="top"></a>
    <table width="620" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td align="CENTER" valign="MIDDLE">
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <!-- MSTableType="nolayout" -->
                    <tr>
                        <td width="280" align="LEFT" valign="TOP">


                            <img alt="FEI Financial" src="<?=app_url()?>/images/FEILogoSmall.bmp" border="0">
                            <br>
                            <font
                                    size="1" face="Arial">14015 Southwest Freeway
                                <br>
                                Suite 12
                                <br>
                                Sugar Land, TX 77478
                                <BR>
                                281-980-2114</font></td>
                        <td WIDTH="400" align="center">
                            <font face="arial, helvetica" size=2><i><b>Thank You for your business!</b></i>

                            </font>
                            <p align="right">
                                <font face="Arial" size=1>

                                    <a href="javascript:window.print()">
                                        <img border="0" src="/images/ico_print.gif" width="16" height="16"></a>
                                </font></td>
                    </tr>
                    <tr>
                        <td colspan=2 height="8"></td>
                    </tr>
                </table>
                <table cellSpacing="0" cellPadding="0" width="600" border="0" id="table6">
                    <tr>
                        <td vAlign="top" align="left">
                            <font face="Arial, Helvetica, sans-serif" color="#cc0000" size="3">
                                <br>
                                <i>
                                    <b>Control</b></i><b><i> No.
                                        {{ $invoice->invoice_number }}</i></b></font><br>
                            <br>
                            &nbsp;<table cellSpacing="0" cellPadding="0" width="100%"
                                         border="1" id="table15">
                                <!-- MSTableType="nolayout" -->
                                <tr>
                                    <td style="border-right-style: none; border-right-width: medium"><font
                                                face="Arial, Helvetica, sans-serif" size="2">
                                            Date: {{ formatDate($invoice->created_at) }}</font></td>
                                    <td style="border-left-style: none; border-left-width: medium">&nbsp;</td>
                                </tr>
                            </table>
                            <table cellSpacing="0" cellPadding="5" width="100%" border="1" id="table9">
                                <tr bgColor="#666666">
                                    <td vAlign="top" align="left" width="300">
                                        <font face="Arial, Helvetica" color="#ffffff" size="2">
                                            <b>Bill To:</b></font></td>
                                    <td vAlign="top" align="left" width="300">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width=220 valign="top">
                                        <p>
                                            <b>
                                                <font size="1" face="Arial">
                                                    {{ $company['first_name']}} {{ $company['last_name'] }}<br>
                                                    {{ $company['company_name'] }}<br>
                                                    {{ $invoice->address1 }}<br>
                                                    @if(strlen(trim($invoice->addreess2)))
                                                        {{ $invoice->address2 }}<br>
                                                    @endif
                                                    @if(strlen(trim($invoice->addreess3)))
                                                        {{ $invoice->address3 }}<br>
                                                    @endif
                                                    {{ $invoice->city }} {{ $invoice->state }} {{ $invoice->zip }}
                                                    <br>{{ $invoice->country }}<br>
                                                    <br>

                                                </font></b></p>


                                    </td>
                                    <td vAlign="top" align="left">

                                        &nbsp;<br></td>
                                </tr>
                            </table>
                            <br>


                            <table border=0 cellpadding=2 cellspacing=0 width="100%" id="table6">

                                <!-- MSTableType="nolayout" -->

                                <tr>
                                    <td align=left bgcolor="#666666"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">&nbsp;
                                    </td>
                                    <td bgcolor="#666666"
                                        style="border-bottom-style: solid; border-bottom-width: 1px"><b>
                                            <font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                ORDER
                                                #</font></b></td>
                                    <td align=left bgcolor="#666666" width="120"
                                        style="border-bottom-style: solid; border-bottom-width: 1px"><b>
                                            <font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                SHIPPING COST</font></b></td>

                                </tr>

                                <tr>
                                    <td align="LEFT" colspan="2">
                                    </td>
                                </tr>


                                @foreach($invoiceItems as $item)
                                    <tr>
                                        <td style="border-style: none; border-width: medium">
                                            &nbsp;
                                        </td>
                                        <td style="border-style: none; border-width: medium">
                                            <font face="Arial" size="2">

                                                {{ $orders[$item->order_id] }}-{{$item->order_id}}</font></td>
                                        <td style="border-style: none; border-width: medium"
                                            align="right" width="120">
                                            <b> <font face="Arial"
                                                      size="2"> {{ formatNumber($item->invoice_amount, 2) }}</font></b>
                                        </td>

                                    </tr>
                                @endforeach

                                &nbsp;</td>

                                <tr>
                                    <td align="LEFT" colspan="2">&nbsp;</td>
                                <tr bgcolor="#666666">
                                    <td align=left colspan="2" bgcolor="#999999"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">
                                        <img src="td/images/whiteshadow.gif" width="1" height="1"></td>
                                </tr>
                                <tr>
                                    <td align=left colspan=1
                                        style="border-top-style: solid; border-top-width: 1px">
                                        &nbsp;
                                    </td>

                                    <td align=right colspan="2"
                                        style="border-top-style: solid; border-top-width: 1px">
                                        <table BORDER="1" style="border-width: 0px">
                                            <tr>
                                                <td style="border-style: none; border-width: medium">
                                                    <font face="Arial" size="2">
                                                        <b>
                                                            {{ formatNumber($invoice->invoice_amount, 2) }}</b></font>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <TR>
                                    <td ALIGN=Right colspan=6>
                                        &nbsp;
                                    </TD>
                                </TR>

                                <TR>
                                    <td ALIGN=Right colspan=6 class="cartmenus">&nbsp;</TD>
                                </TR>
                                <TR>
                                    <td ALIGN=Right colspan=6 class="cartmenus"></TD>
                                </TR>
                            </TABLE>
                            <br>
                            &nbsp;<table cellSpacing="0" cellPadding="4" width="620" border="1" id="table14">
                                <tr align="left" bgColor="#666666">
                                    <td vAlign="top"><b>
                                            <font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Notes
                                                / Special Instructions</font></b></td>
                                </tr>
                                <tr align="left">
                                    <td vAlign="top">

                                        <input TYPE="hidden" NAME="VTI-GROUP" VALUE="0">
                                        <p><textarea style="width: 100%" rows="10" name="NOTES">PLEASE SEE ATTACHED FOR DETAILS.
                                                {{$invoice->notes}}</textarea>
                                        </p>

                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>
                            <br>
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="middle">
                        <td vAlign="top">
                            <font face="Arial, Helvetica" size="-1"><br>
                            </font>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    </p>
</body>
</html>
