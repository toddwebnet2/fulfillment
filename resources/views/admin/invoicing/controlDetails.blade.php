<html>

<head>

    <meta http-equiv="Content-Language" content="en-us">
    <meta name="GENERATOR" content="Microsoft FrontPage 6.0">
    <meta name="ProgId" content="FrontPage.Editor.Document">

    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Invoice Details</title>
    <style>
        <!--
        .EXTRANETBTN {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 8.5pt;
            font-weight: bolder;
            color: #000000
        }

        -->
    </style>
    <!--mstheme-->
    <link rel="stylesheet" type="text/css" href="../_themes/ice2/ice21011.css">
    <meta name="Microsoft Theme" content="ice2 1011">
    <meta name="Microsoft Border" content="b">
</head>

<body
        onload="javascript:print()"><!--msnavigation-->
<table dir="ltr" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><!--msnavigation-->
        <td valign="top">

            <div align="center">
                <center>
                    <table border="1" cellpadding="0" cellspacing="0"
                           style="border-collapse: collapse; border-left-width: 0; border-right-width: 0" width="90%">
                        <tr>
                            <td width="45%"
                                style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                height="36" valign="top">

                                <p>
                                    <nobr><font size="4" face="Arial">
                                            <img border="0" src="/images/FEILogoSmall.bmp" width="189"
                                                 height="34"></font></nobr>
                                    <br>
                                    <font face="Arial">
                                        <b>Control</b></font><b><font face="Arial">
                                            {{ $invoice->invoice_number }} Report</font></b>
                            </td>
                            <td width="45%"
                                style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                height="36" valign="top" nowrap>
                                <font face="Arial" size="1"><b>{{ formatDate($invoice->created_at) }}</b></font></td>
                        </tr>
                        <tr>
                            <td width="45%"
                                style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                height="36"><font face="Arial" size="1"><b></b></font></td>
                            <td width="45%"
                                style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                height="36">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                colspan="2">
                                <div align="center">

                                    </p>
                                    <form method="POST" action="createinvoice.asp">
                                        <div align="center">
                                            <table width="100%" border="1" cellspacing="0" cellpadding="2">
                                                <thead>
                                                <tr>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px" colspan="2">
                                                        <b><font size="1"
                                                                 color="#0000FF">Date<br>
                                                            </font></b></th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">Order
                                                                ID</font></b></th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">Event</font></b>
                                                    </th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">Ship
                                                                From</font></b></th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">Via</font></b>
                                                    </th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">Item</font></b>
                                                    </th>
                                                    <th valign="top" rowspan="2"
                                                        style="border-style: solid; border-width: 1px"><b><font size="1"
                                                                                                                color="#0000FF">QTY</font></b>
                                                    </th>
                                                    <th ALIGN="LEFT" valign="top"
                                                        style="border-style: solid; border-width: 1px">
                                                        <p align="center"><b><font size="1"
                                                                                   color="#0000FF">Costs:</font></b>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th valign="top"
                                                        style="border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
                                                        <b><font size="1" color="#0000FF">
                                                                Shipping</font></b></th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th ALIGN="LEFT" valign="top" colspan="6"
                                                        style="border-style: solid; border-width: 1px">
                                                        &nbsp;
                                                    </th>
                                                    <th ALIGN="LEFT" valign="top"
                                                        style="border-style: solid; border-width: 1px">
                                                        &nbsp;
                                                    </th>

                                                    <th ALIGN="right" valign="top"
                                                        style="border-style: solid; border-width: 1px"
                                                        bgcolor="#FFFF00">&nbsp;
                                                    </th>
                                                </tr>

                                                <?php
                                                $doneTrackings = [];
                                                $doneItems = [];
                                                ?>
                                                @foreach($invoiceDetails as $detail)
                                                    <?php
                                                    extract($detail);


                                                    ?>
                                                    <tr>

                                                        <td style="border-style: solid; border-width: 1px" colspan="2">
                                                            <font
                                                                    size="1">
                                                                {{ $itemDate }}<br>
                                                                &nbsp;</font></td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <font size="1" face="Arial">

                                                                <a href="<?=app_url()?>/order/view/{{ hashEncrypt($orderId) }}/printable"
                                                                   target="_blank">{{ $refNumber}}-{{$orderId}}</a>
                                                            </font></td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <font size="1" face="Arial">

                                                                {{ $event }}</font></td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <font size="1">
                                                                {{ $shipFrom }}</font></td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <font size="1"><br>
                                                                <font color="#0000FF">
                                                                    {{ $trackingShipper }}</font><font
                                                                        color="#0000FF">-{{ $trackingNumber }}</font></font>
                                                        </td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <font size="1">
                                                                {{$itemRef}} - {{$itemTitle}}</font>
                                                        </td>
                                                        <td style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px">
                                                            <p align="center"><font size="1">

                                                                    {{$numItems}}</font></td>
                                                        <td align="right"


                                                            style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; ">
                                                            <font size="1">
                                                                {{ $shippingCost }}</font></td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>

                                        <input type="hidden" name="step" value="1">


                                    </form>


                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left-style: none; border-left-width: medium; border-right-style: none; border-right-width: medium"
                                colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </center>
            </div>


            <!--msnavigation--></td>
    </tr><!--msnavigation--></table><!--msnavigation-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#212C5A">
            <p>&nbsp;</p>

        </td>
    </tr><!--msnavigation--></table>
</body>


</html>

