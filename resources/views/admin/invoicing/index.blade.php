@extends('../layouts.template')

@section('title',  __('fei.adminHeader') )

@section('body')
    <h2>{{ __('menu.invoicing') }}</h2>

    <table class="table table-bordered table-striped">
        <tr>
            <th>Company Code</th>
            <th>Company Name</th>
            <th>Orders to be Invoice</th>
        </tr>
        @foreach($companies as $company)
            <tr>
                <td><a href="<?=app_url();?>/admin/company/{{ hashEncrypt($company->company_id) }}/invoices">{{ $company->company_code }}</a></td>
                <td>{{$company->company_name }} </td>
                <td>{{$company->num_orders}}</td>
            </tr>
        @endforeach
    </table>
@endsection
