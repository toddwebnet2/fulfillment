@extends('../../layouts.template')

@section('title',  __('fei.pendingUsers') )

@section('body')
    <h3>{{ __('fei.pendingUsers') }}</h3>
    <table class="table table-striped">
        <tr>
            <thead>
            <th colspan="2">{{ __('fei.pendingUsers') }}</th>
            </thead>
        </tr>

        @foreach($users as $user)
            <tr>

                <td>
                    @foreach(['username', 'tax_id'] as $field)
                        <b>{{__('company.' . $field)}}: </b> {{ $user->{$field} }} <BR>
                    @endforeach
                    @foreach([
                    'first_name', 'last_name', 'address1', 'address2', 'address3',
                    'city', 'state', 'zip', 'country', 'phone1', 'phone2', 'fax', 'email', 'note'
                    ] as $field)
                        @if(trim($user->contact->{$field})!= '')
                            <b>{{__('company.' . $field)}}: </b> {{ $user->contact{$field} }} <BR>
                        @endif
                    @endforeach

                </td>
                <td style="vertical-align: top">
                    <form action="<?=app_url()?>/admin/users" method="post">
                        {{ Form::hidden('user_id', hashEncrypt($user->id)) }}
                        <b>{{__('fei.company')}}: </b>
                        <select class="form-control required select2" name="company_id">
                            <option value=""></option>
                            @foreach ($companies as $company)
                                <option value="{{ hashEncrypt($company->id) }}"
                                <?=(trim(strtolower($user->tax_id)) == trim(strtolower($company->tax_id))) ? "selected" : ""?>
                                >
                                    {{ $company->company_code }} {{ $company->company_name }} ({{ $company->tax_id }})

                                </option>
                            @endforeach
                        </select>
                        <BR><BR>
                        <input type="button" style="margin-right: 100px;float:right" class="btn btn-danger" onclick="
                                if (confirm('are you sure?')) {
                                document.location = '<?=app_url()?>/admin/users/deny/{{ hashEncrypt($user->id) }}';
                                }
                                " value="Deny"/>

                        <button style="margin-left: 100px;" class="btn btn-primary">Approve</button>
                    </form>
                </td>
            </tr>

        @endforeach
    </table>
@endsection
