@extends('../../layouts.template')

@section('title',  __('fei.userForm') )

@section('body')
    <h3>{{ __('fei.userForm') }}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/admin">{{ __("menu.admin") }}</a></li>
        <li class="breadcrumb-item"><a
                    href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($user['company_id']) }}">{{ $company['company_name'] }}</a>
        </li>
        <li class="breadcrumb-item active">{{ __('fei.userForm') }}</li>
    </ol>

    <form action="<?=app_url()?>/admin/user" method="post" id="userForm" onsubmit="return userForm_validate()">
        {{ csrf_field() }}
        {{ Form::hidden('contact_id', $user['contact_id']) }}
        {{ Form::hidden('user_id', $user['user_id']) }}
        <div class="row">
            <div class="col-md-12" style="text-align:right">
                <a href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($user['company_id']) }}"
                   class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                <a href="JavaScript:userForm_submit()" class="btn btn-primary">{{ __('menu.save') }}</a>


            </div>
        </div>
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="username">{{ __('company.username') }}</label>
                    <input class="form-control required" id="username" name="username"
                           value="{{ $user['username'] }}"/>
                </div>
                <div class="form-group">
                    <label for="password">{{ __('company.password') }}</label>
                    <input class="form-control required" id="password" name="password"
                           value="{{ $user['password'] }}"/>
                </div>
                @if(in_array($company['company_code'], \App\Services\AuthService::getCompanyAdmins()))
                    <div class="form-group">
                        <label for="user_role_id">{{ __('company.user_role') }}</label>
                        <select class="form-control required select2" id="user_role_id" name="user_role_id">
                            @foreach ($userRoles as $role)
                                <option value="<?=$role->id?>" <?=($role->id == $user['user_role_id'])?"selected":""?> ><?=$role->name?></option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    <label for="tax_id">{{ __('company.tax_id') }}</label>
                    <input class="form-control required" id="tax_id" name="tax_id"
                           value="{{ $user['tax_id'] }}"/>
                </div>


            </div>
            <div class="col-md-6">

                <div class="form-group">
                    <label for="first_name">{{ __('company.first_name') }}</label>
                    <input class="form-control required" id="first_name" name="first_name"
                           value="{{ $user['first_name'] }}"/>
                </div>
                <div class="form-group">
                    <label for="last_name">{{ __('company.last_name') }}</label>
                    <input class="form-control required" id="last_name" name="last_name"
                           value="{{ $user['last_name'] }}"/>
                </div>
                <div class="form-group">
                    <label for="company_id">{{ __('fei.company') }}</label>
                    <select class="form-control required select2" name="company_id">
                        <option value=""></option>
                        @foreach ($companies as $company)
                            <option value="{{ $company->id }}"
                            <?=($user['company_id'] == $company->id) ? "selected" : ""?>
                            >
                                {{ $company->company_code }} {{ $company->company_name }} ({{ $company->tax_id }})

                            </option>
                        @endforeach
                    </select>

                </div>

            </div>
            <hr/>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="address1">{{ __('company.address1') }}</label>
                    <input class="form-control " id="address1" name="address1"
                           value="{{ $user['address1'] }}"/>
                </div>
                <div class="form-group">
                    <label for="address2">{{ __('company.address2') }}</label>
                    <input class="form-control " id="address2" name="address2"
                           value="{{ $user['address2'] }}"/>
                </div>
                <div class="form-group">
                    <label for="address3">{{ __('company.address3') }}</label>
                    <input class="form-control " id="address3" name="address3"
                           value="{{ $user['address3'] }}"/>
                </div>
                <div class="form-group">
                    <label for="city">{{ __('company.city') }}</label>
                    <input class="form-control " id="city" name="city"
                           value="{{ $user['city'] }}"/>
                </div>
                <div class="form-group">
                    <label for="state">{{ __('company.state') }}</label>
                    <input class="form-control " id="state" name="state"
                           value="{{ $user['state'] }}"/>
                </div>
                <div class="form-group">
                    <label for="zip">{{ __('company.zip') }}</label>
                    <input class="form-control " id="zip" name="zip"
                           value="{{ $user['zip'] }}"/>
                </div>
                <div class="form-group">
                    <label for="country">{{ __('company.country') }}</label>
                    <input class="form-control " id="country" name="country"
                           value="{{ $user['country'] }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">{{ __('company.email') }}</label>
                    <input class="form-control " id="email" name="email"
                           value="{{ $user['email'] }}"/>
                </div>
                <div class="form-group">
                    <label for="url">{{ __('company.url') }}</label>
                    <input class="form-control " id="url" name="url"
                           value="{{ $user['url'] }}"/>
                </div>
                <div class="form-group">
                    <label for="phone1">{{ __('company.phone1') }}</label>
                    <input class="form-control " id="phone1" name="phone1"
                           value="{{ $user['phone1'] }}"/>
                </div>
                <div class="form-group">
                    <label for="phone2">{{ __('company.phone2') }}</label>
                    <input class="form-control " id="phone2" name="phone2"
                           value="{{ $user['phone2'] }}"/>
                </div>

                <div class="form-group">
                    <label for="fax">{{ __('company.fax') }}</label>
                    <input class="form-control " id="fax" name="fax"
                           value="{{ $user['fax'] }}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="button" class="btn btn-danger" value="Delete"
                       onclick="deleteUser('{{ hashEncrypt($user['company_id']) }}', '{{ hashEncrypt($user['user_id']) }}', '{{ $user['username'] }}')"/>
            </div>
            <div class="col-md-6" style="text-align:right">

                <a href="<?=app_url();?>/admin/company/view/{{ hashEncrypt($user['company_id']) }}"
                   class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                &nbsp;
                <input type='submit' id="userFormSubmit" class="float-right btn btn-primary"
                       value="{{ __('menu.save') }}"/>
            </div>
        </div>
    </form>
    <br/>
@endsection
