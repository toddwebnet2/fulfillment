<form action="<?=app_url()?>/admin/product" method="post" id="companyForm" onsubmit="return companyForm_validate()" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ Form::hidden('id', $item['id']) }}
    {{ Form::hidden('company_id', $item['company_id']) }}
    {{ Form::hidden('image_id', $item['image_id']) }}
    <input type="hidden" id="image_v" name="image_v"/>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            @if(strlen(trim($image['path']))>0)

                <a href="<?=app_url()?>{{ $image['path'] }}" target="_blank">
                    <img src="<?=app_url()?>{{ $image['path'] }}" style="height: 200px;"/>
                </a>

            @endif
            <br/><br/>
            <label for="image">{{ __('product.image') }}</label>
            <input type="file" id="image" name="image"/>

        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label for="ref">{{ __('product.ref') }}</label>
                <input class="form-control required" id="ref" name="ref"
                       value="{{ $item['ref'] }}"/>
            </div>
            <div class="form-group">
                <label for="item_type_id">{{ __('product.item_type') }}</label>
                <select class="form-control required" id="item_type_id" name="item_type_id">
                    <option value="">Select One</option>
                    @foreach ($itemTypes as $itemType)
                        <option value="{{ $itemType['id'] }}" {{ ($itemType['id'] == $item['item_type_id'])?"selected":"" }}>{{ __($itemType['name']) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="weight">{{ __('product.weight') }}</label>
                <input class="form-control " id="weight" name="weight"
                       value="{{ $item['weight'] }}"/>
            </div>
            <div class="form-group">
                <label for="weight_unit">{{ __('product.weight_unit') }}</label>
                <input class="form-control " id="weight_unit" name="weight_unit"
                       value="{{ $item['weight_unit'] }}"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">{{ __('product.title') }}</label>
                <input class="form-control required" id="title" name="title"
                       value="{{ $item['title'] }}"/>
            </div>
            <div class="form-group">
                <label for="reorder">{{ __('product.reorder') }}</label>
                <input class="form-control " id="reorder" name="reorder"
                       value="{{ $item['reorder'] }}"/>
            </div>
            <div class="form-group">
                <label for="item_w">{{ __('product.item_w') }}</label>
                <input class="form-control " id="item_w" name="item_w"
                       value="{{ $item['item_w'] }}"/>
            </div>
            <div class="form-group">
                <label for="item_d">{{ __('product.item_d') }}</label>
                <input class="form-control " id="item_d" name="item_d"
                       value="{{ $item['item_d'] }}"/>
            </div>
            <div class="form-group">
                <label for="item_h">{{ __('product.item_h') }}</label>
                <input class="form-control " id="item_h" name="item_h"
                       value="{{ $item['item_h'] }}"/>
            </div>


        </div>
    </div>

</form>
<br/>

<script>
  function deleteModalExec() {
    deleteItem('<?=hashEncrypt($item['company_id'])?>', '<?=hashEncrypt($item['id'])?>', '<?=$item['ref']?>');
  }
</script>
