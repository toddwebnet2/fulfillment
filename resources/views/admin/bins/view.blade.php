@extends('../../layouts.template')

@section('title',  __('menu.bins') )

@section('body')
    <h2>{{ $bin['warehouse'] }} - {{ $bin['name'] }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/bins">{{ __("menu.bins") }}</a></li>
        <li class="breadcrumb-item active">{{ $bin['warehouse'] }} - {{ $bin['name'] }}</li>
    </ol>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <table class="table  table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('message.binInfo') }}</th>
                        <th>
                            @if ($bin['code'] != '0')
                                <a href='<?=app_url()?>/admin/bin/form/{{ hashEncrypt($bin['bin_id']) }}'
                                   class="btn btn-success float-right">{{ __('menu.edit') }}</a>
                            @endif
                        </th>
                    </tr>
                    </thead>

                    @foreach ($bin as $field => $value)

                        @if (__('bin.' . $field) != 'bin.' . $field )
                            <tr>
                                <td><b>{{ __('bin.' . $field) }}</b></td>
                                <td>{{ $value }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>

            </div>
{{--            <div class="col-md-6">--}}
{{--                <div class="alert-danger alert">todo: list products allowing add/update/delete</div>--}}
{{--            </div>--}}
        </div>
    </div>

@endsection
