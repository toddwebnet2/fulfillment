
    {{ csrf_field() }}
    {{ Form::hidden('company_id', $companyId) }}
    {{ Form::hidden('item_id', $itemId) }}
    @foreach ($bins as $warehouse => $tubs)
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        {{ $warehouse }}
                    </th>
                </tr>
                </thead>
                <tr>
                    <td><div class="row">
                        @foreach($tubs as $bin)
                            <div class="form-group col-md-3">
                                {{ Form::hidden('bins[]', $bin->bin_id) }}
                                {{ Form::hidden('bin_label_' . $bin->bin_id, $warehouse . ' - ' . $bin->bin) }}
                                <label for="name">{{ $bin->bin }}</label>
                                <input class="integer required" id="bin_{{ $bin->bin_id }}" name="bin_{{ $bin->bin_id }}"
                                       value="{{ $bin->num_items }}" style="width: 80px"/>
                            </div>
                        @endforeach
                        </div>
                    </td>
                </tr>
            </table>

        </div>
    @endforeach

