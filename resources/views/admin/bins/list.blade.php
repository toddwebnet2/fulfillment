@extends('../../layouts.template')

@section('title',  __('menu.bins') )

@section('body')
    <h2>{{ __('menu.bins') }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">{{ __("menu.bins") }}</li>

    </ol>
    <a href="<?=app_url()?>/admin/bin/form/{{hashEncrypt(0)}}" class="btn btn-success float-right">{{ __('menu.add') }}</a>

    <table class="table table-striped table-bordered">

        <?php
        $warehouse = '--1';
        ?>
        @foreach($bins as $bin)
            <?php
            if($warehouse != $bin['warehouse'])
            {
            $warehouse = $bin['warehouse'];
            $numProducts = 0;
            $numItems = 0;
            if (array_key_exists($bin['id'], $binCounts)) {
                $numProducts = $binCounts[$bin['id']]->num_products;
                $numItems = $binCounts[$bin['id']]->num_items;
            }
            ?>
            <thead>
            <tr>
                <th style="width:30px">&nbsp;</th>
                <th>{{ $bin['warehouse'] }}</th>
            </tr>
            </thead>
            <?php } ?>
            <tr>
                <td><a href="<?=app_url()?>/admin/bin/view/{{ hashEncrypt($bin['id']) }}"
                       class="btn btn-primary">{{ __('menu.view') }}</a></td>
                <td>{{ $bin['name'] }}
                    <span class="float-right">
                        {{ formatNumber($numProducts) }} {{ __('menu.product') }}(s)
                        {{ formatNumber($numItems) }} {{ __('menu.item') }}(s)
                    </span>
                </td>
            </tr>

        @endforeach
    </table>
@endsection
