@extends('../../layouts.template')

@section('title',  __('menu.bins') )

@section('body')
    <hr/>
    <h3>{{ __('fei.bin') }}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/bins">{{ __("menu.bins") }}</a></li>
        @if ($bin['bin_id'] > 0)
            <li class="breadcrumb-item"><a
                        href="/admin/bin/view/{{ hashEncrypt($bin['bin_id']) }}">{{ $bin['warehouse'] }}
                    - {{ $bin['name'] }}</a>
            </li>
        @endif
        <li class="breadcrumb-item active">{{ __('fei.binForm') }}</li>
    </ol>
    <form action="<?=app_url()?>/admin/bin" method="post" id="binForm" onsubmit="return binForm_validate()">
        {{ csrf_field() }}
        {{ Form::hidden('bin_id', $bin['bin_id'], ['id' => 'bin_id']) }}
        <div class="row">

            <div class="col-md-6">

                <div class="form-group">
                    <label for="warehouse">{{ __('bin.warehouse') }}</label>
                    <select class="select2 form-control required" id="warehouse" name="warehouse">
                        @if(!isLosCabos())
                            <option value="Houston" <?=($bin['warehouse'] == "Houston") ? "selected" : ""?>>Houston
                            </option>
                            <option value="Mexico" <?=($bin['warehouse'] == "Mexico") ? "selected" : ""?>>Mexico
                            </option>

                        @else
                            <option value="LosCabos" <?=($bin['warehouse'] == "LosCabos") ? "selected" : ""?>>LosCabos
                            </option>
                        @endif
                    </select>

                </div>
                <div class="form-group">
                    <label for="name">{{ __('bin.name') }}</label>
                    <input class="form-control required" id="name" name="name"
                           value="{{ $bin['name'] }}"/>
                </div>
                <div class="form-group">
                    <label for="code">{{ __('bin.code') }}</label>
                    <input class="form-control required" id="code" name="code"
                           value="{{ $bin['code'] }}"/><a href="JavaScript:binForm_generateCode()">generate</a>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="text-align:right">
                @if ($bin['bin_id'] > 0)
                    <a href="/admin/bin/{{ hashEncrypt($bin['bin_id']) }}"
                       class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                    &nbsp;@else
                    <a href="/admin/bins"
                       class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                @endif
                <input type='submit' id="binFormSubmit" class="btn btn-primary"
                       value="{{ __('menu.save') }}"/>
            </div>
        </div>
    </form>
    <BR/>
    @if ($bin['bin_id'] == 0)
        <script type="text/javascript">
          setTimeout(function () {
            binForm_generateCode();
          }, 250);
        </script>
    @endif
@endsection
