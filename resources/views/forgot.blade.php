@extends('layouts.template')

@section('title',  __('fei.forgotPassword') )

@section('body')

    <h2>{{ __('fei.forgotPassword') }} </h2>

    <div style="height: 30px">&nbsp;</div>
    <div class="wrapper">
        <form class="form-signin" action="<?=app_url()?>/forgot" method="post">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">{{ __('message.enterUserNameOrEmail') }} </h2>
            <input type="text" class="form-control" name="username" placeholder="{{ __('fei.username') }}" required=""
                   autofocus=""/>
            <BR><BR>
            <button class="btn btn-lg btn-primary" type="submit" style="float:right">{{ __('fei.login') }}</button>
            <BR>
        </form>

    </div>



    <div style="height: 100px">&nbsp;</div>
    <script type="text/javascript">
      var minutes20 = 1000 * 60 * 20;
      setTimeout(function () {
        location.reload();
      }, minutes20);
    </script>
@endsection
