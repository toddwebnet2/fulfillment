<?php

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ __('fei.appName') }} - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=app_url();?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=app_url();?>/css/app.css" rel="stylesheet">
    <script src="<?=app_url();?>/js/jquery.min.js"></script>
    <script src="<?=app_url();?>/js/jquery.numeric.min.js"></script>


    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script type="text/javascript">
      var curLocale = "<?=\App\Services\LanguageService::getLanguage()?>";
    </script>
</head>
<body>
<input type="hidden" id="app_url" value="<?=app_url()?>"/>
<nav class="navbar navbar-toggleable  fixed-top
navbar fixed-top navbar-toggleable-xl
">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?=app_url();?>/"><img src="<?=app_url();?>/images/fei-logo-small.png" style="height:30px"/></a>


    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        @if (Auth::isLoggedIn())
            @if (!isset($noViewCartLink))
                <script type="text/javascript">
                  $(document).ready(function () {
                    loadCartHeader();
                  });
                </script>
            @endif
            <ul class="navbar-nav mr-autod">
                <li class="nav-item">
                    <a class="nav-link" href="<?=app_url();?>/">{{ __("menu.home") }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=app_url();?>/orders">{{ __("menu.orders") }}</a>
                </li>
                @if (\App\Services\AuthService::isAdminUser())
                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/tracking">{{ __("menu.tracking") }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/invoice">{{ __("menu.invoicing") }}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/invoice/generate">{{ __("menu.new_invoice") }}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/company">{{ __("menu.companies") }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/bins">{{ __("menu.bins") }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=app_url();?>/admin/fedex">{{ __("menu.fedex") }}</a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="<?=app_url();?>/logout">{{ __("menu.logout") }}</a>
                </li>
                @if (false)
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Dropdown</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                @endif
            </ul>
            <script type="text/javascript">
              minutes30 = 1000 * 60 * 30;
              setTimeout("logout()", minutes30);
            </script>
        @endif
    </div>


    <ul class="nav navbar-nav navbar-right">
        <li style="padding-right:10px"><a href="JavaScript:setLang('es')">
                <img src="<?=app_url();?>//images/mx.png" style="height:15px"/></a>
        </li>
        <li>
            <a href="JavaScript:setLang('en')">
                <img src="<?=app_url();?>//images/us.png" style="height:15px"/></a>
        </li>
    </ul>

</nav>
@if (Auth::isLoggedIn())
    <div class="userbar" style="text-align:right">
        {!! \App\Services\UserBarService::getUserBarInfo() !!}
    </div>
@endif

<div class="container" id="waitingBody" style="display:none;text-align:center; padding: 50px;">
    <img src="<?=app_url();?>//images/ajax_loader.gif"/>
</div>
<div id="cartHeader"></div>
<div class="container" id="mainBody">
    {!! \App\Services\FlashService::handleFlashMessage() !!}

    <div class="starter-template">
        @section('body')
        @show()
    </div>

</div><!-- /.container -->

<div class="container" id="footer">
    <div class="col-md-12" id="footer-links">

        <a href="https://www.feifinancial.com">Home</a>
        &nbsp;&nbsp; |&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/fulfillment/default.asp">Fulfillment</a>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/login-fei.asp">Comisiones</a>
        &nbsp;&nbsp; |&nbsp;&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/login-fei.asp">Cobranza</a>
        &nbsp;&nbsp; |&nbsp;&nbsp;&nbsp;
        <a href="/assets/FulfillmentManual<?=
        (\Illuminate\Support\Facades\App::getLocale() == 'en') ?
            "-english" :
            "-spanish"
        ?>.pdf" target="_blank">Manual</a>

    </div>
    <div style="text-align:center"><img src="<?=app_url();?>/images/logo.png"/></div>
</div>


<div class="modal fade" id="myModal" role="dialog" tabindex="-1">

    <div class="modal-dialog" style="max-width: 900px">
        <form action="#" method="post" id="modalForm" onsubmit="">
            {{ csrf_field() }}
            <input type="hidden" id="modalFormId"/>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>
                <div class="modal-body" id="modalBody">

                </div>
                <div class="modal-footer" style="display:block">
                    <input id="deleteModal" type="button" style="display:none;float:left !important;"
                           class="btn btn-danger" value="Delete" onclick="deleteModalExec()"/>
                    <button type="submit" style="float:right" class="btn btn-primary">{{ __('menu.save') }}</button>
                    <button type="button" style="float:right" class="btn btn-warning"
                            data-dismiss="modal">{{ __('menu.cancel') }}</button>

                </div>
            </div>
        </form>
    </div>

</div>


<div class="modal fade" id="myImageModal" role="dialog" tabindex="-1">

    <div class="modal-dialog" style="max-width: 900px">
        <form action="#" method="post" id="modalForm" onsubmit="">
            {{ csrf_field() }}
            <input type="hidden" id="modalFormId"/>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="imageModalTitle"></h4>
                </div>
                <div class="modal-body" id="modalBody" style="text-align:center;">
                    <img id="modalImage"/>

                </div>
            </div>
        </form>
    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="<?=app_url();?>/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug
<script src="/js/ie10-viewport-bug-workaround.js"></script>
-->

<link href="<?=app_url();?>/css/select2.min.css" rel="stylesheet"/>
<script src="<?=app_url();?>/js/select2.min.js"></script>

<script src="<?=app_url();?>/js/fulfillment.js"></script>

</body>
</html>
