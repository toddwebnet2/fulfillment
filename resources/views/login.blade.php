@extends('layouts.template')

@section('title',  __('fei.login') )

@section('body')
    <div style="height: 30px">&nbsp;</div>
    <div class="wrapper">
        <form class="form-signin" action="<?=app_url()?>/login" method="post">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">{{ __('fei.appName') }} {{ __('fei.loginHeader') }}</h2>
            <input type="text" class="form-control" name="username" placeholder="{{ __('fei.username') }}" required=""
                   autofocus=""/>
            <input type="password" class="form-control" name="password" placeholder="{{ __('fei.password') }}"
                   required=""/>
            <button class="btn btn-lg btn-primary" type="submit" style="float:right">{{ __('fei.login') }}</button>
<BR>
        </form>

    </div>

    <div class="wrapper">
            <div style="text-align:center">
                <a href="<?=app_url()?>/forgot">{{ __('fei.forgotPassword') }}</a>
                |
                <a href="<?=app_url()?>/create">{{ __('fei.createAccount') }}</a>
            </div>

    </div>

    <div style="height: 100px">&nbsp;</div>
    <script type="text/javascript">
      var minutes20 = 1000 * 60 * 20;
      setTimeout(function () {
        location.reload();
      }, minutes20);
    </script>
@endsection
