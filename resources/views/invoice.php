    <table width="620" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td align="CENTER" valign="MIDDLE">
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <!-- MSTableType="nolayout" -->
                    <tr>
                        <td width="280" align="LEFT" valign="TOP">

                            <a href="../default.asp?CompanyID=FEiAdmin&ContactID=">
                                <img alt="Continue shopping" src="https://app.feifinancial.com/fulfillment/images/database/image.asp?id=64" border="0"></a>
                            <br>
                            <font
                                    size="1" face="Arial">12705 South Kirkwood Rd, Suite 214<br>
                                Houston, TX 77477<br>
                                281-980-2114</font></td>
                        <td WIDTH="400" align="center">
                            <font face="arial, helvetica" size=2><i><b>Thank You for your business!</b></i>

                            </font>
                            <p align="right">
                                <font face="Arial" size=1>

                                    <a href="javascript:window.print()">
                                        <img border="0" src="https://app.feifinancial.com/fulfillment/images/ico_print.gif" width="16" height="16">Click here</a>
                                    to print this receipt<br>
                                    <a href="invoices_supporting.asp?print=1&invoiceno=3263">Click
                                        here</a> for supporting documentation</font></td>
                    </tr>
                    <tr>
                        <td colspan=2 height="8"></td>
                    </tr>
                </table>
                <table cellSpacing="0" cellPadding="0" width="600" border="0" id="table6">
                    <tr>
                        <td vAlign="top" align="left">
                            <font face="Arial, Helvetica, sans-serif" color="#cc0000" size="3">
                                <br>
                                <i>
                                    <b>Control</b></i><b><i> No.
                                        3263</i></b></font><br>
                            <br>
                            &nbsp;<table cellSpacing="0" cellPadding="0" width="100%"
                                         border="1" id="table15">
                                <!-- MSTableType="nolayout" -->
                                <tr>
                                    <td style="border-right-style: none; border-right-width: medium"><font
                                                face="Arial, Helvetica, sans-serif" size="2">
                                            Date: 7/5/2018</font></td>
                                    <td style="border-left-style: none; border-left-width: medium">&nbsp;</td>
                                </tr>
                            </table>
                            <table cellSpacing="0" cellPadding="5" width="600" border="1" id="table9">
                                <tr bgColor="#666666">
                                    <td vAlign="top" align="left" width="300">
                                        <font face="Arial, Helvetica" color="#ffffff" size="2">
                                            <b>Bill To:</b></font></td>
                                    <td vAlign="top" align="left" width="300">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width=220 valign="top">
                                        <p>
                                            <b>
                                                <font size="1" face="Arial">
                                                    Jose M. Barquin<br>Consejo de Promocion Turistica de Mexico SA de CV<br>1700
                                                    Chantilly Drive Northeast<br>Atlanta,&nbsp;Georgia&nbsp;30324<br>USA<br>
                                                    <br>

                                                </font></b></p>


                                    </td>
                                    <td vAlign="top" align="left">

                                        &nbsp;<br></td>
                                </tr>
                            </table>
                            <br>


                            &nbsp;<table border=0 cellpadding=2 cellspacing=0 width="100%" id="table6">

                                <!-- MSTableType="nolayout" -->

                                <tr>
                                    <td align=left bgcolor="#666666"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">&nbsp;
                                    </td>
                                    <td align=center bgcolor="#666666"
                                        style="border-bottom-style: solid; border-bottom-width: 1px"><b>
                                            <font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                ORDER
                                                #</font></b></td>
                                    <td align=left bgcolor="#666666" width="120"
                                        style="border-bottom-style: solid; border-bottom-width: 1px"><b>
                                            <font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                SHIPPING COST</font></b></td>
                                    <td align=center bgcolor="#666666" width="120"
                                        style="border-bottom-style: solid; border-bottom-width: 1px"><b>
                                            <font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                HANDLING COST</font></b></td>
                                    <td align=right bgcolor="#666666" width="120"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">
                                        <b><font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                OTHER COSTS</font></b></td>
                                    <td align=right bgcolor="#666666" width="120"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">
                                        <b><font face="arial, helvetica" size="-1" color="#FFFFFF">
                                                TOTAL</font></b></td>
                                </tr>

                                <tr>
                                    <td align="LEFT" colspan="6">

                                <tr>
                                    <td style="border-style: none; border-width: medium">
                                        &nbsp;
                                    </td>
                                    <td style="border-style: none; border-width: medium">
                                        <font face="Arial" size="2">
                                            29053</font></td>
                                    <td style="border-style: none; border-width: medium"
                                        align="right" width="120">
                                        <b> <font face="Arial" size="2"> 88.54</font></b></td>
                                    <td style="border-style: none; border-width: medium"
                                        align="right" width="120">
                                        <b> <font face="Arial" size="2"> 0.00</font></b></td>
                                    <td style="border-style: none; border-width: medium"
                                        align="right" width="120">
                                        <b> <font face="Arial" size="2"> 0.00</font></b></td>
                                    <td style="border-style: none; border-width: medium"
                                        align="right" width="120">
                                        <b> <font face="Arial" size="2"> 88.54</font></b></td>
                                </tr>

                                &nbsp;</td>

                                <tr>
                                    <td align="LEFT" colspan="6">&nbsp;</td>
                                <tr bgcolor="#666666">
                                    <td align=left colspan="6" bgcolor="#999999"
                                        style="border-bottom-style: solid; border-bottom-width: 1px">
                                        <img src="https://app.feifinancial.com/fulfillment/td/images/whiteshadow.gif" width="1" height="1"></td>
                                </tr>
                                <tr>
                                    <td align=left colspan=2
                                        style="border-top-style: solid; border-top-width: 1px">
                                        &nbsp;
                                    </td>
                                    <td align=right colspan=2
                                        style="border-top-style: solid; border-top-width: 1px"><b>
                                            <font face="arial, helvetica" size="2">Total</font></b><font
                                                face="arial, helvetica" size="2"><b>:</b></font></td>
                                    <td align=right colspan="2"
                                        style="border-top-style: solid; border-top-width: 1px">
                                        <table BORDER="1" style="border-width: 0px">
                                            <tr>
                                                <td style="border-style: none; border-width: medium">
                                                    <font face="Arial" size="2">
                                                        <b>
                                                            88.54</b></font></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <TR>
                                    <td ALIGN=Right colspan=6>
                                        &nbsp;
                                    </TD>
                                </TR>

                                <TR>
                                    <td ALIGN=Right colspan=6 class="cartmenus">&nbsp;</TD>
                                </TR>
                                <TR>
                                    <td ALIGN=Right colspan=6 class="cartmenus"></TD>
                                </TR>
                            </TABLE>
                            <br>
                            &nbsp;<table cellSpacing="0" cellPadding="4" width="620" border="1" id="table14">
                                <tr align="left" bgColor="#666666">
                                    <td vAlign="top"><b>
                                            <font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Notes
                                                / Special Instructions</font></b></td>
                                </tr>
                                <tr align="left">
                                    <td vAlign="top">
                                        <form method="POST" action="invoice_printable.asp"
                                              webbot-action="--WEBBOT-SELF--">
                                            <input TYPE="hidden" NAME="VTI-GROUP" VALUE="0">
                                            <p><textarea rows="10" name="NOTES" cols="70">PLEASE SEE ATTACHED FOR DETAILS.</textarea>
                                            </p>
                                        </form>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>
                            <br>
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="middle">
                        <td vAlign="top">
                            <font face="Arial, Helvetica" size="-1"><br>
                            </font>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


