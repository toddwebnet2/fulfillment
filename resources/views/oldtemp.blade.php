<!DOCTYPE html>
<!-- saved from url=(0032)https://www.feifinancial.com/en/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.feifinancial.com/wp-content/themes/ewebresults/images/favicon.ico">
    <!-- Bootstrap -->
    <link href="<?=app_url();?>/assets/css/styles.css" rel="stylesheet">
    <link href="<?=app_url();?>/assets/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Home Page - FEI Financial</title>

    <!-- This site is optimized with the Yoast SEO plugin v6.1.1 - https://yoa.st/1yg?utm_content=6.1.1 -->
    <link rel="canonical" href="https://www.feifinancial.com/en/">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Home Page - FEI Financial">
    <meta property="og:description" content="WELCOME TO FEI ENTERPRISES, INC.   FEI Enterprises Inc, a company established in 1998, provides services to the Tourism Industry through a targeted set of cost saving products including financial services, printing, warehousing and distribution of marketing/collateral material worldwide. Our service is all inclusive and provides cost and personnel efficiencies attractive to the Hotel Comptroller/Treasurer, Director of Sales, and Director of Marketing. Services Provided by FEI Enterprises Inc. Fulfillment – Logistics and Warehousing for marketing/collateral material. Printing – Coordination and Read More">
    <meta property="og:url" content="https://www.feifinancial.com/en/">
    <meta property="og:site_name" content="FEI Financial">
    <meta property="og:image" content="https://www.feifinancial.com/wp-content/uploads/2015/09/fei-financial.jpg">
    <meta property="og:image:secure_url" content="https://www.feifinancial.com/wp-content/uploads/2015/09/fei-financial.jpg">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="WELCOME TO FEI ENTERPRISES, INC.   FEI Enterprises Inc, a company established in 1998, provides services to the Tourism Industry through a targeted set of cost saving products including financial services, printing, warehousing and distribution of marketing/collateral material worldwide. Our service is all inclusive and provides cost and personnel efficiencies attractive to the Hotel Comptroller/Treasurer, Director of Sales, and Director of Marketing. Services Provided by FEI Enterprises Inc. Fulfillment – Logistics and Warehousing for marketing/collateral material. Printing – Coordination and Read More">
    <meta name="twitter:title" content="Home Page - FEI Financial">
    <meta name="twitter:image" content="https://www.feifinancial.com/wp-content/uploads/2015/09/fei-financial.jpg">
    <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.feifinancial.com\/en\/","name":"FEI Financial","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.feifinancial.com\/en\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    <!-- / Yoast SEO plugin. -->

    <link rel="dns-prefetch" href="https://ajax.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="FEI Financial » Feed" href="https://www.feifinancial.com/en/feed/">
    <link rel="alternate" type="application/rss+xml" title="FEI Financial » Comments Feed" href="https://www.feifinancial.com/en/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="FEI Financial » Home Page Comments Feed" href="https://www.feifinancial.com/en/home/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.feifinancial.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56794,8205,9794,65039],[55358,56794,8203,9794,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script><script src="/assets/js/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="rs-plugin-settings-css" href="/assets/css/settings.css" type="text/css" media="all">
    <style id="rs-plugin-settings-inline-css" type="text/css">
        .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
        .tp-caption.large-golden,.large-golden{color:rgba(249,224,123,1.00);font-size:45px;line-height:60px;font-weight:900;font-style:normal;font-family:Raleway;padding:8px 20px 5px 20px;text-decoration:none;text-align:left;background-color:rgba(0,35,70,1.00);border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.small-dark-blue,.small-dark-blue{color:rgba(0,35,70,1.00);font-size:26px;line-height:32px;font-weight:900;font-style:normal;font-family:Raleway;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.large-blue,.large-blue{color:rgba(123,207,249,1.00);font-size:45px;line-height:60px;font-weight:900;font-style:normal;font-family:Raleway;padding:8px 20px 5px 20px;text-decoration:none;text-align:left;background-color:rgba(0,35,70,1.00);border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}
        .hesperiden.tp-bullets {
        }
        .hesperiden.tp-bullets:before {
            content:" ";
            position:absolute;
            width:100%;
            height:100%;
            background:transparent;
            padding:10px;
            margin-left:-10px;margin-top:-10px;
            box-sizing:content-box;
            border-radius:8px;

        }
        .hesperiden .tp-bullet {
            width:12px;
            height:12px;
            position:absolute;
            background: #999999; /* old browsers */
            background: -moz-linear-gradient(top,  #999999 0%, #e1e1e1 100%); /* ff3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#999999),
            color-stop(100%,#e1e1e1)); /* chrome,safari4+ */
            background: -webkit-linear-gradient(top,  #999999 0%,#e1e1e1 100%); /* chrome10+,safari5.1+ */
            background: -o-linear-gradient(top,  #999999 0%,#e1e1e1 100%); /* opera 11.10+ */
            background: -ms-linear-gradient(top,  #999999 0%,#e1e1e1 100%); /* ie10+ */
            background: linear-gradient(to bottom,  #999999 0%,#e1e1e1 100%); /* w3c */
            filter: progid:dximagetransform.microsoft.gradient(
                    startcolorstr="#999999", endcolorstr="#e1e1e1",gradienttype=0 ); /* ie6-9 */
            border:3px solid #e5e5e5;
            border-radius:50%;
            cursor: pointer;
            box-sizing:content-box;
        }
        .hesperiden .tp-bullet:hover,
        .hesperiden .tp-bullet.selected {
            background:#666;
        }
        .hesperiden .tp-bullet-image {
        }
        .hesperiden .tp-bullet-title {
        }

    </style>
    <link rel="stylesheet" id="slb_core-css" href="/assets/css/app.css" type="text/css" media="all">
    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.themepunch.revolution.min.js"></script>
    <link rel="https://api.w.org/" href="https://www.feifinancial.com/en/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.feifinancial.com/xmlrpc.php?rsd">
    <link rel="shortlink" href="https://www.feifinancial.com/en/">
    <link rel="alternate" type="application/json+oembed" href="https://www.feifinancial.com/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.feifinancial.com%2Fen%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://www.feifinancial.com/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.feifinancial.com%2Fen%2F&amp;format=xml">
    <style type="text/css">
        .qtranxs_flag_es {background-image: url(https://www.feifinancial.com/wp-content/plugins/qtranslate-x/flags/es.png); background-repeat: no-repeat;}
        .qtranxs_flag_en {background-image: url(https://www.feifinancial.com/wp-content/plugins/qtranslate-x/flags/gb.png); background-repeat: no-repeat;}
    </style>
    <link hreflang="es" href="https://www.feifinancial.com/es/" rel="alternate">
    <link hreflang="en" href="https://www.feifinancial.com/en/" rel="alternate">
    <link hreflang="x-default" href="https://www.feifinancial.com/" rel="alternate">
    <meta name="generator" content="qTranslate-X 3.4.6.8">
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <meta name="generator" content="Powered by Slider Revolution 5.0.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
</head>
<body class="home page-template-default page page-id-2">
<nav class="navbar navbar-default navbar-fixed-top hidden-lg hidden-md">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" title="FEI Financial" href="https://www.feifinancial.com/en/">
                <img src="/assets/images/logo-sm.png" alt="FEI Financial">
            </a>
            <div class="navbar-left hidden-sm hidden-md hidden-lg">
                <a href="tel:+12819802114" title="Call Us: (281) 980-2114">(281) 980-2114</a>            </div>
        </div>
        <div class="navbar-collapse collapse">
            <ul id="menu-mobile-menu" class="nav navbar-nav sm" data-smartmenus-id="15188842901133967"><li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-34 active" itemprop="url"><a title="Home" href="https://www.feifinancial.com/en/" itemprop="name">Home</a></li>
                <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38" itemprop="url"><a title="About Us" href="https://www.feifinancial.com/en/about-us/" itemprop="name">About Us</a></li>
                <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36" itemprop="url"><a title="Financial Services" href="https://www.feifinancial.com/en/financial-services/" itemprop="name">Financial Services</a></li>
                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" itemprop="url"><a title="Logistics &amp; Fulfillment" href="https://www.feifinancial.com/en/logistics-fulfillment/" itemprop="name">Logistics &amp; Fulfillment</a></li>
                <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37" itemprop="url"><a title="Contact Us" href="https://www.feifinancial.com/en/contact-us/" itemprop="name">Contact Us</a></li>
            </ul>            <ul class="nav navbar-nav navbar-right sm" data-smartmenus-id="15188842901272854">
                <li><a href="tel:+12819802114" title="Call Us: (281) 980-2114">(281) 980-2114</a></li>
                <li><a class="client-login" href="https://www.feifinancial.com/en/client-login/" title="Client Login">Client Login</a></li>
                <li>
                    <style type="text/css">
                        .qtranxs_widget ul { margin: 0; }
                        .qtranxs_widget ul li
                        {
                            display: inline; /* horizontal list, use "list-item" or other appropriate value for vertical list */
                            list-style-type: none; /* use "initial" or other to enable bullets */
                            margin: 0 5px 0 0; /* adjust spacing between items */
                            opacity: 0.5;
                            -o-transition: 1s ease opacity;
                            -moz-transition: 1s ease opacity;
                            -webkit-transition: 1s ease opacity;
                            transition: 1s ease opacity;
                        }
                        /* .qtranxs_widget ul li span { margin: 0 5px 0 0; } */ /* other way to control spacing */
                        .qtranxs_widget ul li.active { opacity: 0.8; }
                        .qtranxs_widget ul li:hover { opacity: 1; }
                        .qtranxs_widget img { box-shadow: none; vertical-align: middle; display: initial; }
                        .qtranxs_flag { height:12px; width:18px; display:block; }
                        .qtranxs_flag_and_text { padding-left:20px; }
                        .qtranxs_flag span { display:none; }
                    </style>
                    <div id="qtranslate-3" class="qtranxs_widget">
                        <ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-3-chooser">
                            <li class="lang-es"><a href="https://www.feifinancial.com/es/" hreflang="es" title="Español (es)" class="qtranxs_image qtranxs_image_es"><img src="/assets/images/mx.png" alt="Español (es)"><span style="display:none">Español</span></a></li>
                            <li class="lang-en active"><a href="https://www.feifinancial.com/en/" hreflang="en" title="English (en)" class="qtranxs_image qtranxs_image_en"><img src="/assets/images/us.png" alt="English (en)"><span style="display:none">English</span></a></li>
                        </ul><div class="qtranxs_widget_end"></div>
                    </div><!-- / end widget -->              </li>
            </ul>
        </div><!--/.collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->
<header class="container" id="cta">
    <div class="row">
        <section class="col-md-10 hidden-sm hidden-xs">
            <a class="brand" title="FEI Financial" href="https://www.feifinancial.com/en">
                <img class="img-responsive" src="/assets/images/logo.png" alt="FEI Financial">
            </a>
        </section>
        <section class="col-md-14">
            <nav class="navbar navbar-top visible-lg visible-md">
                <div class="navbar-collapse collapse">
                    <ul id="menu-top-menu" class="nav navbar-nav navbar-right"><li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-9 active" itemprop="url"><a title="Home" href="https://www.feifinancial.com/en/" itemprop="name">Home</a></li>
                        <li id="menu-item-11" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11" itemprop="url"><a title="About Us" href="https://www.feifinancial.com/en/about-us/" itemprop="name">About Us</a></li>
                        <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10" itemprop="url"><a title="Contact Us" href="https://www.feifinancial.com/en/contact-us/" itemprop="name">Contact Us</a></li>
                    </ul>              </div><!--/.collapse -->
            </nav><!-- /.navbar-top -->
        </section>
    </div><!-- /.row -->
    <style type="text/css">
        .qtranxs_widget ul { margin: 0; }
        .qtranxs_widget ul li
        {
            display: inline; /* horizontal list, use "list-item" or other appropriate value for vertical list */
            list-style-type: none; /* use "initial" or other to enable bullets */
            margin: 0 5px 0 0; /* adjust spacing between items */
            opacity: 0.5;
            -o-transition: 1s ease opacity;
            -moz-transition: 1s ease opacity;
            -webkit-transition: 1s ease opacity;
            transition: 1s ease opacity;
        }
        /* .qtranxs_widget ul li span { margin: 0 5px 0 0; } */ /* other way to control spacing */
        .qtranxs_widget ul li.active { opacity: 0.8; }
        .qtranxs_widget ul li:hover { opacity: 1; }
        .qtranxs_widget img { box-shadow: none; vertical-align: middle; display: initial; }
        .qtranxs_flag { height:12px; width:18px; display:block; }
        .qtranxs_flag_and_text { padding-left:20px; }
        .qtranxs_flag span { display:none; }
    </style>
    <div id="qtranslate-2" class="qtranxs_widget">
        <ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-2-chooser">
            <li class="lang-es"><a href="https://www.feifinancial.com/es/" hreflang="es" title="Español (es)" class="qtranxs_image qtranxs_image_es"><img src="/assets/images/mx.png" alt="Español (es)"><span style="display:none">Español</span></a></li>
            <li class="lang-en active"><a href="https://www.feifinancial.com/en/" hreflang="en" title="English (en)" class="qtranxs_image qtranxs_image_en"><img src="/assets/images/us.png" alt="English (en)"><span style="display:none">English</span></a></li>
        </ul><div class="qtranxs_widget_end"></div>
    </div><!-- / end widget -->    </header><!-- /.container -->
<nav class="navbar navbar-main visible-lg visible-md">
    <div class="container">
        <div class="navbar-right">
            <a class="client-login" href="https://www.feifinancial.com/en/client-login/" title="Client Login">Client Login</a>
        </div>
        <div id="collapse" class="navbar-collapse collapse">
            <ul id="menu-main-menu" class="nav navbar-nav"><li id="menu-item-14" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14" itemprop="url"><a title="Financial Services" href="https://www.feifinancial.com/en/financial-services/" itemprop="name">Financial Services</a></li>
                <li id="menu-item-13" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13" itemprop="url"><a title="Logistics &amp; Fulfillment" href="https://www.feifinancial.com/en/logistics-fulfillment/" itemprop="name">Logistics &amp; Fulfillment</a></li>
            </ul>          </div><!--/.collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar-main -->
<div class="container main" role="main">
    <div class="row home-slider">
        <div class="content">
            <section class="media-slider">

                <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin: 0px auto; background-color: transparent; padding: 0px; height: 347px; overflow: visible;">
                    <!-- START REVOLUTION SLIDER 5.0.5 auto mode -->
                    <div id="rev_slider_1_1" class="rev_slider fullwidthabanner revslider-initialised tp-simpleresponsive" style="margin-top: 0px; margin-bottom: 0px; height: 347px;" data-version="5.0.5">
                        <ul class="tp-revslider-mainul" style="visibility: visible; display: block; overflow: hidden; width: 838px; height: 100%; max-height: none; left: 0px;">	<!-- SLIDE  -->
                            <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-link="http://www.feifinancial.com/logistics-fulfillment/" data-thumb="https://www.feifinancial.com/wp-content/uploads/2015/09/slide1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="" class="tp-revslider-slidesli active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 20; visibility: inherit; opacity: 1;">
                                <!-- MAIN IMAGE -->
                                <div class="slotholder" style="width: 100%; height: 100%; visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="https://www.feifinancial.com/wp-content/uploads/2015/09/slide1.jpg" alt="" width="903" height="374" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="">--><div class="tp-bgimg defaultimg" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url(&quot;https://www.feifinancial.com/wp-content/uploads/2015/09/slide1.jpg&quot;); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit; z-index: 20;" src="https://www.feifinancial.com/wp-content/uploads/2015/09/slide1.jpg"></div></div>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 28px; top: 24px; z-index: 5;"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;"><div class="tp-caption large-golden   tp-resizeme" id="slide-1-layer-1" data-x="30" data-y="26" data-width="[&#39;auto&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-size: 32px; line-height: 42px; visibility: inherit; transition: none; border-width: 0px; margin: 0px; padding: 7px 19px 5px; letter-spacing: 1px; font-weight: 900; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">FINANCIAL SERVICES FOR<br style="transition: none; line-height: 42px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 1px; font-weight: 900; font-size: 32px;"> THE TOURISM INDUSTRY
                                            </div></div></div></div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 174px; top: 133px; z-index: 6;"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;"><div class="tp-caption small-dark-blue   tp-resizeme" id="slide-1-layer-2" data-x="187" data-y="143" data-width="[&#39;auto&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap; visibility: inherit; transition: none; line-height: 30px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 1px; font-weight: 900; font-size: 24px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">Flexible, Customized &amp;<br style="transition: none; line-height: 30px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 1px; font-weight: 900; font-size: 24px;">
                                                Branded Financial Services
                                            </div></div></div></div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 284px; top: 227px; z-index: 7;"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;"><div class="tp-caption rev-btn rs-hover-ready" id="slide-1-layer-3" data-x="306" data-y="245" data-width="[&#39;auto&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(123, 207, 249, 1.00);bg:rgba(0, 35, 70, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 700; color: rgb(0, 35, 70); font-family: Roboto; background-color: rgba(123, 207, 249, 0.75); padding: 12px 35px; border-color: rgb(0, 0, 0); border-radius: 3px; outline: none; box-shadow: none; box-sizing: border-box; visibility: inherit; transition: none; font-style: normal; border-width: 0px; border-style: none; margin: 0px; letter-spacing: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">GET DETAILS
                                            </div></div></div></div>
                                <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; width: 100% !important; height: 100% !important; left: 0px; top: 0px; z-index: 60;"><div class="tp-loop-wrap" style="width:100% !important;height:100% !important;position:absolute;"><div class="tp-mask-wrap" style="position: absolute; width: 100%; height: 100%; overflow: visible;"><div class="tp-caption sft slidelink" style="cursor: pointer; width: 100%; height: 100%; z-index: 60; visibility: inherit; transition: none; line-height: 19px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 13px; white-space: nowrap; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;" data-x="center" data-y="center" data-start="0"><a style="width:100%;height:100%;display:block" target="_self" href="http://www.feifinancial.com/logistics-fulfillment/"><span style="width:100%;height:100%;display:block"></span></a></div></div></div></div></li>
                            <!-- SLIDE  -->
                            <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-link="http://www.feifinancial.com/financial-services/" data-thumb="https://www.feifinancial.com/wp-content/uploads/2015/09/slide2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="" class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
                                <!-- MAIN IMAGE -->
                                <div class="slotholder" style="width:100%;height:100%;"><!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="https://www.feifinancial.com/wp-content/uploads/2015/09/slide2.jpg" alt="" width="903" height="374" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="">--><div class="tp-bgimg defaultimg" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url(&quot;https://www.feifinancial.com/wp-content/uploads/2015/09/slide2.jpg&quot;); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 0;" src="https://www.feifinancial.com/wp-content/uploads/2015/09/slide2.jpg"></div></div>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption large-blue   tp-resizeme" id="slide-3-layer-1" data-x="24" data-y="20" data-width="[&#39;700&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 5; min-width: 700px; max-width: 700px; white-space: normal; font-size: 40px; line-height: 45px; visibility: hidden;">LOGISTIC &amp; FULFILLMENT
                                            </div></div></div></div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption small-dark-blue   tp-resizeme" id="slide-3-layer-2" data-x="70" data-y="124" data-width="[&#39;auto&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap; border-color: rgb(94, 94, 94); visibility: hidden;">Supply Chain Management, Packing &amp; Shipping
                                            </div></div></div></div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption rev-btn " id="slide-3-layer-3" data-x="348" data-y="250" data-width="[&#39;auto&#39;]" data-height="[&#39;auto&#39;]" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(0, 35, 70, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 700; color: rgb(123, 207, 249); font-family: Roboto; background-color: rgba(0, 35, 70, 0.75); padding: 12px 35px; border-color: rgb(0, 0, 0); border-radius: 3px; outline: none; box-shadow: none; box-sizing: border-box; visibility: hidden;">GET DETAILS
                                            </div></div></div></div>
                                <div class="tp-parallax-wrap" style="width:100% !important;height:100% !important;position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="width:100% !important;height:100% !important;position:absolute;"><div class="tp-mask-wrap" style="width:100% !important;height:100% !important;position:absolute"><div class="tp-caption sft slidelink" style="cursor: pointer; width: 100%; height: 100%; z-index: 60; visibility: hidden;" data-x="center" data-y="center" data-start="0"><a style="width:100%;height:100%;display:block" target="_self" href="http://www.feifinancial.com/financial-services/"><span style="width:100%;height:100%;display:block"></span></a></div></div></div></div></li>
                        </ul>
                        <div class="tp-bannertimer" style="height: 5px; background-color: rgba(0, 0, 0, 0.15); width: 37.5444%; visibility: visible; transform: translate3d(0px, 0px, 0px);"></div>	<div class="tp-loader spinner0" style="display: none;"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div><div class="tp-leftarrow tparrows gyges" style="top: 50%; transform: matrix(1, 0, 0, 1, 20, -20); left: 0px;"></div><div class="tp-rightarrow tparrows gyges" style="top: 50%; transform: matrix(1, 0, 0, 1, -60, -20); left: 100%;"></div><div class="tp-bullets hesperiden horizontal" style="width: 41px; height: 18px; top: 100%; transform: matrix(1, 0, 0, 1, -20, -38); left: 50%;"><div class="tp-bullet selected" style="left: 0px; top: 0px;"></div><div class="tp-bullet" style="left: 23px; top: 0px;"></div></div></div>
                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");htmlDiv.innerHTML = htmlDiv.innerHTML + unescape(".tp-caption.large-golden,.large-golden{color:rgba(249,224,123,1.00);font-size:45px;line-height:60px;font-weight:900;font-style:normal;font-family:Raleway;padding:8px 20px 5px 20px;text-decoration:none;text-align:left;background-color:rgba(0,35,70,1.00);border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.small-dark-blue,.small-dark-blue{color:rgba(0,35,70,1.00);font-size:26px;line-height:32px;font-weight:900;font-style:normal;font-family:Raleway;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.large-blue,.large-blue{color:rgba(123,207,249,1.00);font-size:45px;line-height:60px;font-weight:900;font-style:normal;font-family:Raleway;padding:8px 20px 5px 20px;text-decoration:none;text-align:left;background-color:rgba(0,35,70,1.00);border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}");</script>
                    <script type="text/javascript">

                        /******************************************
                         -	PREPARE PLACEHOLDER FOR SLIDER	-
                         ******************************************/

                        var setREVStartSize=function(){
                            try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                                e.c = jQuery('#rev_slider_1_1');
                                e.gridwidth = [903];
                                e.gridheight = [374];

                                e.sliderLayout = "auto";
                                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
                            }catch(d){console.log("Failure at Presize of Slider:"+d)}
                        };


                        setREVStartSize();
                        function revslider_showDoubleJqueryError(sliderID) {
                            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>"
                            jQuery(sliderID).show().html(errorMessage);
                        }
                        var tpj=jQuery;

                        var revapi1;
                        tpj(document).ready(function() {
                            if(tpj("#rev_slider_1_1").revolution == undefined){
                                revslider_showDoubleJqueryError("#rev_slider_1_1");
                            }else{
                                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                    sliderType:"standard",
                                    jsFileLocation:"https://www.feifinancial.com/wp-content/plugins/revslider/public/assets/js/",
                                    sliderLayout:"auto",
                                    dottedOverlay:"none",
                                    delay:9000,
                                    navigation: {
                                        keyboardNavigation:"off",
                                        keyboard_direction: "horizontal",
                                        mouseScrollNavigation:"off",
                                        onHoverStop:"off",
                                        arrows: {
                                            style:"gyges",
                                            enable:true,
                                            hide_onmobile:false,
                                            hide_onleave:false,
                                            tmp:'',
                                            left: {
                                                h_align:"left",
                                                v_align:"center",
                                                h_offset:20,
                                                v_offset:0
                                            },
                                            right: {
                                                h_align:"right",
                                                v_align:"center",
                                                h_offset:20,
                                                v_offset:0
                                            }
                                        }
                                        ,
                                        bullets: {
                                            enable:true,
                                            hide_onmobile:false,
                                            style:"hesperiden",
                                            hide_onleave:false,
                                            direction:"horizontal",
                                            h_align:"center",
                                            v_align:"bottom",
                                            h_offset:0,
                                            v_offset:20,
                                            space:5,
                                            tmp:''
                                        }
                                    },
                                    gridwidth:903,
                                    gridheight:374,
                                    lazyType:"none",
                                    shadow:0,
                                    spinner:"spinner0",
                                    stopLoop:"off",
                                    stopAfterLoops:-1,
                                    stopAtSlide:-1,
                                    shuffle:"off",
                                    autoHeight:"off",
                                    hideThumbsOnMobile:"off",
                                    hideSliderAtLimit:0,
                                    hideCaptionAtLimit:0,
                                    hideAllCaptionAtLilmit:0,
                                    startWithSlide:0,
                                    debugMode:false,
                                    fallbacks: {
                                        simplifyAll:"off",
                                        nextSlideOnWindowFocus:"off",
                                        disableFocusListener:false,
                                    }
                                });
                            }
                        });	/*ready*/
                    </script>
                    <script>
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        htmlDiv.innerHTML = htmlDiv.innerHTML + unescape("%0A.hesperiden.tp-bullets%20%7B%0A%7D%0A.hesperiden.tp-bullets%3Abefore%20%7B%0A%09content%3A%22%20%22%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%20%20%20border-radius%3A8px%3B%0A%20%20%0A%7D%0A.hesperiden%20.tp-bullet%20%7B%0A%09width%3A12px%3B%0A%09height%3A12px%3B%0A%09position%3Aabsolute%3B%0A%09background%3A%20%23999999%3B%20%2F%2A%20old%20browsers%20%2A%2F%0A%20%20%20%20background%3A%20-moz-linear-gradient%28top%2C%20%20%23999999%200%25%2C%20%23e1e1e1%20100%25%29%3B%20%2F%2A%20ff3.6%2B%20%2A%2F%0A%20%20%20%20background%3A%20-webkit-gradient%28linear%2C%20left%20top%2C%20left%20bottom%2C%20color-stop%280%25%2C%23999999%29%2C%20%0A%20%20%20%20color-stop%28100%25%2C%23e1e1e1%29%29%3B%20%2F%2A%20chrome%2Csafari4%2B%20%2A%2F%0A%20%20%20%20background%3A%20-webkit-linear-gradient%28top%2C%20%20%23999999%200%25%2C%23e1e1e1%20100%25%29%3B%20%2F%2A%20chrome10%2B%2Csafari5.1%2B%20%2A%2F%0A%20%20%20%20background%3A%20-o-linear-gradient%28top%2C%20%20%23999999%200%25%2C%23e1e1e1%20100%25%29%3B%20%2F%2A%20opera%2011.10%2B%20%2A%2F%0A%20%20%20%20background%3A%20-ms-linear-gradient%28top%2C%20%20%23999999%200%25%2C%23e1e1e1%20100%25%29%3B%20%2F%2A%20ie10%2B%20%2A%2F%0A%20%20%20%20background%3A%20linear-gradient%28to%20bottom%2C%20%20%23999999%200%25%2C%23e1e1e1%20100%25%29%3B%20%2F%2A%20w3c%20%2A%2F%0A%20%20%20%20filter%3A%20progid%3Adximagetransform.microsoft.gradient%28%20%0A%20%20%20%20startcolorstr%3D%22%23999999%22%2C%20endcolorstr%3D%22%23e1e1e1%22%2Cgradienttype%3D0%20%29%3B%20%2F%2A%20ie6-9%20%2A%2F%0A%09border%3A3px%20solid%20%23e5e5e5%3B%0A%09border-radius%3A50%25%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.hesperiden%20.tp-bullet%3Ahover%2C%0A.hesperiden%20.tp-bullet.selected%20%7B%0A%09background%3A%23666%3B%0A%7D%0A.hesperiden%20.tp-bullet-image%20%7B%0A%7D%0A.hesperiden%20.tp-bullet-title%20%7B%0A%7D%0A%0A");
                    </script>
                </div><!-- END REVOLUTION SLIDER -->      </section>
        </div><!-- /.content -->
        <aside class="sidebar">
            <div id="sideform"><div id="sidebar_form-2" class="widget_sidebar_form"><h3>Request A Customized Quote</h3>    <form class="form-nolabel" method="POST" action="https://www.feifinancial.com/en/#sideform">
                        <div class="form-group hidden">
                            <label for="username">User Name</label>
                            <input type="text" class="form-control" name="username" id="username" value="" placeholder="User Name">
                        </div>
                        <div class="form-group ">
                            <label class="sr-only" for="names">Name</label><input type="text" class="form-control" id="names" name="names" value="" placeholder="Name">
                        </div><div class="form-group ">
                            <label class="sr-only" for="emails">Email</label><input type="email" class="form-control" id="emails" name="emails" value="" placeholder="Email">
                        </div><div class="form-group ">
                            <label class="sr-only" for="phones">Phone</label><input type="tel" class="form-control" id="phones" name="phones" value="" placeholder="Phone">
                        </div><div class="form-group ">
                            <label class="sr-only" for="messages">Message</label><textarea class="form-control" id="messages" name="messages" rows="4" placeholder="Message"></textarea></div><input type="hidden" id="hjg378d4g3d" name="hjg378d4g3d" value="50cfd39e53"><input type="hidden" name="_wp_http_referer" value="/en/"><div class="text-center">
                            <button type="submit" class="btn btn-primary">Send Email</button>
                        </div>    </form>
                </div></div><!-- /#sidebar -->  </aside>  </div><!-- /.row -->
    <div class="row featured">
        <div class="col-md-6">
            <a href="https://www.feifinancial.com/en/financial-services/" title="Financial Services">
                <h4>Financial Services</h4>
                <p>
                    <span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span> <span class="more">more info</span>
                </p>
                <figure><img width="167" height="127" src="/assets/images/box11.png" class="img-responsive wp-post-image" alt="Financial Services - FEI Enterprises Inc"></figure>
            </a>
        </div><!-- /.col -->
        <div class="col-md-6">
            <a href="https://www.feifinancial.com/en/logistics-fulfillment/" title="Logistics &amp; Fulfillment">
                <h4>Logistics &amp; Fulfillment</h4>
                <p>
                    <span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span> <span class="more">more info</span>
                </p>
                <figure><img width="167" height="127" src="/assets/images/box21.png" class="img-responsive wp-post-image" alt="Logistics and Fulfillment - FEI Enterprises Inc"></figure>
            </a>
        </div><!-- /.col -->
        <div class="col-md-6">
            <a href="https://www.feifinancial.com/en/client-login/" title="Client Login">
                <h4>Client Login</h4>
                <p>
                    <span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span> <span class="more">more info</span>
                </p>
                <figure><img width="167" height="127" src="/assets/images//box31.png" class="img-responsive wp-post-image" alt="Client Login - FEI Enterprises Inc"></figure>
            </a>
        </div><!-- /.col -->
        <div class="col-md-6">
            <a href="https://www.feifinancial.com/en/#" title="Guaranteed 20% off shipping">
                <img class="img-responsive" src="/assets/images/20-off-shipping.png" alt="Guaranteed 20% off shipping">
            </a>
        </div><!-- /.col -->
    </div>
    <article id="page-2" class="entry post-2 page type-page status-publish hentry">
        <h1>WELCOME TO FEI ENTERPRISES, INC.</h1>
        <p><img class="alignright size-full wp-image-33" src="/assets/images/fei-financial.jpg" alt="fei-financial" width="591" height="522" srcset="https://www.feifinancial.com/wp-content/uploads/2015/09/fei-financial.jpg 591w, https://www.feifinancial.com/wp-content/uploads/2015/09/fei-financial-300x265.jpg 300w" sizes="(max-width: 591px) 100vw, 591px"></p>
        <p>&nbsp;</p>
        <p>FEI Enterprises Inc, a company established in 1998, provides services to the Tourism Industry through a targeted set of cost saving products including financial services, printing, warehousing and distribution of marketing/collateral material worldwide. Our service is all inclusive and provides cost and personnel efficiencies attractive to the Hotel Comptroller/Treasurer, Director of Sales, and Director of Marketing.</p>
        <p>Services Provided by FEI Enterprises Inc.</p>
        <ul>
            <li>Fulfillment – Logistics and Warehousing for marketing/collateral material.</li>
            <li>Printing – Coordination and printing of items like brochures, conference booths and give-aways.</li>
            <li>Agency Commission Payments – Travel agency commission payments consolidated onto one service invoice and delivered to Travel Agencies Worldwide.</li>
            <li>Accounts Receivable – International accounts receivable processing.</li>
            <li>Membership – Maintenance fee &amp;&nbsp;monthly dues processing as well as&nbsp;Welcome Package delivery for new and existing members.</li>
        </ul>
        <p>Our cost beneficial warehousing and logistics service is utilized by Hotels, Resorts,&nbsp;Latinamerican Tourism Boards, Conventions and Visitors Bureau’s. &nbsp;They use&nbsp;our fulfillment program to efficiently distribute marketing and collateral material, on time, to conventions, tradeshows or end users in the US, Canada and Worldwide.</p>
        <p>Satisfied customers appreciate the cost savings as well as our utmost&nbsp;customer service. Our high level of service has often resulted in clients raving about our prompt service, frequent touch points and competent execution.</p>
    </article>
    <div class="clearfix"></div></div><!-- /.container -->
<footer class="container">
    <div class="row">
        <nav class="navbar navbar-footer">
            <div class="navbar-collapse collapse">
                <ul id="menu-footer-menu" class="nav navbar-nav"><li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-21 active" itemprop="url"><a title="Home" href="https://www.feifinancial.com/en/" itemprop="name">Home</a></li>
                    <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" itemprop="url"><a title="Financial Services" href="https://www.feifinancial.com/en/financial-services/" itemprop="name">Financial Services</a></li>
                    <li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" itemprop="url"><a title="Logistics &amp; Fulfillment" href="https://www.feifinancial.com/en/logistics-fulfillment/" itemprop="name">Logistics &amp; Fulfillment</a></li>
                    <li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" itemprop="url"><a title="Contact Us" href="https://www.feifinancial.com/en/contact-us/" itemprop="name">Contact Us</a></li>
                </ul>        </div>
        </nav><!-- /.navbar-footer -->
    </div><!-- /.row -->
    <div class="row">
        <section class="col-xs-24">
            <a class="ftr-logo" title="FEI Financial" href="https://www.feifinancial.com/en">
                <img class="img-responsive" src="/assets/images/ftr-logo.png" alt="FEI Financial">
            </a>
        </section>
    </div>
</footer>
<script type="text/javascript" src="/assets/js/scripts.js"></script>
<script type="text/javascript" src="/assets/js/wp-embed.min.js"></script>
<script type="text/javascript" id="slb_context">/* <![CDATA[ */if ( !!window.jQuery ) {(function($){$(document).ready(function(){if ( !!window.SLB ) { {$.extend(SLB, {"context":["public","user_guest"]});} }})})(jQuery);}/* ]]> */</script>

</body></html>
