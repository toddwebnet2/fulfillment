@extends('layouts.template')

@section('title',  __('fei.createAccount') )

@section('body')
    <h1>{{ __('fei.createAccount') }}</h1>



    <form action="<?=app_url()?>/create" method="post" id="userForm" onsubmit="return userCreateForm_validate()">
        {{ csrf_field() }}
        {{ Form::hidden('contact_id', $user['contact_id']) }}
        {{ Form::hidden('user_id', $user['user_id']) }}

        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="username">{{ __('company.username') }}</label>
                    <input class="form-control required" id="username" name="username" tabindex="1"
                           value="{{ $user['username'] }}"/>
                </div>
                <div class="form-group">
                    <label for="first_name">{{ __('company.first_name') }}</label>
                    <input class="form-control required" id="first_name" name="first_name" tabindex="3"
                           value="{{ $user['first_name'] }}"/>
                </div>
                <div class="form-group">
                    <label for="password">{{ __('company.password') }}</label>
                    <input class="form-control required" id="password" name="password" tabindex="5"
                           value="{{ $user['password'] }}"/>
                </div>


            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tax_id">{{ __('company.tax_id') }}</label>
                    <input class="form-control required" id="tax_id" name="tax_id" tabindex="2"
                           value="{{ $user['tax_id'] }}"/>
                </div>

                <div class="form-group">
                    <label for="last_name">{{ __('company.last_name') }}</label>
                    <input class="form-control required" id="last_name" name="last_name" tabindex="4"
                           value="{{ $user['last_name'] }}"/>
                </div>


            </div>
            <hr/>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="address1">{{ __('company.address1') }}</label>
                    <input class="form-control " id="address1" name="address1" tabindex="6"
                           value="{{ $user['address1'] }}"/>
                </div>
                <div class="form-group">
                    <label for="address2">{{ __('company.address2') }}</label>
                    <input class="form-control " id="address2" name="address2" tabindex="7"
                           value="{{ $user['address2'] }}"/>
                </div>
                <div class="form-group">
                    <label for="address3">{{ __('company.address3') }}</label>
                    <input class="form-control " id="address3" name="address3" tabindex="8"
                           value="{{ $user['address3'] }}"/>
                </div>
                <div class="form-group">
                    <label for="city">{{ __('company.city') }}</label>
                    <input class="form-control " id="city" name="city" tabindex="9"
                           value="{{ $user['city'] }}"/>
                </div>
                <div class="form-group">
                    <label for="state">{{ __('company.state') }}</label>
                    <input class="form-control " id="state" name="state" tabindex="10"
                           value="{{ $user['state'] }}"/>
                </div>
                <div class="form-group">
                    <label for="zip">{{ __('company.zip') }}</label>
                    <input class="form-control " id="zip" name="zip" tabindex="11"
                           value="{{ $user['zip'] }}"/>
                </div>
                <div class="form-group">
                    <label for="country">{{ __('company.country') }}</label>
                    <input class="form-control " id="country" name="country" tabindex="12"
                           value="{{ $user['country'] }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">{{ __('company.email') }}</label>
                    <input class="form-control " id="email" name="email" tabindex="13"
                           value="{{ $user['email'] }}"/>
                </div>
                <div class="form-group">
                    <label for="url">{{ __('company.url') }}</label>
                    <input class="form-control " id="url" name="url" tabindex="14"
                           value="{{ $user['url'] }}"/>
                </div>
                <div class="form-group">
                    <label for="phone1">{{ __('company.phone1') }}</label>
                    <input class="form-control " id="phone1" name="phone1" tabindex="15"
                           value="{{ $user['phone1'] }}"/>
                </div>
                <div class="form-group">
                    <label for="phone2">{{ __('company.phone2') }}</label>
                    <input class="form-control " id="phone2" name="phone2" tabindex="16"
                           value="{{ $user['phone2'] }}"/>
                </div>

                <div class="form-group">
                    <label for="fax">{{ __('company.fax') }}</label>
                    <input class="form-control " id="fax" name="fax" tabindex="17"
                           value="{{ $user['fax'] }}"/>
                </div>


                <div class="form-group" style="margin-top: 110px">
                    <label for="warehouse" id="warehouseLabel">{{ __('fei.warehouse') }}</label>
                    <select id="warehouse" name="warehouse" class="form-control select2">
                        @if(isLosCabos())
                            <option>Los Cabos</option>
                        @else
                            <option></option>
                            <option>USA</option>
                            <option>Mexico</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align:right">
                <a href="<?=app_url()?>/"
                   class="btn btn-secondary">{{ __('menu.cancel') }}</a>
                &nbsp;
                <input type='submit' id="userFormSubmit" class="float-right btn btn-primary" tabindex="18"
                       value="{{ __('menu.save') }}"/>
            </div>
        </div>
    </form>
@endsection
