@extends('layouts.template')

@section('title',  __('fei.viewCart') )

@section('body')
    <h2>{{ __('fei.viewCart') }}</h2>
    <div class="row">
    <?=$cart?>
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align: right">
        <a href="<?=app_url();?>/checkout" class="btn btn-primary">{{ __('menu.checkout') }}</a>
        </div>
    </div>
    <br/>
@endsection
