<input type="hidden" id="cannotOrderThatMany" value="{{ __('product.cannotOrderThatMany') }}"/>

<table class="table">
    <tr>
        <thead>
        <th colspan="2">{{ __('fei.viewCart') }} </th>
        </thead>
    </tr>
    @foreach ($cart as $item)
        <?php

        $itemCount = $itemCounts[$item->item_id];
        $inventoryCount = $itemInvCounts[$item->item_id];
        ?>
        <tr>
            <td>
                <img src="<?=app_url()?>{{ $item->path }}" style="max-width: {{ $width }}px;max-height: {{ $width }}px"/>

            </td>
            <td>
                {{$item->title}} <BR> {{ __($item->item_type) }} - {{$item->ref}}<Br>
                @if ($formView)
                    <div style="font-size:12px;color:darkblue;font-weight:bold">
                        {{ formatNumber($itemCount) }} {{ __('product.itemsAvailable') }}
                        {{ formatNumber($inventoryCount) }} {{ __('product.itemsInStock') }}
                    </div>
                    <input type="hidden" id="qtyOnHand_{{ $item->item_id }}" value="{{$itemCount}}"/>
                    <input type="number" onkeypress="return IntegersOnly(event)" style="width: 75px" maxlength="6"
                           id="productinput_{{$item->item_id}}" value="{{$item->num_items}}"
                           onfocus="this.select();" onmouseup="return false;">
                    <a href="JavaScript:addCart({{$item->item_id}})"
                       class="btn btn-primary">{{ __('menu.updateCart') }}</a>
                @else
                    {{$item->num_items}}
                @endif
            </td>

        </tr>
    @endforeach
</table>
