@extends('layouts.template')
@section('title',  __('menu.checkout') )
@section('body')
    <h2>{{ __('menu.checkout') }}</h2>
    <form action="<?=app_url()?>/checkout/finalize" method="post" id="checkoutForm" onsubmit="return checkoutForm_validate()">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12"><?=$cart?></div>
            <div class="col-md-12"><h2>{{ __('menu.checkout') }}</h2></div>

            <div class="col-md-6">
                <table class="table table-striped">
                    <tr>
                        <thead>
                        <th colspan="2">{{ __('order.from') }}</th>
                        </thead>
                    </tr>

                    <tr>
                        <td>
                            <label style="font-weight:bold" for="shipper_id">{{ __('order.shipper') }}</label><span
                                    class="requiredStar">*</span>
                        </td>
                        <td>
                            <select class="select2 form-control required" id="shipper_id" name="shipper_id"
                                    style="width: 350px">
                                @foreach ($shippers as $id => $shipper)
                                    <option value="{{ $id }}">{{ $shipper }}</option>
                                @endforeach
                            </select>

                    </tr>


                    <tr>
                        <td>
                            <label style="font-weight:bold" for="prevTo">{{ __('order.previous') }}</label>
                        </td>
                        <td>
                        <span style="color:blue;font-size: 10px">
                            <select class="select2 form-control required" id="prevTo"
                                    onchange="copyFromPrevious(this.value)" style="width: 350px">
                                <option value="" selected></option>
                                @foreach ($fromHistory as $id => $from)
                                    <option value="<?=htmlspecialchars(json_encode($from))?>"><?=buildCompiledAddress($from)?></option>
                                @endforeach
                            </select>
                        <BR/>{{  __('order.previousExplanation') }}</span>

                    </tr>

                    <tr>
                        <td><label style="font-weight:bold"
                                   for="from_first_name">{{ __('order.first_name') }}</label><span class="requiredStar">*</span>
                        </td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%"
                                   name="from_first_name"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold"
                                   for="from_last_name">{{ __('order.last_name') }}</label><span
                                    class="requiredStar">*</span>
                        </td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%"
                                   name="from_last_name"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_company">{{ __('order.company') }}</label><span
                                    class="requiredStar">*</span>
                        </td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%"
                                   name="from_company"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_address1">{{ __('order.address1') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="128" style="width:100%"
                                   name="from_address1"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_address2">{{ __('order.address2') }}</label></td>
                        <td><input type="text" maxlength="128" style="width:100%"
                                   name="from_address2"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_address3">{{ __('order.address3') }}</label></td>
                        <td><input type="text" maxlength="128" style="width:100%"
                                   name="from_address3"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_city">{{ __('order.city') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="from_city"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_state">{{ __('order.state') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="from_state"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_zip">{{ __('order.zip') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="from_zip"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_country">{{ __('order.country') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td>
                            <input type="text" class="required" maxlength="64" style="width:100%" name="from_country"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_phone1">{{ __('order.phone1') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="from_phone1"/>
                        </td>
                    <tr>
                        <td><label style="font-weight:bold" for="from_phone2">{{ __('order.phone2') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="from_phone2"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_fax">{{ __('order.fax') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="from_fax"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="from_email">{{ __('order.email') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="from_email"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-striped">
                    <tr>
                        <thead>
                        <th colspan="2">{{ __('order.to') }}</th>
                        </thead>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center"><a
                                    href="JavaScript:copyFromShipping()">{{ __('order.copyfromShipping') }}</a></td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-weight:bold" for="prevFrom">{{ __('order.previous') }}</label>
                        </td>
                        <td>
                            <span style="color:blue;font-size: 10px">
                            <select class="select2 form-control required" id="prevFrom"
                                    onchange="copyFromPrevious(this.value)" style="width: 350px">
                                <option value="" selected></option>
                                @foreach ($toHistory as $id => $to)
                                    <option value="<?=htmlspecialchars(json_encode($to))?>"><?=buildCompiledAddress($to)?></option>
                                @endforeach
                            </select>
                            <BR/>{{  __('order.previousExplanation') }}</span>

                    </tr>
                    <tr>
                        <td><label style="font-weight:bold"
                                   for="to_first_name">{{ __('order.first_name') }}</label><span
                                    class="requiredStar">*</span>
                        </td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_first_name"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_last_name">{{ __('order.last_name') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_last_name"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_company">{{ __('order.company') }}</label><span
                                    class="requiredStar">*</span>
                        </td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%"
                                   name="to_company"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_address1">{{ __('order.address1') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="128" style="width:100%" name="to_address1"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_address2">{{ __('order.address2') }}</label></td>
                        <td><input type="text" maxlength="128" style="width:100%" name="to_address2"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_address3">{{ __('order.address3') }}</label></td>
                        <td><input type="text" maxlength="128" style="width:100%" name="to_address3"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_city">{{ __('order.city') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_city"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_state">{{ __('order.state') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_state"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_zip">{{ __('order.zip') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_zip"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_country">{{ __('order.country') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_country"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_phone1">{{ __('order.phone1') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="to_phone1"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_phone2">{{ __('order.phone2') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="to_phone2"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_fax">{{ __('order.fax') }}</label></td>
                        <td><input type="text" maxlength="64" style="width:100%" name="to_fax"/></td>
                    </tr>
                    <tr>
                        <td><label style="font-weight:bold" for="to_email">{{ __('order.email') }}</label><span
                                    class="requiredStar">*</span></td>
                        <td><input type="text" class="required" maxlength="64" style="width:100%" name="to_email"/></td>
                    </tr>

                </table>

            </div>
            <div class="col-md-12" style="text-align:center">

                <br><b>{{ __('order.event') }}</b><br>
                <input type="text" name="event" maxlength="64" value=""/>
                <br><br>

                <b>{{ __('order.notes') }}</b><br>
                <textarea name="notes" style="width: 80%;height: 120px;"></textarea>
                <br>
                <input type="submit" class="btn btn-primary" value="{{ __('menu.continue') }}"/>
            </div>

        </div>
    </form>
    <br/>
    <span class="requiredStar"> * Required</span>
@endsection
