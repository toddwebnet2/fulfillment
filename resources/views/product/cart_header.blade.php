<span style="margin-right:15px;margin-top: 10px;float:right">
    <a class='btn btn-primary' href="<?=app_url();?>/product/cart/view">
        {{ __('message.viewCart') }} -
        {{ $num_products }}
        {{ __('menu.product') }}(s)
    </a>
</span>
