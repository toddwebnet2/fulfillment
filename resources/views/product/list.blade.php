@if(count($itemTypes)>1)
    <div class="row" style="padding-left: 100px">
        {{--        <b>{{ __('product.item_type') }}: </b><BR>--}}

        <a style="margin: 10px" id="productTypeList"
           class="btn <?=($typeId == '') ? "btn-primary" : "btn-warning"?> "
           href="Javascript:loadHomeProducts('')">All</a>

        @foreach($itemTypes as $id => $itemType)
            <a style="margin: 10px" id="productTypeList<?=$id?>"
               class="btn <?=($typeId == $id) ? "btn-primary" : "btn-warning"?> "
               href="Javascript:loadHomeProducts('<?=$id?>')"><?=__($itemType)?></a>
        @endforeach

        <hr/>
    </div>
@endif

<input type="hidden" id="cannotOrderThatMany" value="{{ __('product.cannotOrderThatMany') }}"/>
<div class="row">
    @foreach ($items as $item)
        <?php
        $itemCount = $itemCounts[$item->id];
        $inventoryCount = $itemInvCounts[$item->id];
        ?>
        <div class="col-md-4">
            <div style="text-align: center">

                <a href="JavaScript:imageModalShow('{{$item->id}}')">
                    <img id="productImage_{{$item->id}}" src="<?=app_url()?>{{ $item->productImagePath() }}"
                         style="max-height: 150px;max-width: 150px;"/>
                </a>
                <BR>
                <b id="productTitle_{{$item->id}}">{{$item->title}} <BR> {{ __($item->itemType()) }} - {{$item->ref}}
                </b><br/>

                <div style="font-size:12px;color:darkblue;font-weight:bold">
                    {{ formatNumber($itemCount) }} {{ __('product.itemsAvailable') }}
                    {{ formatNumber($inventoryCount) }} {{ __('product.itemsInStock') }}
                </div>
                <input type="hidden" id="qtyOnHand_{{ $item->id }}" value="{{$itemCount}}"/>

                @if ($itemCount > 0)
                    <input type="number" onkeypress="return IntegersOnly(event)" style="width: 75px" maxlength="6"
                           id="productinput_{{$item->id}}" value="{{$shoppingCartItems[$item->id]}}"
                           onfocus="this.select();" onmouseup="return false;">
                    <a href="JavaScript:addCart({{$item->id}})" id="AddCart_{{ $item->id }}"
                       class="addCartBtn btn btn-primary">
                        {{ __('menu.updateCart') }}</a>
                    @if ($itemCount <= 100)
                        <BR>
                    @endif
                @endif
                @if ($itemCount <= 100)
                    <span style="color:#d43f3a;font-weight:bold">{{ __('product.getMoreStuff') }}</span>
                    <br><br>
                @endif
            </div>
            <BR/><BR/>


        </div>
    @endforeach
    @if(count($items) == 0)
        <div class="col-md-12" style="text-align:center;font-weight:bold">{{ __('product.getMoreStuff') }}
            <br><br>&nbsp;
        </div>
    @endif
</div>
