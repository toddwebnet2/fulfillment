@extends('layouts.template')
@section('title',  __('menu.checkout') )
@section('body')
    <h2>{{ __('menu.checkout') }}</h2>
    <div class="row">
        <div class="col-md-12"><?=$cart?></div>
        <div class="col-md-12"><h2>{{ __('menu.checkout') }}</h2></div>

        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th colspan="2">{{ __('order.from') }}</th>
                    </thead>
                </tr>

                <tr>
                    <td>
                        <label style="font-weight:bold" for="shipper_id">{{ __('order.shipper') }}</label>
                    </td>
                    <td>
                        {{ $shipper }}
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_first_name">{{ __('order.first_name') }}</label>
                    </td>
                    <td>{{ $formFields['from_first_name'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_last_name">{{ __('order.last_name') }}</label>
                    </td>
                    <td>{{ $formFields['from_last_name'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_company">{{ __('order.company') }}</label>
                    </td>
                    <td>{{ $formFields['from_company'] }}</td>
                </tr>

                <tr>
                    <td><label style="font-weight:bold" for="from_address1">{{ __('order.address1') }}</label></td>
                    <td>{{ $formFields['from_address1'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_address2">{{ __('order.address2') }}</label></td>
                    <td>{{ $formFields['from_address2'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_address3">{{ __('order.address3') }}</label></td>
                    <td>{{ $formFields['from_address3'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_city">{{ __('order.city') }}</label></td>
                    <td>{{ $formFields['from_city'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_state">{{ __('order.state') }}</label></td>
                    <td>{{ $formFields['from_state'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_zip">{{ __('order.zip') }}</label></td>
                    <td>{{ $formFields['from_zip'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_country">{{ __('order.country') }}</label></td>
                    <td>{{ $formFields['from_country'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_phone1">{{ __('order.phone1') }}</label></td>
                    <td>{{ $formFields['from_phone1'] }}</td>

                <tr>
                    <td><label style="font-weight:bold" for="from_phone2">{{ __('order.phone2') }}</label></td>
                    <td>{{ $formFields['from_phone2'] }}</td>

                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_fax">{{ __('order.fax') }}</label></td>
                    <td>{{ $formFields['from_fax'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="from_email">{{ __('order.email') }}</label></td>
                    <td>{{ $formFields['from_email'] }}</td>

                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th colspan="2">{{ __('order.to') }}</th>
                    </thead>
                </tr>
                <tr>
                    <td>
                        <label style="font-weight:bold" for="">&nbsp;</label>
                    </td>
                    <td>
                  &nbsp;
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_first_name">{{ __('order.first_name') }}</label>
                    </td>
                    <td>{{ $formFields['to_first_name'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_last_name">{{ __('order.last_name') }}</label></td>
                    <td>{{ $formFields['to_last_name'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_company">{{ __('order.company') }}</label>
                    </td>
                    <td>{{ $formFields['to_company'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_address1">{{ __('order.address1') }}</label></td>
                    <td>{{ $formFields['to_address1'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_address2">{{ __('order.address2') }}</label></td>
                    <td>{{ $formFields['to_address2'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_address3">{{ __('order.address3') }}</label></td>
                    <td>{{ $formFields['to_address3'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_city">{{ __('order.city') }}</label></td>
                    <td>{{ $formFields['to_city'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_state">{{ __('order.state') }}</label></td>
                    <td>{{ $formFields['to_state'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_zip">{{ __('order.zip') }}</label></td>
                    <td>{{ $formFields['to_zip'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_country">{{ __('order.country') }}</label></td>
                    <td>{{ $formFields['to_country'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_phone1">{{ __('order.phone1') }}</label></td>
                    <td>{{ $formFields['to_phone1'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_phone2">{{ __('order.phone2') }}</label></td>
                    <td>{{ $formFields['to_phone2'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_fax">{{ __('order.fax') }}</label></td>
                    <td>{{ $formFields['to_fax'] }}</td>
                </tr>
                <tr>
                    <td><label style="font-weight:bold" for="to_email">{{ __('order.email') }}</label></td>
                    <td>{{ $formFields['to_email'] }}</td>
                </tr>

            </table>

        </div>
        <div class="col-md-12" style="text-align:center">
            <b>{{ __('order.event') }}</b><br>
            {{$formFields['event']}}<br>
            <b>{{ __('order.notes') }}</b><br>
            {{$formFields['notes']}}
            <br><br>
            <form action="<?=app_url()?>/checkout/checkout" method="post">
                {{ csrf_field() }}
                @foreach ($formFields as $name=>$value)
                    {{ Form::hidden($name, $value) }}
                @endforeach
                <input type="submit" class="btn btn-primary" value="{{ __('menu.continue') }}"/>
            </form>
        </div>

    </div>

    <br/>
@endsection
