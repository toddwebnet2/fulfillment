@extends('layouts.template')

@section('title',  $title )

@section('body')
    <h1 style="margin-top: 50px">{{ $title }}</h1>
    <div class="row">
        <div class="wrapper" style="margin-left: 100px">
            <?=$message?>
            <BR><BR>

            <a href="<?=app_url()?>/">Back to Login</a>

        </div>

    </div>

@endsection
