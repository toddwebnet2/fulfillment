<h2>{{ __('order.picklist') }}</h2>

<?php $itemList = [];

?>
<h2>Order Number: <?=$order->confirmation_number?> - {{ $order->id }}</h2>
<b>Notes: </b><?=$order->notes?>
<table class="table table-striped" border="0">
    <tr>
        <thead>
        <th colspan="2">{{ __('menu.products') }}</th>
        </thead>
    </tr>
    @foreach($picklist as $item)
        <?php
        $item = (object)$item;
        $index = 0;
        $itemList[] = $item->item_id;
        ?>
        <tr>
            <td valign="top" style="vertical-align: top; text-align: center"><img id="productImage_{{$item->id}}"
                     src="<?=app_url()?>{{ \App\Models\Item::find($item->item_id)->productImagePath() }}"
                     style="max-height: 50px;max-width: 75px;"/></td>
            <td valign="top" style="vertical-align: top">
                {{$item->title}} <BR> {{ __($item->item_type) }}
                - {{$item->ref}} <br/>
            </td>

            <td valign="top" style="vertical-align: top">
                <table align="right" border="0" cellspacing="0" cellpadding="0" style="margin: 0px;padding: 0px">
                    <tr>
                        <thead>
                        <th>{{ __('menu.bin') }}</th>
                        <th>{{ __('fei.count') }}</th>
                        </thead>
                    </tr>
                    @foreach($item->bins as $bin)
                        <?php $index++;?>
                        @if(isset($bin->id) && $bin->id !== null)
                            <tr>
                                <td style="zwidth: 300px">
                                    {{ $bin->warehouse }} - {{ $bin->bin}}
                                </td>
                                <td style="text-align: center;padding-left:4px">

                                    {{ formatNumber( $bin->num_items )}}</td>
                            </tr>
                        @endif
                    @endforeach
{{--                    <tr>--}}
{{--                        <td style="text-align: right"><b>{{ $item->ref }} - {{  __('fei.total') }}:</b></td>--}}
{{--                        <td style="font-weight: bold; font-size: 24px;padding: 4px"> {{ $item->num_items }} </td>--}}
{{--                    </tr>--}}
                </table>
            </td>
        </tr>
        <tr><td colspan="3"><hr/></td></tr>
    @endforeach
</table>




