@extends('layouts.template')

@section('title',  __('menu.orders') )
@section('body')
    @if ($isAdmin)
        <a href="<?=app_url()?>/orders/canceled" class="float-right">{{__('order.canceledOrders')}}</a>
    @endif
    <h2>{{ __('order.orders') }}</h2>
    <ol class="breadcrumb">

        <li class="breadcrumb-item active">{{ __('order.orders') }}</li>
    </ol>
    <form action="<?=app_url()?>/orders" method="post">
        @if ($isAdmin)
            <table align="center">
                <tr>
                    <td>
                        <label for="warehouse">{{ __('fei.company') }}: </label></td>
                    <td>
                        <select class="select2 form-control required" name="companyId">
                            <option value="">{{ __('message.allCompanies') }}</option>
                            @foreach($companies as $id => $company)
                                <option value="{{ $id}}" <?=($companyId == $id) ? 'selected' : ''?>>{{ $company }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><input type="submit" class="btn btn-primary" value="{{ __('fei.submit') }}"/></td>
                </tr>
            </table>
            @endif
            {{--        <table align="center">--}}
            {{--            <tr>--}}
            {{--                <td>{{ __('order.completedOrders') }}</td>--}}
            {{--                <td>{{ __('fei.startDate') }}</td>--}}
            {{--                <td>--}}
            {{--                    <input type="text" class="datetime" name="startDate" value="{{ $startDate }}"/>--}}
            {{--                </td>--}}
            {{--                <td>{{ __('fei.endDate') }}</td>--}}
            {{--                <td>--}}
            {{--                    <input type="text" class="datetime" name="endDate" value="{{ $endDate }}"/>--}}
            {{--                </td>--}}
            {{--                <td><input type="submit" class="btn btn-primary" value="{{ __('fei.submit') }}"/></td>--}}
            {{--            </tr>--}}
            {{--        </table>--}}

            </div>
    </form>

    <div class="row">
        <div class="col-md-4">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>{{ __('order.pendingOrders') }}</th>
                    </thead>
                </tr>
                @if (count($pendingOrders) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($pendingOrders as $order)
                    <tr>
                        <td>
                            <a href="<?=app_url();?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }}
                                - {{ $order->id }}</a>
                            - {{ formatDate($order->created_at) }}
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>

        <div class="col-md-4">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>{{ __('order.trackingOrders') }}</th>
                    </thead>
                </tr>
                @if (count($trackingOrders) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($trackingOrders as $order)
                    <tr>
                        <td>
                            <a href="<?=app_url();?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }}
                                - {{ $order->id }}</a>
                            - {{ formatDate($order->updated_at) }}
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>


        <div class="col-md-4">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>
                        @if (\App\Services\AuthService::isAdminUser())
                            {{ __('order.readyForInvoice') }}
                        @else
                            {{ __('order.completedOrders') }}
                        @endif
                    </th>

                    </thead>
                </tr>
                <tr>
                    <td style="font-weight:bold">{{ $start }} - {{$end}} </td>
                </tr>
                @if (count($invoicableOrders) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($invoicableOrders as $order)
                    <tr>
                        <td>
                            <a href="<?=app_url()?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }}
                                - {{ $order->id }}</a>
                            - {{ formatDate($order->updated_at) }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>

@endsection
