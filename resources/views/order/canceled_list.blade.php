@extends('layouts.template')

@section('title',  __('menu.orders') )
@section('body')

    <h2>{{__('order.canceledOrders')}}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url()?>/orders">{{ __('order.orders') }}</a></li>
        <li class="breadcrumb-item active">{{__('order.canceledOrders')}}</li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <thead>
                    <th>{{ __('order.canceledOrders') }}</th>
                    </thead>
                </tr>
                @if (count($orders) == 0)
                    <tr>
                        <td> 0 {{ __('fei.records') }}</td>
                    </tr>
                @endif
                @foreach($orders as $order)

                    <tr>
                        <td>
                            <a href="<?=app_url();?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }}
                                - {{ $order->id }}</a>
                            - {{ formatDate($order->created_at) }}
                            - {{ formatDate($order->updated_at) }}

                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
@endsection
