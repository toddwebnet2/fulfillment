@extends('layouts.template')

@section('title',  __('menu.orders') )
@section('body')
    <h2>{{ $order->confirmation_number }} - {{ $order->id }}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/orders">{{ __('order.orders') }}</a></li>
        <li class="breadcrumb-item active">{{ $order->confirmation_number }} - {{ $order->id }}</li>
    </ol>


    <div class="col-md-12">

        <a href="<?=app_url();?>/order/view/{{ hashEncrypt($order->id) }}/printable" target="_blank"
           class="btn btn-warning">{{ __('fei.printView') }}</a>

        @if(\App\Services\AuthService::isAdminUser())

            @if($order->status_id == 5 && strtotime($order->updated_at) >= strtotime("61 days ago") )
                <a href="<?=app_url();?>/order/reset/{{ hashEncrypt($order->id) }}/canceled"
                   class="btn btn-primary">Reset Order to Pending</a>
            @endif

{{--            Status is Ready for INvoice, option to send back to tracking--}}
            @if($order->status_id == 2)
                <a href="<?=app_url();?>/order/back-to-tracking/{{ hashEncrypt($order->id) }}" target="_blank"
                   class="btn btn-primary">Send Back to Tracking</a>
            @endif

            @if($order->status_id == 1)
                <a href="<?=app_url();?>/order/picklist/{{ hashEncrypt($order->id) }}" target="_blank"
                   class="btn btn-primary">{{ __('order.picklist') }}</a>
            @endif

            @if($order->status_id == 5)
                <a href="<?=app_url();?>/order/reset/{{ hashEncrypt($order->id) }}/replenish"
                   class="btn btn-primary">{{ __('order.resetOrder') }}</a>
            @endif

            @if($order->status_id < 2)
                <a href="<?=app_url();?>/order/tracking/{{ hashEncrypt($order->id) }}"
                   class="btn btn-primary">{{ __('order.processTracking') }}</a>


                <a style="float:right" href="JavaScript:cancelOrder({{ hashEncrypt($order->id) }})"
                   class="btn btn-danger">Cancel
                    Order</a>
            @else
                <a style="float:right"
                   href="<?=app_url();?>/admin/company/{{ hashEncrypt($order->company_id) }}/invoices"
                   class="btn btn-warning">{{ __('menu.gotoInvoicing') }}</a>
            @endif
        @endif

    </div>

    <BR>

    <div class="row" style="background-color:white">

        <div class="col-md-12">
            <?=$orderView?>
        </div>

    </div>

    <input type="hidden" id="areYouSure" value="{{ __('message.areYouSure') }}"/>
@endsection


