@extends('layouts.template')

@section('title',  __('menu.orders') )
@section('body')
    <h2>{{ __('order.processTracking') }}</h2>

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=app_url();?>/orders">{{ __('order.orders') }}</a></li>
        <li class="breadcrumb-item"><a
                    href="<?=app_url();?>/order/view/{{ hashEncrypt($order->id) }}">{{ $order->confirmation_number }} - {{ $order->id }}</a></li>
        <li class="breadcrumb-item active">{{ __('order.processTracking') }}</li>
    </ol>

    {{ Form::hidden('',__('order.trackingNumber'), ['id'=> 'trackingNumber'] ) }}
    {{ Form::hidden('',__('order.shipper'), ['id'=> 'shipper'] ) }}

    <form action="<?=app_url()?>/order/tracking/finalize/{{hashEncrypt($order->id) }}" method="post"
          onsubmit="return finalizeFulfillment()">
        {{ Form::hidden('totalsMustEqual', __('order.totalsMustEqual')) }}
        <?php $itemList = [];?>
        <table class="table table-striped">
            <tr>
                <thead>
                <th colspan="2">{{ __('menu.products') }}</th>
                </thead>
            </tr>
            @foreach($picklist as $item)
                <?php
                $item = (object)$item;
                $index = 0;
                $itemList[] = $item->item_id;
                ?>
                {{ Form::hidden('descr_' . $item->item_id, $item->ref . ' - ' . $item->title) }}
                <tr>
                    <td valign="top" style="vertical-align: top">{{$item->title}} <BR> {{ __($item->item_type) }}
                        - {{$item->ref}} <br/>
                        <img id="productImage_{{$item->id}}"
                             src="<?=app_url()?>{{ \App\Models\Item::find($item->item_id)->productImagePath() }}"
                             style="height: 150px"/>
                    </td>

                    <td valign="top" style="vertical-align: top">
                        <table align="right" cellspacing="0" cellpadding="0" style="margin: 0px;padding: 0px">
                            <tr>
                                <thead>
                                <th>{{ __('menu.bin') }}</th>
                                <th>{{ __('fei.count') }}</th>
                                </thead>
                            </tr>
                            @foreach($item->bins as $bin)
                                <?php $index++;?>
                                @if(isset($bin->id) && $bin->id !== null)
                                    <tr>
                                        <td style="width: 300px">
                                            {{ $bin->warehouse }} - {{ $bin->bin}}
                                        </td>
                                        <td>
                                            {{Form::hidden('item_bin_' . $item->item_id . '_' . $index, $bin->id)}}
                                            <input name="item_{{ $item->item_id }}_{{ $index }}" style="width: 75px"
                                                   type="text" value="{{ $bin->num_items }}" maxlength="5"
                                                   class="integer"
                                            /></td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td style="text-align: right"><b>{{ $item->ref }} - {{  __('fei.total') }}:</b></td>
                                <td>{{ $item->num_items }}</td>
                            </tr>
                            {{ Form::hidden('total_' . $item->item_id, $item->num_items) }}
                        </table>
                    </td>
                </tr>
            @endforeach
        </table>
        {{Form::hidden('itemList', implode(',', $itemList))}}

        <div class="row">
            <h2>{{ __('order.tracking') }}</h2>
        </div>
        <div class="row" id="trackingDiv">
        </div>
        <div class="row">
            @if($statusId < 2)
                <div class="col-md-12">
                    <a style="float:right"
                       href="JavaScript: addTracking('',0, {{ $order->shipper_id }}, '');"> {{ __('order.addTracking') }}</a>
                    {{ Form::hidden('num_tracking', 0) }}
                </div>
            @endif

        </div>

        <input type="submit" class="btn btn-primary"
               value="{{ __('order.finalizeFulfillment') }}"/> <br/> <br/>

    </form>
    <input type="hidden" id="shipperJson" value="{!! htmlspecialchars(json_encode($shippers)) !!}"/>


    <script type="text/javascript">
      $(document).ready(function () {
          @foreach($tracking as $track)
          addTracking("{{$track->tracking_number}}", "{{$track->id}}", "{{$track->shipper_id}}");
          @endforeach
          addTracking('', 0, {{ $order->shipper_id }});
          @if($statusId >=2)
          $('input').attr('disabled', 'disabled');
          @endif
      });

    </script>
    <input type="hidden" id="confirmationFinalize" value="{{ __('message.confirmationFinalize') }}"/>

@endsection
