<h2>{{ __('order.orderStatusIs') }} {{ $status }}</h2>
<table width="620" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="CENTER" valign="MIDDLE">


            <table cellSpacing="0" cellPadding="0" width="600" border="0" id="table6">
                <tr>
                    <td vAlign="top" align="left">
                        <div style="background-color: black"><img
                                    src="{{ trim(env('APP_URL'). '/') }}/images/fei-logo-small.png"
                                    style="padding: 4px;height:30px"/></div>
                        <font face="Arial, Helvetica, sans-serif" color="#cc0000" size="3">

                            <b><i>{{ __('order.orderConfirmationDescr') }} </i></b> </font><font
                                face="Arial, Helvetica, sans-serif" size="2">
                            {{ $order->confirmation_number }} - {{ $order->id }} </font><font face="Arial, Helvetica, sans-serif"
                                                                          color="#cc0000" size="3">
                        </font>
                        <br>
                        <br>

                        &nbsp;<table cellSpacing="0" cellPadding="0" width="100%" border="1" id="table7">
                            <tr>
                                <td><font face="Arial, Helvetica, sans-serif" size="2">
                                        {{ formatDAte($order->created_at) }}</font></td>
                                <td><font face="Arial, Helvetica, sans-serif" size="2">
{{--                                        {{ __('order.customerNumber') }} {{ $company->company_code }}--}}
                                    </font></td>
                            </tr>
                        </table>


                        <table border=0 cellpadding=2 cellspacing=0 width="100%" id="table6">

                            <tr>
                                <td align=left bgcolor="#666666">&nbsp;</td>
                                <td align=center bgcolor="#666666" valign=bottom><b>
                                        <font face="arial, helvetica" color="#FFFFFF"
                                              style="font-size: 9pt">{{ strtoupper(__('order.itemNumber')) }}</font></b>
                                </td>
                                <td align=left bgcolor="#666666" valign=bottom>
                                    <font color=#ffffff face="arial, helvetica"
                                          style="font-size: 9pt"><b>{{ strtoupper(__('order.description')) }}</b></font>
                                </td>
                                <td align=right bgcolor="#666666" valign=bottom>
                                    <font color=#ffffff face="arial, helvetica"
                                          style="font-size: 9pt"><b>&nbsp;&nbsp;{{ strtoupper(__('order.qty')) }}&nbsp;&nbsp;</b></font>
                                </td>

                            </tr>
                            <?php $totals = 0;?>
                            @foreach($items as $item)
                                <?php $totals += $item->num_items;?>
                                <tr>
                                    <td align="LEFT" colspan="4">

                                    </td>
                                </tr>

                                <tr>
                                    <td style="border-style: none; border-width: medium">&nbsp;</td>
                                    <td style="border-style: none; border-width: medium"><span
                                                style="font-size: 9pt">{{ $item->ref }}</span></td>
                                    <td style="border-style: none; border-width: medium"><span style="font-size: 9pt">{{ __($item->item_type) }}
                                            - {{ $item->title }}</span></td>
                                    <td style="border-style: none; border-width: medium" align="center">
                                        <p align="center">
                                            <span style="font-size: 9pt">{{ formatNumber($item->num_items) }}</span>
                                        </p>
                                    </td>

                                </tr>
                            @endforeach
                            <tr>
                                <td align="LEFT" colspan="4">

                                </td>
                            </tr>

                            <tr>

                                <td colspan=3 align=right><b>
                                        <font face="arial, helvetica"
                                              size="2">{{ __('order.totalQuantity') }}</font></b><font
                                            face="arial, helvetica" size="2"><b>:</b></font></td>
                                <td style="border-style: none; border-width: medium" align=center>

                                    {{ formatNumber($totals) }}
                                </td>
                            </tr>

                            <tr>
                                <td align="LEFT" colspan="4">
                                    &nbsp;
                                </td>
                            </tr>

                            <TR>
                                <td ALIGN=Right colspan=8>
                                    <font color="#CC0000" face="arial,helvetica"
                                          size="2"><b>{{ strtoupper(__('order.shippingMethodSelected')) }}</b></font>
                                </TD>
                            </TR>
                            <TR>
                                <TD ALIGN=Right colspan=8>
                                    <p>
                                        {{ $shipper->shipper_name }}</p>
                                    <nobr></nobr>
                                    <input TYPE="hidden" NAME="CartID" VALUE="29285">
                                </TD>
                            </TR>

                            <TR>
                                <td ALIGN=Right colspan=8 class="cartmenus">&nbsp;</TD>
                            </TR>
                            <TR>
                                <td ALIGN=Right colspan=8 class="cartmenus"></TD>
                            </TR>
                        </TABLE>
                        <br>
                        &nbsp;<table cellSpacing="0" cellPadding="5" width="600" border="1" id="table9">
                            <tr bgColor="#666666">
                                <td vAlign="top" align="left" width="300">
                                    <font face="Arial, Helvetica" color="#ffffff" size="2">
                                        <b>{{ __('order.from') }}:</b></font></td>
                                <td vAlign="top" align="left" width="300">
                                    <b>
                                        <font face="Arial, Helvetica" size="2" color="#FFFFFF">
                                        </font></b><font face="Arial, Helvetica" color="#ffffff"
                                                         size="2"><b>{{ __('order.to') }}:</b></font>
                                </td>
                            </tr>
                            <tr>
                                <td width=220 valign="top">
                                    <p>
                                        <b>
                                            <font size="1"
                                                  face="Arial"><?=\App\Services\OrderService::formatAddresses($order, 'from')?>
                                            </font></b></p>
                                    &nbsp;
                                </td>
                                <td vAlign="top" align="left">
                                    <table border="0" width="100%" id="table13" style="border-width: 0px">
                                        <tr>
                                            <td width=220 valign="top">
                                                <p>
                                                    <b>
                                                        <font size="1" face="Arial">
                                                            <?=\App\Services\OrderService::formatAddresses($order, 'to')?></font></b>
                                                </p>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        @if($orderTracking->count()> 0)
                            <br>
                            &nbsp;
                            <table cellSpacing="0" cellPadding="4" width="620" border="1" id="table14">
                                <tr align="left" bgColor="#666666">
                                    <td vAlign="top"><b>
                                            <font face="Arial, Helvetica, sans-serif" size="2"
                                                  color="#FFFFFF">{{ __('order.tracking') }}</font></b></td>
                                </tr>
                                @foreach($orderTracking as $trackingNumber)
                                    <tr align="left">
                                        <td vAlign="top">
                                            <p style="margin-top: 0; margin-bottom: 0">
                                                {{ $shippers[$trackingNumber->shipper_id] }}:
                                                <?= \App\Services\TrackingService::getTrackingLink($trackingNumber->tracking_number, $trackingNumber->shipper_id) ?>
                                                @if ($showCost)
                                                    Cost: {{ $trackingNumber->cost }}
                                                @endif
                                            </p>
                                            <hr color="#CCCCCC" size="1">
                                            &nbsp;
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <br>
                        @endif
                        <br>
                        &nbsp;<table cellSpacing="0" cellPadding="4" width="620" border="1" id="table14">
                            @if(strlen(trim($order->event))> 0)
                                <tr align="left" bgColor="#666666">
                                    <td vAlign="top"><b>
                                            <font face="Arial, Helvetica, sans-serif" size="2"
                                                  color="#FFFFFF">{{ __('order.event') }}</font></b></td>
                                </tr>
                                <tr align="left">
                                    <td vAlign="top">
                                        <p style="margin-top: 0; margin-bottom: 0">
                                            <font size="1">
                                                <?=str_replace('\n', "<BR>", $order->event)?></font></p>
                                        <hr color="#CCCCCC" size="1">
                                        &nbsp;
                                    </td>
                                </tr>
                            @endif
                            <tr align="left" bgColor="#666666">
                                <td vAlign="top"><b>
                                        <font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Notes
                                            / Special Instructions</font></b></td>
                            </tr>
                            <tr align="left">
                                <td vAlign="top">
                                    <p style="margin-top: 0; margin-bottom: 0">
                                        <font size="1">
                                            <?=str_replace('\n', "<BR>", $order->notes)?></font></p>
                                    <hr color="#CCCCCC" size="1">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                        &nbsp;
                    </td>
                </tr>
                <tr align="middle">
                    <td vAlign="top">
                        <font face="Arial, Helvetica" size="-1"><br>
                        </font>&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
