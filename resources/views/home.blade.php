@extends('layouts.template')

@section('title',  __('fei.welcome_to') )

@section('body')
    @if(\App\Services\AuthService::isAdminUser())
        @if($pendingUserCount > 0)
            <div class="alert alert-danger" style="text-align:center"><a href="<?=app_url()?>/admin/users">{{ $pendingUserCount }}
                    {{ __('fei.pendingUsers') }}</a></div>
        @endif
        <?= \App\Services\UserBarService::getOrdersBar() ?>
        <span style="float:right;margin-top: 10px">{{ Form::select('company_id', $companies, $companyId, [
            'class'=>'select2',
            'onchange' => 'loadHomeProducts(\'\')'
        ]) }}</span>

    @else
        {{ Form::hidden('company_id', $companyId) }}
    @endif
    <h2>{{ __('fei.welcome') }} {{ $company->company_name }}</h2>

    <div id="productList"></div>
    <script type="application/javascript">
      $(document).ready(function () {
        loadHomeProducts('');

      });
    </script>

@endsection
